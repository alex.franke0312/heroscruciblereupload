﻿using UnityEngine;
using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;
using UnityEngine.SceneManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.UI;

namespace GameStudio.HunterGatherer.SceneManagement
{
    /// <summary>Handles loading different type of scenes</summary>
    public class SceneManager : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField]
        private string mainMenuSceneName = "Menu";

        [SerializeField]
        private String[] gameSceneNames = null;

        public bool InMainMenu
        {
            get { return UnitySceneManager.GetActiveScene().name == mainMenuSceneName; }
        }

        public bool InGameScene
        {
            get { return IsGameScene(UnitySceneManager.GetActiveScene()); }
        }

        private List<string> scenesInBuild = new List<string>();

        public static SceneManager Instance { get; private set; }

        public event Action OnPlayersLoaded;

        private bool isLoading;

        private void Awake()
        {
            Application.targetFrameRate = 0;

            //Singleton
            if (Instance == null)
            {
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);

            //Add scenes in build to the scenesInBuildList
            for (int i = 0; i < UnitySceneManager.sceneCountInBuildSettings; i++)
            {
                string scenePath = SceneUtility.GetScenePathByBuildIndex(i);
                int lastSlash = scenePath.LastIndexOf("/");
                scenesInBuild.Add(scenePath.Substring(lastSlash + 1, scenePath.LastIndexOf(".") - lastSlash - 1));
            }
        }

        /// <summary>Checks if a scene is a game scene</summary>
        public bool IsGameScene(Scene scene)
        {
            foreach (string gameScene in gameSceneNames)
            {
                if (scene.name == gameScene)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>Loads a game scene</summary>
        public void LoadGameScene(string sceneName)
        {
            //Don't continue if scene doesn't exits
            if (!SceneIsLoadable(sceneName))
            {
                return;
            }

            //Don't load the scene if it's already loaded
            if (!UnitySceneManager.GetSceneByName(sceneName).isLoaded && !isLoading)
            {
                isLoading = true;
                LoadingScreen.Instance.Play();

                //Load new game scene
                UnitySceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive).completed += loadOperation =>
                {
                    //Unload current game scene
                    UnitySceneManager.UnloadSceneAsync(UnitySceneManager.GetActiveScene()).completed += unloadOperation =>
                    {
                        //Set new game scene as active scene
                        UnitySceneManager.SetActiveScene(UnitySceneManager.GetSceneByName(sceneName));
                    };

                    isLoading = false;

                    StartCoroutine(CheckPlayersLoaded());
                };
            }
            else
            {
                LoadLoadedOrLoadingSceneError(sceneName);
            }
        }

        /// <summary>Loads a main menu scene</summary>
        public void LoadMainMenuScene()
        {
            //Don't continue if scene doesn't exits
            if (!SceneIsLoadable(mainMenuSceneName))
            {
                return;
            }

            //Don't load the scene if it's already loaded
            if (!UnitySceneManager.GetSceneByName(mainMenuSceneName).isLoaded && !isLoading)
            {
                isLoading = true;
                //Load scene single
                UnitySceneManager.LoadSceneAsync(mainMenuSceneName, LoadSceneMode.Single).completed += loadOperation =>
                {
                    isLoading = false;
                };
            }
            else
            {
                LoadLoadedOrLoadingSceneError(mainMenuSceneName);
            }
        }

        /// <summary>Check if scene exists, used for loading</summary>
        private bool SceneIsLoadable(string sceneName)
        {
            if (!scenesInBuild.Contains(sceneName))
            {
                Debug.LogError("Can't load scene because " + sceneName + " doesn't exist");
                return false;
            }

            return true;
        }

        /// <summary>Show error message for trying to load a scene that is loaded or loading</summary>
        private void LoadLoadedOrLoadingSceneError(string sceneName)
        {
            Debug.LogWarning("You are trying to load " + sceneName + "but this scene has already been loaded or is currently loading!");
        }

        /// <summary>Invokes the OnPlayersLoaded event when all players are loaded in</summary>
        private System.Collections.IEnumerator CheckPlayersLoaded()
        {
            Dictionary<Player, Dictionary<string, object>> playerProperties = NetworkingService.PlayerProperties;
            yield return new WaitUntil(() => playerProperties.All(p => (bool)p.Value[PlayerPropertyHandler.InGameSceneKey]));

            OnPlayersLoaded?.Invoke();       
        }
    }
}