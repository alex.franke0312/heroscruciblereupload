﻿using GameStudio.HunterGatherer.Divisions;
using GameStudio.HunterGatherer.Networking;
using UnityEngine;

namespace GameStudio.HunterGatherer.Environment.Flood
{
    /// <summary>Handles the interaction with the flood</summary>
    public class FloodInteract : InteractBehaviour
    {
        [SerializeField]
        private string layerName = "Flood";
        private NetworkedObject networkedObject;
        [SerializeField]
        private bool destroyInstant = true;

        [SerializeField]
        private Unit unit;

        private Flood flood;

        protected override void Awake()
        {
            base.Awake();
            networkedObject = GetComponentInParent<NetworkedObject>();
        }

        private void OnTriggerEnter(Collider collision)
        {
            // Guard clause to return if object isn't owned
            if (!networkedObject.IsMine)
            {
                return;
            }

            //If it collides with the flood
            if (collision.gameObject.layer == LayerMask.NameToLayer(layerName))
            {
                flood = collision.gameObject.GetComponent<Flood>();
                OnInteract.Invoke();
            }
        }

        private void OnTriggerStay(Collider collision)
        {
            // Guard clause to return if object isn't owned
            if (!networkedObject.IsMine)
            {
                return;
            }

            //If it collides with the flood
            if (collision.gameObject.layer == LayerMask.NameToLayer(layerName))
            {
                flood = collision.gameObject.GetComponent<Flood>();
                OnInteract.Invoke();
            }
        }

        /// <summary>Destroy networkedObject via networking service</summary>
        protected override void Interact()
        {
            if (destroyInstant || !unit)
                NetworkingService.Instance.Destroy(networkedObject);
            else
            {
                unit.TakeDamage(flood.Damage, flood.gameObject);
                unit.SyncHealth(unit.Health);
            }
        }
    }
}