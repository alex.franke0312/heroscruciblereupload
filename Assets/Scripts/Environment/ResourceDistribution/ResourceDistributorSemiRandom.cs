using System.Collections.Generic;
using GameStudio.HunterGatherer.Networking;
using UnityEngine;

namespace GameStudio.HunterGatherer.Resources
{
    public class ResourceDistributorSemiRandom : MonoBehaviour
    {
        [SerializeField]
        private ResourceSetting[] resourceSettings;
        [SerializeField]
        private ResourceManager resourceManager;


        public void SpawnResources()
        {
            int iRandom = 0;
            List<GameObject> spawnedResources;

            Dictionary<GameObject, Transform[]> resourceSpawnPoints = new Dictionary<GameObject, Transform[]>();
            for (int i = 0; i < resourceSettings.Length; i++)
            {
                Transform[] spawnPoints = resourceSettings[i].spawnPoints.GetComponentsInChildren<Transform>();
                resourceSpawnPoints.Add(resourceSettings[i].resource, spawnPoints);
            }

            foreach (ResourceSetting setting in resourceSettings)
            {
                spawnedResources = new List<GameObject>();

                for (int i = 0; i < resourceSpawnPoints[setting.resource].Length; i++)
                {
                    GameObject go = NetworkingService.Instance.Instantiate(setting.resource.name, resourceSpawnPoints[setting.resource][i].position, resourceSpawnPoints[setting.resource][i].rotation, true);
                    PickUpTracker pickUpTracker = go.GetComponent<PickUpTracker>();
                    if (pickUpTracker != null)
                    {
                        pickUpTracker.manager = resourceManager;
                    }
                    resourceManager.activePickUps.Add(go);
                    spawnedResources.Add(go);
                }

                if (setting.randomizeActive)
                {
                    for (int i = spawnedResources.Count - (Random.Range(setting.minActiveResources, setting.maxActiveResources) * (NetworkingService.PlayerList.Length * setting.playerMultiplier)); i > 0; i--)
                    {
                        iRandom = Random.Range(0, spawnedResources.Count - 1);

                        if (NetworkingService.IsHost)
                            NetworkingService.Instance.Destroy(spawnedResources[iRandom].GetComponent<NetworkedObject>());

                        spawnedResources.RemoveAt(iRandom);
                    }
                }
            }
        }

        private void OnValidate()
        {
            for (int i = 0; i < resourceSettings.Length; i++)
            {
                resourceSettings[i].OnValidate();
            }
        }
    }

    [System.Serializable]
    public class ResourceSetting
    {
        public string name;
        public GameObject resource;
        public GameObject spawnPoints;

        public bool randomizeActive = false;

        [Tooltip("Min active resources for one player")]
        public int minActiveResources;

        [Tooltip("Max active resource for one player")]
        public int maxActiveResources;
        public int playerMultiplier;

        public void OnValidate()
        {
            if (maxActiveResources < minActiveResources)
                minActiveResources = maxActiveResources;

            if (minActiveResources > maxActiveResources)
                maxActiveResources = minActiveResources;
        }
    }
}