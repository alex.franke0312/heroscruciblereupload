﻿using System.Collections;
using System.Collections.Generic;
using GameStudio.HunterGatherer.Networking;
using UnityEngine;

public class PickUpTracker : MonoBehaviour
{
    public ResourceManager manager;
    
    /// <summary>
    /// For placing new items the pickups are tracked by a manager so when picked up it should be removed from list.
    /// </summary>
    private void OnDisable()
    {
        if (NetworkingService.IsHost && manager != null)
        {
            manager.activePickUps.Remove(gameObject);
        }
    }
}
