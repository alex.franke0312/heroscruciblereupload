﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using GameStudio.HunterGatherer.Environment;
using UnityEngine;
using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.SceneManagement;
using GameStudio.HunterGatherer.Resources;
using UnityEngine.Serialization;

public class ResourceManager : MonoBehaviour
{
    [SerializeField] private ResourceDistributorSemiRandom resourcePlacerSemiRandom;

    [SerializeField] private DistributerValues[] distributerValuesArray;
    [FormerlySerializedAs("ActivePickUps")]
    public List<GameObject> activePickUps;

    private int currentPos = 0;

    private void Start()
    {
        SceneManager.Instance.OnPlayersLoaded += OnAllPlayersLoaded;
    }

    private void OnDestroy()
    {
        if (SceneManager.Instance)
            SceneManager.Instance.OnPlayersLoaded -= OnAllPlayersLoaded;
        else
            Debug.LogWarning("SceneManager Instance is null, can't access OnPlayersLoaded");
    }

    /// <summary>Called when all players have loaded the game scene, it spawns all resources</summary>
    private void OnAllPlayersLoaded()
    {
        if (NetworkingService.IsHost)
        {
            if (resourcePlacerSemiRandom != null)
            {
                resourcePlacerSemiRandom.SpawnResources();
            }
        }
    }

    [System.Serializable]
    public struct DistributerValues
    {
        public int itemAmount;
        public int minHeight;
        public ResourceDistributor.RangeSettings RangeSquare;
        public int range;
    }
}
