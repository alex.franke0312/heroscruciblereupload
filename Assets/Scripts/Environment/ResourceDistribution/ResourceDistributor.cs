﻿using System;
using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.SceneManagement;
using System.Collections.Generic;
using System.Security.Principal;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace GameStudio.HunterGatherer.Environment
{
    /// <summary>Used to Distributes resources across terrain when testing or playing the game</summary>
    public class ResourceDistributor : MonoBehaviour
    {
        [Header("Settings")]
        public SpawnableResource[] resources = null;

        [SerializeField, Tooltip("minimum height on map where object can spawn")]
        public int minHeight = 10;

        [FormerlySerializedAs("RangeSquare")] [Header("Range Settings")]
        public RangeSettings rangeSettings;
        
        [Header("PlacementSettings")]
        [SerializeField] 
        private float navMeshSampleDistance = 25f;

        [SerializeField] 
        private bool usePoissonDiscSampling = false;
        [SerializeField] 
        public int range = 1;
        private int samplesBeforeRejection = 15;
        private List<Vector2> poissonPositions = new List<Vector2>();


        [Header("References")] 
        [SerializeField]
        private GameObject terrain = null;

        private Mesh terrainMesh;

        private List<GameObject> inEditorSpawns = new List<GameObject>();

        [SerializeField] private ResourceManager resourceManager;

        /// <summary>Called when all players have loaded the game scene, it spawns all resources</summary>
        private void OnAllPlayersLoaded()
        {
            if (NetworkingService.IsHost)
            {
                //only spawn resources if you are the host or testing in the editor
                if (resources != null)
                {
                    SpawnResources();
                }
            }
        }

        /// <summary>Creates resources at given position</summary>
        public void SpawnResources()
        {
            terrainMesh = terrain.GetComponent<MeshFilter>().mesh;
            Vector3 terrainSize = terrainMesh.bounds.size;

            // Creating list of vertices to pick from in PointOnMap
            if (usePoissonDiscSampling == true)
            {
                poissonPositions = PoissonDiscSampling.GeneratePoints(range, new Vector2(rangeSettings.Height, rangeSettings.Width),
                    samplesBeforeRejection);
            }

            foreach (SpawnableResource resource in resources)
            {
                for (int count = 0; count < resource.Count; count++)
                {
                    Quaternion rotation =
                        Quaternion.Euler(new Vector3(0, Random.Range(0f, 359.9f), 0)); //Random rotationa rount Y axis
                    Vector3 position = GetPointOnMap(terrainSize);
                    
                    GameObject go = NetworkingService.Instance.Instantiate(resource.Go.name, position, rotation,
                        true);
                    PickUpTracker pickUpTracker = go.GetComponent<PickUpTracker>();
                    if (pickUpTracker != null) 
                        {
                        pickUpTracker.manager = resourceManager;
                        }
                    resourceManager.activePickUps.Add(go);
                }
            }
        }

        /// <summary>Destroys spawns created in editor</summary>
        public void DestroyEditorSpawns()
        {
            if (inEditorSpawns.Count != 0)
            {
                foreach (GameObject spawn in inEditorSpawns)
                {
                    DestroyImmediate(spawn);
                }

                inEditorSpawns.Clear();
            }
        }

        /// <summary>Spawns resources in editor like it would normaly when playing the game</summary>
        public void SpawnResourcesInEditor()
        {
            terrainMesh = terrain.GetComponent<MeshFilter>().sharedMesh;
            
            // Creating list of vertices to pick from in PointOnMap
            if (usePoissonDiscSampling == true)
            {
                poissonPositions = PoissonDiscSampling.GeneratePoints(range, new Vector2(rangeSettings.Height, rangeSettings.Width),
                    samplesBeforeRejection);
            }

            if (resources == null)
            {
                return;
            }

            Vector3 terrainSize = new Vector3(0,100,0);

            foreach (SpawnableResource resource in resources)
            {
                for (int count = 0; count < resource.Count; count++)
                {
                    GameObject go = Instantiate(resource.Go, GetPointOnMap(terrainSize), Quaternion.identity);
                    go.SetActive(true);
                    inEditorSpawns.Add(go);
                }
            }
        }

        /// <summary>Returns random point on given mapsize based on margin</summary>
        private Vector3 GetPointOnMap(Vector3 mapSize)
        {
            Vector3 position = Vector3.zero;

            if (usePoissonDiscSampling == true && poissonPositions.Count != 0)
            {
                int randomPoint = Random.Range(0, poissonPositions.Count);
                position = new Vector3(rangeSettings.BottomLeftCorner.x, 0 , rangeSettings.BottomLeftCorner.y) +
                           new Vector3(poissonPositions[randomPoint].x, mapSize.y, poissonPositions[randomPoint].y) +
                           new Vector3(Random.Range(-4, 4), 0, Random.Range(-4,4));
                poissonPositions.RemoveAt(randomPoint);
            }
            else
            {
                position = new Vector3(rangeSettings.BottomLeftCorner.x, 0 , rangeSettings.BottomLeftCorner.y) +
                           new Vector3(Random.Range(0, rangeSettings.Height), mapSize.y, Random.Range(0, rangeSettings.Width));
            }

            // get full position
            RaycastHit rayHit;
            if (Physics.Raycast(position, Vector3.down, out rayHit, 100f, LayerMask.GetMask("Terrain")))
            {
                position = rayHit.point;
            }

            if (position.y < minHeight)
            {
                return GetPointOnMap(mapSize);
            }
            
            //clamp to navmesh
            NavMeshHit meshHit;
            if (NavMesh.SamplePosition(position, out meshHit, navMeshSampleDistance, NavMesh.AllAreas))
            {
                position = meshHit.position;
            }
            else
            {
                Debug.LogWarning("Can't clamp object at position: " + position +
                                 " to navmesh :: increase navmeshsampledistance");
            }

            return position;
            
        }

        private void OnDrawGizmos()
        {
            if (usePoissonDiscSampling)
            {
                foreach (var VARIABLE in poissonPositions)
                {
                    Gizmos.color = Color.magenta;
                    Gizmos.DrawSphere(new Vector3(rangeSettings.BottomLeftCorner.x, 0 , rangeSettings.BottomLeftCorner.y) + new Vector3(VARIABLE.x, terrainMesh.bounds.size.y, VARIABLE.y), 5f);
                }
            }
        }
        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireCube(new Vector3(rangeSettings.BottomLeftCorner.x + rangeSettings.Height / 2, minHeight, rangeSettings.BottomLeftCorner.y + rangeSettings.Width / 2), new Vector3( rangeSettings.Height, 1, rangeSettings.Width));
        }

        /// <summary>Container for a spawnable resource containing the prefab reference and the count</summary>
        [System.Serializable]
        public struct SpawnableResource
        {
#pragma warning disable 0649
            public GameObject Go;
            public int Count;
#pragma warning restore 0649
        }
        
        /// <summary>Container for rangeSettings</summary>
        [System.Serializable]
        public struct RangeSettings
        {
#pragma warning disable 0649
            public Vector2 BottomLeftCorner;
            public int Width;
            public int Height;
#pragma warning restore 0649
        }
    }
}