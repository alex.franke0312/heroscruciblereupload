using UnityEngine;
using UnityEngine.Events;
using GameStudio.HunterGatherer.Networking;
using Photon.Pun;

namespace GameStudio.HunterGatherer.Resources
{
    [RequireComponent(typeof(PhotonView))]
    [RequireComponent(typeof(Collider))]
    public class PickUpInteract : MonoBehaviour
    {
        [Tooltip("Make sure to only select one layer!")]
        [SerializeField] LayerMask unitLayer;
        float layerIndex => Mathf.RoundToInt(Mathf.Log(unitLayer.value, 2));
        PhotonView view;

        bool used = false;

        public UnityEvent OnInteract;

        new Collider collider;
        [SerializeField] GameObject model;

        void Awake()
        {
            collider = GetComponent<Collider>();
        }

        void OnEnable()
        {
            collider.enabled = true;
            if (model)
                model.SetActive(true);
        }

        void OnTriggerEnter(Collider other)
        {
            if (used)
                return;

            if (other.gameObject.layer == layerIndex)
            {
                collider.enabled = false;

                view = other.GetComponentInParent<PhotonView>();
                if (view && view.IsMine)
                {
                    OnInteract?.Invoke();
                }

                used = true;
                DestroyPickUp();
            }
        }

        void OnCollisionEnter(Collision collision)
        {
            if (used)
                return;

            if (collision.collider.gameObject.layer == layerIndex)
            {
                collider.enabled = false;

                view = collision.collider.GetComponentInParent<PhotonView>();
                if (view && view.IsMine)
                {
                    OnInteract?.Invoke();
                }

                used = true;
                DestroyPickUp();
            }
        }

        void DestroyPickUp()
        {
            if (NetworkingService.IsHost)
            {
                NetworkingService.Instance.Destroy(GetComponent<NetworkedObject>());
            }

            if (model)
                model.SetActive(false);
        }
    }
}