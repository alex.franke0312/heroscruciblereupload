using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameStudio.HunterGatherer;
using System;
using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.Divisions.Upgrades;

public class NetworkMovingObjectArgs : EventArgs
{
    public NetworkedMovingObject nmo;

    public NetworkMovingObjectArgs(NetworkedMovingObject networkedMovingObject)
    {
        nmo = networkedMovingObject;
    }
}
