using System;
using System.Collections;
using System.Collections.Generic;
using GameStudio.HunterGatherer.Divisions;
using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.Structures;
using UnityEngine;

public class DivisionListEventArgs : EventArgs
{
    public NetworkedMovingObject division;
    public bool setActive;

    public StructureInteract? structureInteract;

    public DivisionListEventArgs(NetworkedMovingObject division, bool setActive, StructureInteract? structureInteract = null)
    {
        this.division = division;
        this.setActive = setActive;
        this.structureInteract = structureInteract;
    }
}
