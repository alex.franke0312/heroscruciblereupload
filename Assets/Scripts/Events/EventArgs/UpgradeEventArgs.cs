using System;
using System.Collections;
using System.Collections.Generic;
using GameStudio.HunterGatherer.Divisions.Upgrades;
using GameStudio.HunterGatherer.Networking;
using UnityEngine;

public class UpgradeEventArgs : EventArgs
{
    public NetworkedMovingObject NetworkedMovingObject { get; private set; }
    public UpgradeBase Upgrade { get; private set; }

    public UpgradeEventArgs(NetworkedMovingObject networkedMovingObject, UpgradeBase upgrade)
    {
        this.NetworkedMovingObject = networkedMovingObject;
        this.Upgrade = upgrade;
    }
}
