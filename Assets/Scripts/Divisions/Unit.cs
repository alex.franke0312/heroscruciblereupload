﻿using System;
using GameStudio.HunterGatherer.CustomEvents;
using GameStudio.HunterGatherer.Divisions.UnitBehaviours;
using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.Networking.Events;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

namespace GameStudio.HunterGatherer.Divisions
{
    /// <summary>Unit used for one unit in a division</summary>
    public class Unit : MonoBehaviour
    {
        [Header("References")]
        [SerializeField]
        private NetworkedMovingObject networkedMovingObject = null;

        [SerializeField] private Renderer[] renderersToApplyDivisionTexture = new Renderer[0];

        private MaterialPropertyBlock _materialPropertyBlock;

        private MaterialPropertyBlock materialPropertyBlock
        {
            get
            {
                if (_materialPropertyBlock == null)
                    _materialPropertyBlock = new MaterialPropertyBlock();
                return _materialPropertyBlock;
            }
            set { _materialPropertyBlock = value; }
        }

        private float health;
        private UnitState _state;
        private Division _division;
        private bool _isVisible;
        public UnityEventFloat onHit;
        public UnityEvent OnMiss;
        public UnityEvent OnBlock;
        public UnityEvent onHeal;
        public UnityEvent<float> onHealthChanged;

        public Unit LastAttacker { get; private set; }
        public GameObject lastDamagingObject { get; private set; }

        public float Health => health;

        public void SetHealth(float health)
        {
            this.health = health;
            onHealthChanged?.Invoke(health);
        }


        public UnitState State
        {
            get { return _state; }
            set
            {
                // Guard clause to not change state if it's from death to non-idle, that's ILLEGAL
                if (State == UnitState.Death && value != UnitState.Idle)
                {
                    return;
                }

                OnStateChanged.Invoke(State, false);
                _state = value;
                OnStateChanged.Invoke(State, true);
            }
        }

        public UnityEventDivisionGoal OnDivisionChangedGoal { get; } = new UnityEventDivisionGoal();
        public UnityEventUnitState OnStateChanged { get; } = new UnityEventUnitState();

        public Division Division
        {
            get { return _division; }
            set
            {
                if (_division != null)
                {
                    _division.OnChangedGoal.RemoveListener(DivisionChangedGoal);
                }

                _division = value;
                if (_division != null)
                {
                    _division.OnChangedGoal.AddListener(DivisionChangedGoal);
                }
            }
        }

        public NetworkedMovingObject NetworkedMovingObject => networkedMovingObject;
        public bool IsTargetable { get; set; }

        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = value;
                Division?.CheckSelectable(_isVisible);
            }
        }

        public bool HasArrived => State == UnitState.Idle;
        public MoveTarget MoveTarget { get; set; }
        public Unit AttackTarget { get; set; }

        private void OnEnable()
        {
            Division = null;
            IsTargetable = true;
            MoveTarget = new MoveTarget(transform.position, Vector3.forward);
            OnStateChanged.Invoke(State, true);

            if (NetworkingService.IsConnected || NetworkingService.OfflineMode)
            {
                NetworkingService.AddNetworkingEventListener(NetworkingEventType.UnitSetDivision, SetDivision);
                NetworkingService.AddNetworkingEventListener(NetworkingEventType.UnitHit, Hit);
                NetworkingService.AddNetworkingEventListener(NetworkingEventType.SyncHealth, SyncHealth);

            }

            // Stop the NavMeshAgent of enemies to avoid the network and and AI from fighting over control.
            if (!NetworkedMovingObject.IsMine && TryGetComponent(out NavMeshAgent navMeshAgent))
            {
                navMeshAgent.updatePosition = false;
                navMeshAgent.updateRotation = false;
                navMeshAgent.updateUpAxis = false;
            }
        }

        private void OnDisable()
        {
            State = UnitState.Idle;
            IsTargetable = false;
            Division.RemoveUnitFromDivision(this);
            MoveTarget = null;
            AttackTarget = null;
            LastAttacker = null;

            if (NetworkingService.IsConnected || NetworkingService.OfflineMode)
            {
                NetworkingService.RemoveNetworkingEventListener(NetworkingEventType.UnitSetDivision, SetDivision);
                NetworkingService.RemoveNetworkingEventListener(NetworkingEventType.UnitHit, Hit);
            }

            // Reenable navmeshagent so that if it's retrieved from the pool, it will work properly.
            if (TryGetComponent(out NavMeshAgent navMeshAgent))
            {
                navMeshAgent.updatePosition = true;
                navMeshAgent.updateRotation = true;
                navMeshAgent.updateUpAxis = true;
            }
        }

        private void Update()
        {
            // Guard clause to exit if division is not set
            if (Division == null || !Division.SelectableObject.NetworkedMovable.IsMine)
            {
                return;
            }
        }

        private void OnDrawGizmos()
        {
            if (State == UnitState.Move)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(MoveTarget.Position, 1f);
            }
        }

        /// <summary>Raise the event to set the division of this unit to the given division</summary>
        public void RaiseSetDivision(Division division)
        {
            UnitSetDivisionInfo info = new UnitSetDivisionInfo
            {
                UnitViewID = networkedMovingObject.ViewId,
                DivisionViewID = division.SelectableObject.NetworkedMovable.ViewId
            };
            NetworkingService.RaiseEvent(NetworkingEventType.UnitSetDivision, info, EventReceivers.Other);
            SetDivision(info);
        }

        /// <summary>Raise the event to give a new manpower resource to the attacker's owner when the client's unit dies</summary>
        public void RaiseManPowerResource(DivisionType divisionType, Unit attacker)
        {
            ManPowerResourceAddedInfo info = new ManPowerResourceAddedInfo
            { AttackerViewID = attacker.networkedMovingObject.ViewId, DivisionType = divisionType };
            NetworkingService.RaiseEvent(NetworkingEventType.ManPowerResourceAdded, info, EventReceivers.Other);
        }

        /// <summary>Change the division of the unit, received from the UnitSetDivision event</summary>
        private void SetDivision(object content)
        {
            // Guard clause to exit if not connected and not in offline mode
            if (!NetworkingService.IsConnected && !NetworkingService.OfflineMode)
            {
                return;
            }

            UnitSetDivisionInfo info = (UnitSetDivisionInfo)content;

            // Guard clause to exit if viewID doesn't match this unit's viewID
            if (info.UnitViewID != networkedMovingObject.ViewId)
            {
                return;
            }

            GameObject divisionObject = NetworkingResources.Find(info.DivisionViewID);

            if (divisionObject != null)
            {
                Division = divisionObject.GetComponent<Division>();
                ApplyDivisionType();
                Division.AddUnitToDivision(this);
            }

        }

        /// <summary>Invoke the changed goal event</summary>
        private void DivisionChangedGoal(DivisionGoal goal)
        {
            OnDivisionChangedGoal.Invoke(goal);
        }

        /// <summary>
        /// Sets the materials of unit to given materials
        /// </summary>
        /// <param name="material"></param>
        /// <param name="unitTexture"></param>
        /// <param name="divisionTexture"></param>
        public void SetMaterial(Material material, Texture unitTexture, Texture divisionTexture)
        {
            foreach (Renderer renderer in renderersToApplyDivisionTexture)
            {
                if (renderer == null)
                    continue;

                if (material && divisionTexture)
                {
                    renderer.sharedMaterial = material;
                    renderer.GetPropertyBlock(materialPropertyBlock);
                    materialPropertyBlock.SetTexture("_MainTex", divisionTexture);
                    renderer.SetPropertyBlock(materialPropertyBlock);
                }
            }
        }

        /// <summary>Apply division type by when unit switches division or the unit's division changes it's type</summary>
        private void ApplyDivisionType()
        {
            health = Division.TypeData.MaxHealth;
            GetComponent<UnitMovement>().navMeshAgent.speed = Division.DivisionSpeed;
        }

        /// <summary>Raise the event to get hit by a given attacker for the given damage</summary>
        public void RaiseHit(HitType hitType, Unit attacker, float damage = 0)
        {
            UnitHitInfo info = new UnitHitInfo
            {
                UnitViewID = networkedMovingObject.ViewId,
                AttackerViewID = attacker.networkedMovingObject.ViewId,
                HitType = hitType,
                Damage = damage,
                KnockbackDistance = attacker.Division.TypeData.KnockbackDistance,
                KnockbackDuration = attacker.Division.TypeData.KnockbackDuration
            };
            NetworkingService.RaiseEvent(NetworkingEventType.UnitHit, info, EventReceivers.All);
        }

        /// <summary>Handle what happens when you get hit by another unit, received from the UnitHit event</summary>
        public void Hit(object content)
        {
            // Guard clause to exit if not connected and not in offline mode
            if (!NetworkingService.IsConnected && !NetworkingService.OfflineMode)
            {
                return;
            }

            UnitHitInfo info = (UnitHitInfo)content;

            // Guard clause to exit if viewID doesn't match this unit's viewID
            if (info.UnitViewID != networkedMovingObject.ViewId)
            {
                return;
            }

            GameObject attackerObject = NetworkingResources.Find(info.AttackerViewID);

            // Guard clause to exit if attackerObject is null, or we are already dead
            if (attackerObject == null || !IsTargetable)
            {
                return;
            }

            // Trigger auto-attack when idling
            Unit attacker = attackerObject.GetComponent<Unit>();
            if ((info.HitType == HitType.Block || info.HitType == HitType.Hit) && Division != null && Division.IsMine &&
                Division.Goal == DivisionGoal.Idle && attacker.IsVisible)
            {
                Division.AttackOrder(attacker.Division);
            }

            switch (info.HitType)
            {
                case HitType.Hit:
                    health -= info.Damage;
                    onHit.Invoke(Division.GetDivisionHealth(false));
                    onHealthChanged?.Invoke(health);
                    LastAttacker = attacker;
                    if (health <= 0 && State != UnitState.Death)
                    {
                        // If this is our unit and this is the lethal blow, reward the attacker manpower
                        if (NetworkedMovingObject.IsMine)
                        {
                            RaiseManPowerResource(Division.TypeData.Type, LastAttacker);
                        }

                        State = UnitState.Death;
                    }

                    Division.TypeData.HitsSinceLastBlock++;

                    //// Hit knockback
                    //Vector3 knockbackDirection = (transform.position - attacker.transform.position);
                    //knockbackDirection.y = 0;
                    //KnockBack(knockbackDirection, info.KnockbackDistance * Division.TypeData.KnockbackFactor, info.KnockbackDuration * Division.TypeData.KnockbackFactor);
                    break;

                // Note by team Tech Shack; previous team(s) made the blocking mechanic fucking ugly, and we're not going to fix it. Sorry!
                case HitType.Block:
                    if (networkedMovingObject.IsMine)
                        OnBlock.Invoke();
                    Division.TypeData.HitsSinceLastBlock = 0;
                    //attacker.Division.TypeData.WillBlockNextHit = false;

                    //// Block knockback
                    //knockbackDirection = (transform.position - attacker.transform.position);
                    //knockbackDirection.y = 0;
                    //KnockBack(knockbackDirection, info.KnockbackDistance * Division.TypeData.KnockbackFactor, info.KnockbackDuration * Division.TypeData.KnockbackFactor);
                    break;

                case HitType.Miss:
                    OnMiss.Invoke();
                    break;
            }
        }

        private void SyncHealth(object obj)
        {
            if (!NetworkingService.IsConnected && !NetworkingService.OfflineMode)
            {
                return;
            }

            SyncUnitHealthInfo info = (SyncUnitHealthInfo)obj;

            // Guard clause to exit if viewID doesn't match this unit's viewID
            if (info.unitID != networkedMovingObject.ViewId)
            {
                return;
            }

            health = info.hp;
        }

        public void Heal(float percentageHeal)
        {
            float maxHealth = Division.TypeData.MaxHealth;
            float hp = health;
            hp += maxHealth * percentageHeal;
            hp = Mathf.Clamp(hp, 0, Division.TypeData.MaxHealth);
            health = hp;
            SyncHealth(hp);
            onHealthChanged?.Invoke(health);
            onHeal?.Invoke();
        }

        public void SyncHealth(float hp)
        {
            SyncUnitHealthInfo info = new SyncUnitHealthInfo
            {
                unitID = networkedMovingObject.ViewId,
                hp = hp
            };
            NetworkingService.RaiseEvent(NetworkingEventType.SyncHealth, info, EventReceivers.All);
        }

        // THIS ISNT USED - REMOVE
        public void TakeDamage(float damageAmount, GameObject damagingObject)
        {
            lastDamagingObject = damagingObject;
            health -= damageAmount;
            onHit.Invoke(Division.GetDivisionHealth(false));
            onHealthChanged?.Invoke(health);

            if (health <= 0 && State != UnitState.Death)
            {
                State = UnitState.Death;
            }
        }

        /// <summary>Knock this unit back in the given direction the given distance over the given duration</summary>
        private void KnockBack(Vector3 direction, float distance, float duration)
        {
            LeanTween.move(gameObject, transform.position + direction.normalized * distance, duration)
                .setEase(LeanTweenType.easeOutQuint);
        }
    }


}