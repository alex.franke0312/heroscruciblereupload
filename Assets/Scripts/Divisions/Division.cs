﻿using GameStudio.HunterGatherer.CustomEvents;
using GameStudio.HunterGatherer.Divisions.UnitBehaviours;
using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.Networking.Events;
using GameStudio.HunterGatherer.Selection;
using GameStudio.HunterGatherer.UI;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using Photon.Pun.UtilityScripts;

namespace GameStudio.HunterGatherer.Divisions
{
    /// <summary>Division that serves as a group of units that the player can command</summary>
    public class Division : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField, Tooltip("Distance between units when they stand in formation")]
        private float unitSpacing = 2f;

        [SerializeField, Min(1), Tooltip("The ratio the division uses for formation")]
        private float formationRatio = 2f;

        [SerializeField, Range(1, 100),
         Tooltip("The percentage of units that have to arrive at a location before idle state is set")]
        private int unitsArrivedTillIdle = 90;

        [SerializeField, Min(0),
         Tooltip("Distance within which the division will look for a new target after killing a previous one")]
        private float autoFindNextTargetDistance = 20f;

        [Header("References")]
        [SerializeField]
        private NetworkedMovingObject networkedMovingObject = null;

        [SerializeField] private DivisionFlag divisionFlag = null;

        [SerializeField] private SelectableObject selectableObject = null;

        [SerializeField] private DivisionColorPalette divisionColorPalette = null;

        [SerializeField] private DivisionTypeDatas divisionTypeDatas = null;

        [SerializeField] public UI.DivisionOverviewItem overviewItem = null;
        public DivisionTypeDatas DivisionTypeDatas => divisionTypeDatas;

        [SerializeField] private Material UnitMaterial = null;

        /// <summary>Determines if this division has had units, so it doesn't delete itself before the first unit has been added</summary>
        private bool hadUnits;

        private Unit flagBearer;
        private MoveTarget _moveTarget;
        private Division _attackTarget;
        private DivisionGoal _goal;

        public DivisionGoal Goal
        {
            get { return _goal; }
            set
            {
                _goal = value;
                OnChangedGoal.Invoke(Goal);
            }
        }

        private float _divisionSpeed;

        public float DivisionSpeed
        {
            get { return _divisionSpeed; }
            set
            {
                if (value == _divisionSpeed)
                    return;

                _divisionSpeed = value;
                foreach (Unit unit in Units)
                {
                    unit.GetComponent<UnitMovement>().navMeshAgent.speed = _divisionSpeed;
                    unit.NetworkedMovingObject.MovementSpeed = _divisionSpeed;
                }

                //makes sure the flag always keeps up with the division 
                NetworkedMovingObject.MovementSpeed = float.MaxValue;
            }
        }

        public UnityEventDivisionGoal OnChangedGoal { get; } = new UnityEventDivisionGoal();
        public UnityEventDivision OnDisableDivision { get; } = new UnityEventDivision();
        public UnityEventInt OnUnitCountChanged { get; } = new UnityEventInt();
        public UnityEvent OnDivisionTypeChanged { get; } = new UnityEvent();
        public UnityEvent OnDivisionStatsChanged { get; } = new UnityEvent();
        public UnityEvent OnUnitAdded { get; } = new UnityEvent();
        public UnityEventBoolBase OnEnterExitBase { get; } = new UnityEventBoolBase();
        public Color DivisionColor { get; private set; }
        public Texture UnitTexture { get; private set; }
        public Texture FlagTexture { get; private set; }
        public SingleDivisionTypeData TypeData;
        public NetworkedMovingObject NetworkedMovingObject => networkedMovingObject;

        public UnityEvent<float> onHealthChanged;
        public UnityEvent onHeal;

        public Unit FirstUnit
        {
            get
            {
                if (Units.Count != 0)
                {
                    return Units[0];
                }

                return null;
            }
        }

        public float GetDivisionHealth(bool maxHealth = false)
        {
            float health = 0;
            if (!maxHealth)
            {
                for (int i = 0; i < Units.Count; i++)
                {
                    health += Units[i].Health;
                }
            }

            if (maxHealth)
            {
                health = TypeData.MaxHealth * TypeData.MaxUnitCount;
            }

            return health;
        }

        public MoveTarget MoveTarget
        {
            get { return _moveTarget; }
            set
            {
                _moveTarget = value;
                RaiseSetMoveTarget(_moveTarget);
            }
        }

        public Division AttackTarget
        {
            get { return _attackTarget; }
            set
            {
                if (_attackTarget != null)
                {
                    _attackTarget.OnDisableDivision.RemoveListener(AttackTargetDies);
                }

                _attackTarget = value;
                if (_attackTarget != null)
                {
                    _attackTarget.OnDisableDivision.AddListener(AttackTargetDies);
                }
            }
        }

        public SelectableObject SelectableObject => selectableObject;
        public bool IsSelected => SelectableObject.IsSelected;
        public bool IsMine => SelectableObject.NetworkedMovable.IsMine;
        public int UnitCount => Units.Count;
        public List<Unit> Units { get; private set; } = new List<Unit>();
        public List<Unit> VisibleUnits { get; private set; } = new List<Unit>();
        public DivisionType Type => TypeData.Type;
        public float UnitSpacing => unitSpacing;
        public float FormationRatio => formationRatio;

        private void Awake()
        {
            OnUnitCountChanged.AddListener((_) => VisibleUnits = Units.Where(x => x.IsVisible).ToList());
        }

        private void OnEnable()
        {
            MoveTarget = new MoveTarget(transform.position, Vector3.forward);
            SelectableObject.SetGroup(IsMine ? SelectableObjectGroup.Friendly : SelectableObjectGroup.Enemy);
            hadUnits = false;

            if (NetworkingService.IsConnected || NetworkingService.OfflineMode)
            {
                NetworkingService.AddNetworkingEventListener(NetworkingEventType.DivisionSetType, SetType);
                NetworkingService.AddNetworkingEventListener(NetworkingEventType.DivisionSetMoveTarget, SetMoveTarget);
                NetworkingService.AddNetworkingEventListener(NetworkingEventType.UnitSetMovementSpeed,
                    SetMovementSpeed);
                NetworkingService.AddNetworkingEventListener(NetworkingEventType.SyncDivisionData, SyncData);
            }

            SetColor();
        }

        private void OnDisable()
        {
            OnDisableDivision.Invoke(this);

            // reset Division for object pool.
            AttackTarget = null;
            Goal = DivisionGoal.Idle;
            Units = new List<Unit>();
            VisibleUnits = new List<Unit>();
            RaiseSetType(DivisionType.Nomad);
            MoveTarget = null;
            // remove all listeners when disabling 
            OnChangedGoal.RemoveAllListeners();
            OnDisableDivision.RemoveAllListeners();
            OnUnitCountChanged.RemoveAllListeners();
            OnDivisionTypeChanged.RemoveAllListeners();
            OnDivisionStatsChanged.RemoveAllListeners();
            OnUnitAdded.RemoveAllListeners();
            OnEnterExitBase.RemoveAllListeners();
            if (NetworkingService.IsConnected || NetworkingService.OfflineMode)
            {
                NetworkingService.RemoveNetworkingEventListener(NetworkingEventType.DivisionSetType, SetType);
                NetworkingService.RemoveNetworkingEventListener(NetworkingEventType.DivisionSetMoveTarget,
                    SetMoveTarget);
                NetworkingService.RemoveNetworkingEventListener(NetworkingEventType.UnitSetMovementSpeed,
                    SetMovementSpeed);
                NetworkingService.RemoveNetworkingEventListener(NetworkingEventType.SyncDivisionData, SyncData);
            }
        }

        private void Update()
        {
            // Guard clause to exit if division is not local
            if (!selectableObject.NetworkedMovable.IsMine)
            {
                return;
            }

            // Destroy division if no units left
            if (Units.Count == 0 && hadUnits)
            {
                if (selectableObject == null || selectableObject.NetworkedMovable == null)
                {
                    Debug.LogWarning("It's null! We can't destroy!");
                    return;
                }

                NetworkingService.Instance.Destroy(selectableObject.NetworkedMovable);
                return;
            }

            UpdatePosition();
        }

        /// <summary>Update flag bearer and set position to that of the flag bearer</summary>
        private void UpdatePosition()
        {
            // Pick a new flag bearer if we don't have one currently (unset or died)
            if (flagBearer == null || !flagBearer.IsTargetable)
            {
                flagBearer = FindFirstTargetableUnit();

                // Guard clause to not update position if flag bearer is still null
                if (flagBearer == null)
                {
                    return;
                }
            }

            // Set position to flag bearers position
            transform.position = flagBearer.transform.position;
        }

        /// <summary>Return first unit belonging to the division that is targetable</summary>
        private Unit FindFirstTargetableUnit()
        {
            foreach (Unit unit in Units)
            {
                if (unit.IsTargetable)
                {
                    return unit;
                }
            }

            return null;
        }

        /// <summary>Handle interaction between given divisions and this division</summary>
        public void Interact(List<Division> divisions)
        {
            // Guard clause to not attack when friendly
            if (IsMine)
            {
                return;
            }

            divisions.ForEach(x => x.AttackOrder(this));
        }

        /// <summary>Give division order to move to given position and end with the given rotation</summary>
        public void MoveOrder(Vector3 position, Vector3 direction)
        {
            if (direction == Vector3.zero)
            {
                direction = (position - transform.position);
            }

            direction.y = 0;

            MoveTarget = new MoveTarget(position, direction);
            AssignClosestMoveTarget(position, direction);

            AttackTarget = null;
            Goal = DivisionGoal.Move;
        }

        /// <summary> Orders the herodivision to place the base </summary>
        public void PlaceBaseOrder(Vector3 position)
        {
            Vector3 direction;
            direction = position - transform.position;
            MoveTarget = new MoveTarget(position, direction);
            AssignClosestMoveTarget(MoveTarget.Position, MoveTarget.Direction);

            AttackTarget = null;
            Goal = DivisionGoal.PlaceBase;
        }

        /// <summary>Give division order to attack given division</summary>
        public void AttackOrder(Division division)
        {
            // Guard clause to exit if division has no units to attack
            if (division.UnitCount == 0)
            {
                return;
            }

            AttackTarget = division;
            MoveTarget = new MoveTarget(AttackTarget.transform, AttackTarget.transform.position - transform.position);
            Goal = DivisionGoal.Attack;
        }

        /// <summary>Give division order to defend their current location against the given division</summary>
        public void DefendOrder(Division division)
        {
            // Guard clause to exit if division has no units to attack
            if (division.UnitCount == 0)
            {
                return;
            }

            AttackTarget = division;
            MoveTarget = new MoveTarget(AttackTarget.transform, AttackTarget.transform.position - transform.position);

            Goal = DivisionGoal.Defend;
        }

        /// <summary>Give division order to idle on current position</summary>
        private void IdleOrder()
        {
            AttackTarget = null;
            Goal = DivisionGoal.Idle;
        }

        /// <summary>Handle what needs to happen when the current attack target of the division dies</summary>
        private void AttackTargetDies(Division _)
        {
            FindNewAttackTargetOrStop();
        }

        /// <summary>See if we can attack an enemy in proximity, otherwise move to our current location</summary>
        private void FindNewAttackTargetOrStop()
        {
            // Make a list of all selectable enemy divisions
            List<Division> enemiesInProximity = new List<Division>();
            foreach (SelectableObject selectableObject in SelectionManager.Instance.SelectableObjects)
            {
                // Don't check this object if it isn't selectable
                if (!selectableObject.IsSelectable)
                {
                    continue;
                }

                Division divisionOfSelectableObject = selectableObject.GetComponent<Division>();
                if (divisionOfSelectableObject != null && divisionOfSelectableObject.UnitCount > 0 &&
                    selectableObject.NetworkedMovable != null && !selectableObject.NetworkedMovable.IsMine)
                {
                    enemiesInProximity.Add(divisionOfSelectableObject);
                }
            }

            // Attack the closest selectable enemy division if he is within autoFindNextTargetDistance, otherwise give idle order
            if (enemiesInProximity.Count > 0)
            {
                enemiesInProximity.Sort((enemy1, enemy2) =>
                    (transform.position - enemy1.transform.position).sqrMagnitude.CompareTo(
                        (transform.position - enemy2.transform.position).sqrMagnitude));
                Division closestEnemyDivision = enemiesInProximity[0];

                if (Vector3.Distance(transform.position, closestEnemyDivision.transform.position) <=
                    autoFindNextTargetDistance)
                {
                    AttackOrder(closestEnemyDivision);
                    return;
                }
            }

            // Stop
            MoveOrder(transform.position, MoveTarget.Direction);
        }

        /// <summary>Raise the event to set the type of this division to the given type</summary>
        public void RaiseSetType(DivisionType type)
        {
            DivisionSetTypeInfo info = new DivisionSetTypeInfo
            { DivisionViewID = selectableObject.NetworkedMovable.ViewId, DivisionType = type };
            NetworkingService.RaiseEvent(NetworkingEventType.DivisionSetType, info, EventReceivers.Other);
            SetType(info);
        }

        /// <summary>Change the type of the division, received from the DivisionSetType event</summary>
        private void SetType(object content)
        {
            // Guard clause to exit if not connected and not in offline mode
            if (!NetworkingService.IsConnected && !NetworkingService.OfflineMode)
            {
                return;
            }

            DivisionSetTypeInfo info = (DivisionSetTypeInfo)content;

            // Guard clause to exit if viewID doesn't match this division's viewID
            if (info.DivisionViewID != selectableObject.NetworkedMovable.ViewId)
            {
                return;
            }


            TypeData = new SingleDivisionTypeData(divisionTypeDatas.GetDivisionTypeData(info.DivisionType));
            DivisionSpeed = TypeData.MovementSpeed;
            SelectableObject.ObjectName = TypeData.TypeName;

            // Replace units with units of new type
            if (IsMine)
            {
                List<Vector3> positions = new List<Vector3>();
                int unitCount = UnitCount;
                for (int i = Units.Count - 1; i >= 0; i--)
                {
                    hadUnits = false;
                    positions.Add(Units[i].transform.position);
                    DestroyUnit();
                }

                for (int i = 0; i < unitCount; i++)
                {
                    Unit unit = SpawnUnit();
                    unit.transform.position = positions[i];
                }
            }

            //Set the textures here, because the division type is now known, so the correct flag can be used and updated when needed
            SetTexture();
            ApplyFlagMaterial();
            OnDivisionTypeChanged.Invoke();
        }

        /// <summary>Raise the event to set the type of this division to the given type</summary>
        public void RaiseSetMoveTarget(MoveTarget target)
        {
            if (target == null)
            {
                return;
            }

            DivisionSetMoveTargetInfo info = new DivisionSetMoveTargetInfo
            {
                divisionId = selectableObject.NetworkedMovable.ViewId,
                position = target.Position,
                direction = target.Direction,
            };
            NetworkingService.RaiseEvent(NetworkingEventType.DivisionSetMoveTarget, info, EventReceivers.Other);
        }

        /// <summary>Change the moveTarget of the division, received from the RaiseSetMoveTarget event</summary>
        private void SetMoveTarget(object content)
        {
            // Guard clause to exit if not connected and not in offline mode
            if (!NetworkingService.IsConnected && !NetworkingService.OfflineMode)
            {
                return;
            }

            DivisionSetMoveTargetInfo info = (DivisionSetMoveTargetInfo)content;

            // Guard clause to exit if viewID doesn't match this division's viewID
            if (info.divisionId != selectableObject.NetworkedMovable.ViewId)
            {
                return;
            }

            _moveTarget = new MoveTarget(info.position, info.direction);
        }

        /// <summary>
        /// Stores (network player color) and SecondaryMaterial (Random color from color palette)
        /// </summary>
        private void SetColor()
        {
            DivisionColor = SelectableObject.NetworkedMovable.IsMine
                ? divisionColorPalette.GetFriendlyDivisionColor()
                : divisionColorPalette.EnemyFlag;
        }

        /// <summary>
        /// Stores (network player texture)
        /// </summary>
        private void SetTexture()
        {
            UnitTexture = SelectableObject.NetworkedMovable.IsMine
                ? divisionColorPalette.GetFriendlyDivisionTexture(DivisionColor)
                : NetworkingPlayerManager.Instance.GetPlayerTexture(SelectableObject.NetworkedMovable.View.Owner
                    .GetPlayerNumber());

            FlagTexture = SelectableObject.NetworkedMovable.IsMine
                ? divisionColorPalette.GetFriendlyFlagTexture(DivisionColor, Type)
                : NetworkingPlayerManager.Instance.GetPlayerFlagTexture(
                    SelectableObject.NetworkedMovable.View.Owner.GetPlayerNumber(), Type);
        }

        /// <summary>Set material of flag to given material</summary>
        public void ApplyFlagMaterial() => divisionFlag.SetMaterial(UnitMaterial, FlagTexture);

        /// <summary>Return unit from division closest to given position</summary>
        public Unit GetClosestUnit(Vector3 position)
        {
            // Guard clause if there is no units
            if (Units.Count == 0)
            {
                return null;
            }

            // Sort and pick closest
            List<Unit> sorted = new List<Unit>();
            Units.ForEach(x =>
            {
                if (x.IsVisible)
                {
                    sorted.Add(x);
                }
            });

            // Guard clause if there is no visible units
            if (sorted.Count == 0)
            {
                return null;
            }

            sorted.Sort((u1, u2) => Vector3.Distance(position, u1.transform.position)
                .CompareTo(Vector3.Distance(position, u2.transform.position)));
            return sorted[0];
        }

        /// <summary>Return unit of division picked based on given index, to better divide attack targets</summary>
        public Unit GetIndexedUnit(int index)
        {
            // Guard clause if there is no units
            if (Units.Count == 0 || index < 0)
            {
                return null;
            }

            // Sort and pick closest
            List<Unit> visibleUnits = new List<Unit>();
            Units.ForEach(x =>
            {
                if (x.IsVisible)
                {
                    visibleUnits.Add(x);
                }
            });

            // Guard clause if there is no visible units
            if (visibleUnits.Count == 0)
            {
                return null;
            }

            int continuedIndex = index % visibleUnits.Count;

            return visibleUnits[continuedIndex];
        }

        /// <summary> Assigns each units MoveTarget, starting with the furthest, getting the closest position. </summary>
        private void AssignClosestMoveTarget(Vector3 position, Vector3 direction)
        {
            List<Vector3> formationPositions =
                FormationLayout.GeneratePositions(Units.Count, position, direction, FormationRatio, UnitSpacing);
            Unit[] sortedUnits = Units.OrderByDescending(u => Vector3.Distance(u.transform.position, position))
                .ToArray();

            foreach (Unit unit in sortedUnits)
            {
                int closestIndex = -1;
                float closestDistance = float.MaxValue;

                for (int iLayout = 0; iLayout < formationPositions.Count; iLayout++)
                {
                    float distance = Vector3.Distance(unit.transform.position, formationPositions[iLayout]);
                    if (distance < closestDistance)
                    {
                        closestIndex = iLayout;
                        closestDistance = distance;
                    }
                }

                unit.MoveTarget = new MoveTarget(formationPositions[closestIndex], direction);
                formationPositions.RemoveAt(closestIndex);
            }
        }

        /// <summary>Spawn new unit and set its division</summary>
        public Unit SpawnUnit()
        {
            //Guard clause to exit if the division contains the max amount of units
            if (Units.Count == TypeData.MaxUnitCount)
            {
                return null;
            }

            Vector3 spawnPos = transform.position;
            NavMeshHit closestHit;
            if (NavMesh.SamplePosition(transform.position, out closestHit, 500, 1))
            {
                spawnPos = closestHit.position;
            }

            Unit newUnit = NetworkingService.Instance
                .Instantiate(TypeData.PrefabUnit.name, spawnPos, Quaternion.identity).GetComponent<Unit>();
            newUnit.RaiseSetDivision(this);
            return newUnit;
        }

        /// <summary>Add unit to the division's list of units</summary>
        public void AddUnitToDivision(Unit unit)
        {
            unit.GetComponent<UnitMovement>().division = this;
            Units.Add(unit);
            hadUnits = true;
            OnUnitCountChanged.Invoke(UnitCount);

            unit.SetMaterial(UnitMaterial, UnitTexture, UnitTexture);
            unit.OnStateChanged.AddListener(HasArrived);

            unit.onHeal.AddListener(onHeal.Invoke);
            unit.onHealthChanged.AddListener(OnHealthChanged);

            // Reassign idle order
            MoveTarget moveTarget =
                MoveTarget != null ? MoveTarget : new MoveTarget(transform.position, Vector3.forward);
            MoveOrder(moveTarget.Position, moveTarget.Direction);
        }

        private void OnHealthChanged(float f)
        {
            onHealthChanged.Invoke(GetDivisionHealth());
        }

        /// <summary>Destroy last unit in division's list of units</summary>
        public void DestroyUnit()
        {
            // Guard clause if there is no units
            if (Units.Count == 0)
            {
                return;
            }

            // Select last unit of units if no unit is passed
            Unit unit = Units[Units.Count - 1];

            unit.onHeal.RemoveListener(onHeal.Invoke);
            unit.onHealthChanged.RemoveListener(OnHealthChanged);

            // Destroy unit
            NetworkingService.Instance.Destroy(unit.NetworkedMovingObject);
        }

        /// <summary>Remove given unit from division's list of units</summary>
        public void RemoveUnitFromDivision(Unit unit)
        {
            // Guard clause if units doesn't contain unit
            if (!Units.Contains(unit))
            {
                return;
            }

            unit.OnStateChanged.RemoveListener(HasArrived);
            Units.Remove(unit);
            OnUnitCountChanged.Invoke(UnitCount);

            unit.onHeal.RemoveListener(onHeal.Invoke);
            unit.onHealthChanged.RemoveListener(OnHealthChanged);
        }

        /// <summary>Check if the division needs to be selectable, called by a unit when it's visibility changes</summary>
        public void CheckSelectable(bool unitVisible)
        {
            VisibleUnits = Units.Where(x => x.IsVisible).ToList();

            // Guard clause to exit if incoming unit change in unit visibility already equals the current status of selectableObject or if the division has not had units (e.g. when switching type)
            if (unitVisible == selectableObject.IsSelectable || !hadUnits)
            {
                return;
            }

            foreach (Unit unit in Units)
            {
                if (unit.IsVisible)
                {
                    SelectableObject.IsSelectable = true;
                    return;
                }
            }

            SelectableObject.IsSelectable = false;
        }

        /// <summary> Checks if a certain amount of units have arrived at the MoveTarget </summary>
        public void HasArrived(UnitState unitState, bool enabled)
        {
            // Guard clause to exit if the unitstate change is irrelevant (disable or not idle) or if the division goal is already idle
            if (!enabled || unitState != UnitState.Idle || Goal == DivisionGoal.Idle)
            {
                return;
            }

            int arrivedUnits = 0;
            foreach (Unit unit in Units)
            {
                if (unit.HasArrived)
                {
                    arrivedUnits++;
                }
            }

            // Switch to idle state once a percentage of the units have arrived
            if ((arrivedUnits / (float)Units.Count) * 100f >= unitsArrivedTillIdle)
            {
                IdleOrder();
            }
        }

        /// <summary>Raise event that sets movement speed of this unit to given speed</summary>
        public void RaiseSetMovementSpeed(float speed)
        {
            UnitSetMovementSpeedInfo info = new UnitSetMovementSpeedInfo
            {
                UnitViewID = NetworkedMovingObject.ViewId,
                Speed = speed
            };
            NetworkingService.RaiseEvent(NetworkingEventType.UnitSetMovementSpeed, info, EventReceivers.Other);
        }

        /// <summary>Raise Sync Typdata Event</summary>
        public void RaiseSyncTypeData()
        {
            SyncDivisionDataInfo info = new SyncDivisionDataInfo
            {
                divisionID = NetworkedMovingObject.ViewId,
                divisionTypeData = TypeData
            };
            NetworkingService.RaiseEvent(NetworkingEventType.SyncDivisionData, info, EventReceivers.Other);
        }

        /// <summary>Set movement speed of this unit to given speed, received from RaiseSetMovementSpeed</summary>
        private void SyncData(object content)
        {
            // Guard clause to exit if not connected and not in offline mode
            if (!NetworkingService.IsConnected && !NetworkingService.OfflineMode)
            {
                return;
            }

            SyncDivisionDataInfo info = (SyncDivisionDataInfo)content;

            // Guard clause to exit if viewID doesn't match this unit's viewID
            if (info.divisionID != NetworkedMovingObject.ViewId)
            {
                return;
            }

            Sprite icon = TypeData.Icon;
            GameObject prefab = TypeData.PrefabUnit;
            DivisionType divisionType = TypeData.Type;

            TypeData = info.divisionTypeData;
            TypeData.Icon = icon;
            TypeData.PrefabUnit = prefab;
            TypeData.Type = divisionType;
        }

        /// <summary>Set movement speed of this unit to given speed, received from RaiseSetMovementSpeed</summary>
        private void SetMovementSpeed(object content)
        {
            // Guard clause to exit if not connected and not in offline mode
            if (!NetworkingService.IsConnected && !NetworkingService.OfflineMode)
            {
                return;
            }

            UnitSetMovementSpeedInfo info = (UnitSetMovementSpeedInfo)content;

            // Guard clause to exit if viewID doesn't match this unit's viewID
            if (info.UnitViewID != NetworkedMovingObject.ViewId)
            {
                return;
            }

            DivisionSpeed = info.Speed;
        }


        public bool IsDivisionVisible()
        {
            foreach (Unit unit in Units)
            {
                if (unit.IsVisible)
                {
                    return true;
                }
            }

            return false;
        }
    }
}