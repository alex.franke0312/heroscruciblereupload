using System.Collections;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace GameStudio.HunterGatherer.Divisions
{
    public class UnitRegen : MonoBehaviour
    {
        [SerializeField] private Unit Unit;
        [SerializeField, Range(0, 1)] private float percentageRegen = 0.03f;
        [SerializeField] private float delayBetweenHeals = 1;

        [SerializeField] GameObject healthBarBackground;

        void Start()
        {
            StartCoroutine(Regen());
        }

        private IEnumerator Regen()
        {
            while (true)
            {
                if (Unit.Division.IsMine && Unit.State == UnitState.Idle &&
                    Unit.Health < Unit.Division.TypeData.MaxHealth)
                {
                    Unit.Heal(percentageRegen);
                    yield return new WaitForSeconds(delayBetweenHeals);
                }

                yield return new WaitForFixedUpdate();
            }
        }
    }
}