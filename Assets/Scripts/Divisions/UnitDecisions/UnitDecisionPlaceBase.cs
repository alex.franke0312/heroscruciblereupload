﻿namespace GameStudio.HunterGatherer.Divisions.UnitBehaviours
{
    /// <summary> Decision the hero uses to place a base</summary>
    public class UnitDecisionPlaceBase : UnitDecision
    {
        private UnitState stateWhenMoving = UnitState.Move;

        /// <summary> Execute the place base decision </summary>
        protected override void ExecuteDecision()
        {
            Unit.State = stateWhenMoving;
        }
    }
}
