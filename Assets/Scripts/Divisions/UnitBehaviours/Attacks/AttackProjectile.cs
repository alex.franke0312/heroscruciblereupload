﻿using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.Networking.Events;
using UnityEngine;

namespace GameStudio.HunterGatherer.Divisions.UnitBehaviours.UnitAttacks
{
    /// <summary>Ranged projectile attack pattern, triggered by calling trigger attack</summary>
    /// Note by team Tech Shack; previous team(s) made the blocking mechanic really ugly, and we've managed to fix it only somewhat. Sorry!
    public class AttackProjectile : MonoBehaviour
    {
        [Header("References")]
        [SerializeField]
        public Unit unit = null;

        [SerializeField]
        private Projectile prefabProjectile = null;

        private void OnEnable()
        {
            if (NetworkingService.IsConnected || NetworkingService.OfflineMode)
            {
                NetworkingService.AddNetworkingEventListener(NetworkingEventType.UnitFireProjectile, FireProjectile);
            }
        }

        private void OnDisable()
        {
            if (NetworkingService.IsConnected || NetworkingService.OfflineMode)
            {
                NetworkingService.RemoveNetworkingEventListener(NetworkingEventType.UnitFireProjectile, FireProjectile);
            }
        }

        /// <summary>Check if we hit, miss or block and send a projectile event that will be handled locally in FireProjectile</summary>
        public void TriggerAttack()
        {
            UnitFireProjectileInfo info = new UnitFireProjectileInfo
            {
                TargetViewID = unit.AttackTarget.NetworkedMovingObject.ViewId,
                AttackerViewID = unit.NetworkedMovingObject.ViewId,
                Damage = 0
            };

            bool blocked = unit.AttackTarget.Division.TypeData.HitsSinceLastBlock >=
                           unit.AttackTarget.Division.TypeData.HitsPerBlock;
            if (blocked)
            {
                info.HitType = HitType.Block;
            }
            else
            {
                info.HitType = HitType.Hit;
                info.Damage = unit.Division.TypeData.Damage;
            }

            NetworkingService.RaiseEvent(NetworkingEventType.UnitFireProjectile, info, EventReceivers.All);
        }

        /// <summary>Received locally by event from TriggerAttack, making the projectile and letting it fly</summary>
        private void FireProjectile(object content)
        {
            // Guard clause to exit if not connected and not in offline mode
            if (!NetworkingService.IsConnected && !NetworkingService.OfflineMode)
            {
                return;
            }

            UnitFireProjectileInfo info = (UnitFireProjectileInfo)content;

            // Guard clause to exit if attacker viewID doesn't match this unit's viewID
            if (info.AttackerViewID != unit.NetworkedMovingObject.ViewId)
            {
                return;
            }

            GameObject targetObject = NetworkingResources.Find(info.TargetViewID);

            // Guard clause to exit if targetObject is null
            if (targetObject == null)
            {
                return;
            }

            // Fire projectile towards enemy
            Projectile projectile = Instantiate(prefabProjectile, transform.position, Quaternion.identity).GetComponent<Projectile>();
            projectile.Fire(unit, targetObject.GetComponent<Unit>(), info.HitType);
        }
    }
}