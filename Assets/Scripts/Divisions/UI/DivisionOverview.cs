﻿using GameStudio.HunterGatherer.CustomEvents;
using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.Networking.Events;
using GameStudio.HunterGatherer.Structures;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameStudio.HunterGatherer.Divisions.UI
{
    /// <summary>Handles the UI element displaying the player's divisions</summary>
    public class DivisionOverview : MonoBehaviour
    {
        [Header("References")]
        [SerializeField]
        private DivisionOverviewItem prefabOverviewItem = null;

        [SerializeField]
        private DivisionOverviewItemEmpty prefabOverviewItemEmpty = null;

        [SerializeField]
        private RectTransform overviewItemParent = null;

        public Dictionary<Division,DivisionOverviewItem> divisionItems = new Dictionary<Division, DivisionOverviewItem>();
        List<DivisionOverviewItemEmpty> emptyDivisionSlots = new List<DivisionOverviewItemEmpty>();
        private Base baseBeingEntered;
        private bool _enemyInBase;
        private bool _heroInBase;

        private int indexID = 0;

        public static DivisionOverview Instance { get; private set; }
        public UnityEventInt OnAddDivisionItem { get; } = new UnityEventInt();
        public UnityEvent OnDivisionsInBaseChanged { get; } = new UnityEvent();
        public bool HeroIsInBase
        {
            get
            {
                return _heroInBase;
            }
            set
            {
                _heroInBase = value;
                OnDivisionsInBaseChanged.Invoke();
            }
        }
        public bool EnemyIsInBase
        {
            get
            {
                return _enemyInBase;
            }
            set
            {
                if (_enemyInBase != value)
                {
                    _enemyInBase = value;
                    OnDivisionsInBaseChanged.Invoke();
                }
            }
        }

        private void Start()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
            NetworkingPlayerManager.Instance.OnDivisionCreated.AddListener(AddDivisionItem);
        }

        /// <summary>Create a new item for the given division and add it to the display</summary>
        private void AddDivisionItem(Division division)
        {
            

            // Guard clause to return if an item for the given division already exists
            if (divisionItems.ContainsKey(division)) //TODO: manpower amount check doesn't belong here
            {
                return;
            }

            DivisionOverviewItem newItem = Instantiate(prefabOverviewItem, overviewItemParent).GetComponent<DivisionOverviewItem>();
            newItem.Setup(division);
            newItem.id = indexID++;
            divisionItems.Add(division, newItem);
            division.OnDisableDivision.AddListener(RemoveDivisionItem);
            if (division.Type == DivisionType.Hero)
            {
                division.OnEnterExitBase.AddListener(HeroEntersExitsBase);
            }
           
            // Reenter empty items
            for (int i = emptyDivisionSlots.Count - 1; i >= 0; i--)
            {
                DivisionOverviewItemEmpty emptyItem = emptyDivisionSlots[i];
                emptyDivisionSlots.Remove(emptyItem);
                Destroy(emptyItem.gameObject);
            }
            int amountOfEmptyDivisions = NetworkingPlayerManager.Instance.MaxDivisions - NetworkingPlayerManager.Instance.PlayerDivisions.Count;
            
            for (int i = 0; i < amountOfEmptyDivisions; i++)
            {
                DivisionOverviewItemEmpty emptyItem = Instantiate(prefabOverviewItemEmpty, overviewItemParent).GetComponent<DivisionOverviewItemEmpty>();
                emptyItem.ToggleButton(HeroIsInBase, baseBeingEntered);
                emptyDivisionSlots.Add(emptyItem);
            }
            int amountOfManpowerToRemove = 1;
            OnAddDivisionItem.Invoke(amountOfManpowerToRemove);

            //Binding the division overviewItem to the division
            division.overviewItem = newItem;
        }

        /// <summary>Remove the item belonging to the given division from the display and destroy it</summary>
        private void RemoveDivisionItem(Division division)
        {
            // Guard clause to return if no item exists for the given division
            if (!divisionItems.ContainsKey(division))
            {
                return;
            }

            division.OnDisableDivision.RemoveListener(RemoveDivisionItem);
            if (division.Type == DivisionType.Hero)
            {
                division.OnEnterExitBase.RemoveListener(HeroEntersExitsBase);
            }
            DivisionOverviewItem item = divisionItems[division];
            divisionItems.Remove(division);
            Destroy(item.gameObject);
           
            // Add new empty item to replace division
            DivisionOverviewItemEmpty emptyItem = Instantiate(prefabOverviewItemEmpty, overviewItemParent).GetComponent<DivisionOverviewItemEmpty>();
            emptyItem.ToggleButton(HeroIsInBase, baseBeingEntered);
            emptyDivisionSlots.Add(emptyItem);
        }

        /// <summary>Handle what needs to happen when the hero division enters or exits the base</summary>
        private void HeroEntersExitsBase(bool shouldBeEnabled, Base baseBeingEntered)
        {
            HeroIsInBase = shouldBeEnabled;
            this.baseBeingEntered = baseBeingEntered;
            foreach (DivisionOverviewItemEmpty item in emptyDivisionSlots)
            {
                item.ToggleButton(shouldBeEnabled, baseBeingEntered);
            }
        }
    }
}