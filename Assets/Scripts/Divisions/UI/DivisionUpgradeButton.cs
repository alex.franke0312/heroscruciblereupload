using System;
using GameStudio.HunterGatherer.Divisions.Upgrades;
using GameStudio.HunterGatherer.Structures;
using UnityEngine;
using UnityEngine.UI;

namespace GameStudio.HunterGatherer.Divisions.UI
{
    public class DivisionUpgradeButton : MonoBehaviour
    {
        [SerializeField] private DivisionOverviewItem divisionOverviewItem;
        [SerializeField] private Button upgradeButton;
        [SerializeField] private string setButtonActive = "OnUpgradeRange";
        [SerializeField] private string upgradeDivision = "OnUpgraded";
        [SerializeField] private string hideUI = "OnHideUI";
        [SerializeField] private UpgradeBase divisionUpgrade;
        [SerializeField] private UpgradeBase heroUpgrade;
        private StructureInteract structureInteract;
        private bool isHero;
        private string heroName;

        /// <summary>
        /// create events for upgrading a division and showing the upgrade buttons
        /// </summary>
        private void Start()
        {
            Division buttonDivision = divisionOverviewItem.division.NetworkedMovingObject.GetComponent<Division>();
            if (buttonDivision)
            {
                isHero = (buttonDivision.Type == DivisionType.Hero);
                heroName = buttonDivision.Type.ToString();
            }
            EventManager.Instance.AddEvent(setButtonActive, true);
            EventManager.Instance.AddEvent(upgradeDivision, true);
            EventManager.Instance.AddEvent(hideUI, true);
            EventManager.Instance.AddListener(setButtonActive, SetUIActiveOnRange);
            EventManager.Instance.AddListener(hideUI, DisableUIUpgrade);
            upgradeButton.gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            EventManager.Instance.RemoveListener(setButtonActive, SetUIActiveOnRange);
            EventManager.Instance.RemoveListener(hideUI, DisableUIUpgrade);
        }

        public void Upgrade()
        {
            //Determine which upgrade type to use.
            UpgradeBase upgrade = divisionUpgrade;
            if (isHero)
                upgrade = heroUpgrade;

            //invoke the event that should upgrade the division.
            UpgradeEventArgs upgradeArgs =
                new UpgradeEventArgs(divisionOverviewItem.division.NetworkedMovingObject, upgrade);

            EventManager.Instance.Invoke(upgradeDivision, upgradeArgs);
            structureInteract.Use();
            divisionOverviewItem.upgrades++;
            HideUpgradeButtonsEventArgs hideUIArgs = new HideUpgradeButtonsEventArgs(isHero);
            EventManager.Instance.Invoke(hideUI, hideUIArgs);
        }

        /// <summary>
        /// Set the UI active and inactive based on a boolean provided in the DivisionListEventArgs class.
        /// </summary>
        /// <param name="a">the DivisionListEventArgs that holds a boolean for activating or deactivating the buttons.</param>
        public void SetUIActiveOnRange(EventArgs a)
        {
            if (divisionOverviewItem.upgrades >= divisionOverviewItem.maxUpgrades)
                return;
            DivisionListEventArgs divisionListEventArgs = (DivisionListEventArgs)a;
            bool isValidDivision = false;
            if (isHero)
                isValidDivision = (divisionListEventArgs.division.GetComponent<Division>().SelectableObject.ObjectName == heroName);
            else
                isValidDivision = divisionListEventArgs.division.Equals(divisionOverviewItem.division.NetworkedMovingObject);
            if (isValidDivision)
            {
                structureInteract = divisionListEventArgs.structureInteract;
                upgradeButton.gameObject.SetActive(divisionListEventArgs.setActive);
            }
        }

        public void DisableUIUpgrade(EventArgs a)
        {
            HideUpgradeButtonsEventArgs args = (HideUpgradeButtonsEventArgs)a;
            //Stops the temple and blacksmith from disabling eachothers UI if both are visible.
            if (isHero == args.isHero)
                upgradeButton.gameObject.SetActive(false);
        }
    }
}