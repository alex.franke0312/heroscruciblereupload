﻿using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.Structures;
using UnityEngine;
using UnityEngine.UI;

namespace GameStudio.HunterGatherer.Divisions.UI
{
    /// <summary>Handles an empty slot in the DivisionOverview, displaying a plus when the hero is in the base to add a new division</summary>
    public class DivisionOverviewItemEmpty : MonoBehaviour
    {
        [Header("References")]
        [SerializeField]
        private Button btnAddDivision = null;

        [SerializeField]
        private float divisionSpawnMaxOffset = 5f;

        private Vector3 spawnPosition;
        private Image[] btnImages;

        private void Awake()
        {
            btnImages = btnAddDivision.GetComponentsInChildren<Image>();
            btnAddDivision.onClick.AddListener(SpawnDivision);
        }

        private void OnDestroy()
        {
        }

        /// <summary>Toggle the button to the given bool and set the spawnpoint to the given base</summary>
        public void ToggleButton(bool shouldBeActive, Base baseBeingEntered)
        {
            btnAddDivision.gameObject.SetActive(shouldBeActive);
            if (baseBeingEntered != null)
            {
                spawnPosition = baseBeingEntered.transform.position;
            }
            else
            {
                // Default position if no base is passed
                spawnPosition = NetworkingPlayerManager.Instance.HeroDivision.transform.position;
            }
        }

        /// <summary>Spawn division at the set spawn position</summary>
        private void SpawnDivision()
        {
            Vector3 spawnPosition = this.spawnPosition + new Vector3(Random.Range(-divisionSpawnMaxOffset, divisionSpawnMaxOffset), 0, Random.Range(-divisionSpawnMaxOffset, divisionSpawnMaxOffset));
            NetworkingPlayerManager.Instance.SpawnDivision(spawnPosition, Quaternion.identity);
        }

        /// <summary> Set the interactable state of the add division buttons </summary>
        public void SetButtonState(bool state)
        {
            Color editBtnColor = (state) ? Color.white : Color.red;


            foreach (Image i in btnImages)
            {
                i.color = editBtnColor;
            }
        }
    }
}