﻿using GameStudio.HunterGatherer.Networking.Events;
using GameStudio.HunterGatherer.Utilities;
using UnityEngine;

namespace GameStudio.HunterGatherer.Divisions
{
    /// <summary>Handles a projectile flying from a start position to an end position, with the option of tracking the target</summary>
    public class Projectile : MonoBehaviour
    {
        [SerializeField]
        private Vector3 unitOffset = Vector3.up;

        [SerializeField]
        private float distanceToTimeRatio = 15f;

        [SerializeField]
        private float distanceToHeightRatio = 3f;

        private Unit attackerUnit;
        private Unit targetUnit;
        private HitType hitType;
        private Vector3 startPos;
        private Vector3 endPos;
        private Vector3 lastPos;
        private bool isFlying;
        private float flyTimeTotal;
        private float flyTimeCurrent;
        private float height;

        private void Update()
        {
            // Return if the projectile isn't flying yet/anymore
            if (!isFlying || targetUnit == null)
            {
                return;
            }

            // Keep updating endPos if the projectile will hit (= target tracking)
            if (hitType != HitType.Miss)
            {
                endPos = targetUnit.transform.position + unitOffset;
            }

            // Move and rotate
            lastPos = transform.position;
            transform.position = Parabola.Lerp(startPos, endPos, height, flyTimeCurrent / flyTimeTotal);
            transform.LookAt(transform.position + transform.position - lastPos);

            // Increase time, check if arrived
            flyTimeCurrent += Time.deltaTime;
            if (flyTimeCurrent >= flyTimeTotal)
            {
                //targetUnit.Hit(1, originUnit);
                int damage = hitType == HitType.Hit ? 1 : 0;
                UnitHitInfo unitHitInfo = new UnitHitInfo
                {
                    AttackerViewID = attackerUnit.NetworkedMovingObject.ViewId,
                    UnitViewID = targetUnit.NetworkedMovingObject.ViewId,
                    HitType = hitType,
                    Damage = damage,
                    KnockbackDistance = attackerUnit.Division.TypeData.KnockbackDistance,
                    KnockbackDuration = attackerUnit.Division.TypeData.KnockbackDuration
                };
                targetUnit.Hit(unitHitInfo);
                isFlying = false;
                Destroy(gameObject);
            }
        }

        /// <summary>Fire the projectile by giving it a start and end location, and giving the determined result (hit, miss or block)</summary>
        public void Fire(Unit attackerUnit, Unit targetUnit, HitType hitType)
        {
            this.attackerUnit = attackerUnit;
            this.targetUnit = targetUnit;
            this.hitType = hitType;

            startPos = attackerUnit.transform.position + unitOffset;
            endPos = targetUnit.transform.position;
            flyTimeTotal = Vector3.Distance(startPos, endPos) / distanceToTimeRatio;
            height = Vector3.Distance(startPos, endPos) / distanceToHeightRatio;
            isFlying = true;
        }
    }
}