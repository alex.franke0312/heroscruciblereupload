﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameStudio.HunterGatherer.Divisions
{
    public class SingleDivisionTypeData
    {
        public string TypeName = string.Empty;
        public DivisionType Type = DivisionType.Swordsmen;
        public Sprite Icon = null;
        public GameObject PrefabUnit = null;
        public int MaxUnitCount = 24;
        public float MovementSpeed = 4f;
        public float MovementSpeedRunning = 8f;
        public float DistanceFromEnemiesToStartRunning = 0f;
        public float MoveSpeedUpTime = 10f;
        public float MoveSpeedUpFinishedTime = 15f;
        public float MoveSpeedIncreasePercent = 0.5f;
        public float Damage = 1;
        public float HitChance = 0.5f;
        public float Range = 3f;
        public float AutoAttackRange = 10f;
        public float Cooldown = 2f;
        public float KnockbackDistance = 3f;
        public float KnockbackDuration = 0.5f;
        public float ChargeUpTime = 0.5f;
        public float MaxHealth = 1;
        public float KnockbackTenacity = 0f;
        public float BlockChanceMelee = 0f;
        public float BlockChanceProjectile = 0f;
        public float KnockbackFactor => 1f - KnockbackTenacity;
        public int HitsPerBlock = 4;
        public int HitsSinceLastBlock = 0;

        public SingleDivisionTypeData(DivisionTypeData typeData)
        {
            TypeName = typeData.TypeName;
            Type = typeData.Type;
            Icon = typeData.Icon;
            PrefabUnit = typeData.PrefabUnit;
            MaxUnitCount = typeData.MaxUnitCount;
            MovementSpeed = typeData.MovementSpeed;
            MovementSpeedRunning = typeData.MovementSpeedRunning;
            DistanceFromEnemiesToStartRunning = typeData.DistanceFromEnemiesToStartRunning;
            MoveSpeedUpTime = typeData.MoveSpeedUpTime;
            MoveSpeedUpFinishedTime = typeData.MoveSpeedUpFinishedTime;
            MoveSpeedIncreasePercent = typeData.MoveSpeedIncreasePercent;
            Damage = typeData.Damage;
            HitChance = typeData.HitChance;
            Range = typeData.Range;
            AutoAttackRange = typeData.AutoAttackRange;
            Cooldown = typeData.Cooldown;
            KnockbackDistance = typeData.KnockbackDistance;
            KnockbackDuration = typeData.KnockbackDuration;
            ChargeUpTime = typeData.ChargeUpTime;
            MaxHealth = typeData.MaxHealth;
            KnockbackTenacity = typeData.KnockbackTenacity;
            BlockChanceMelee = typeData.BlockChanceMelee;
            BlockChanceProjectile = typeData.BlockChanceProjectile;
            HitsPerBlock = typeData.HitsPerBlock;
            HitsSinceLastBlock = typeData.HitsSinceLastBlock;
        }


    }
}
