using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameStudio.HunterGatherer.Divisions.Upgrades
{
    /// <summary>
    /// Script that holds the functionality of the Upgrade system.
    /// It listens to an Event System for a Division and a specific Upgrade, then updates the Division's stats.
    /// </summary>
    public class UpgradeFunctionality : MonoBehaviour
    {
        private void OnEnable()
        {
            EventManager.Instance.AddListener("OnUpgraded", Upgrade);
            //All divisions are part of a DDOL parent, so our functionality has to be too.
        }

        private void OnDisable()
        {
            EventManager.Instance.RemoveListener("OnUpgraded", Upgrade);
        }

        public void Upgrade(EventArgs e)
        {
            UpgradeEventArgs networkedArgs = (UpgradeEventArgs)e;
            Division d = networkedArgs.NetworkedMovingObject.GetComponent<Division>();
            if (d != null)
            {
                Upgrade(networkedArgs.NetworkedMovingObject.GetComponent<Division>(), networkedArgs.Upgrade);
            }
        }

        public void Upgrade(Division division, UpgradeBase upgrade)
        {
            UpdateValues(division.TypeData, upgrade);
            division.RaiseSyncTypeData();
        }


        private void UpdateValues(SingleDivisionTypeData divisionTypeData, UpgradeBase upgrade)
        {
            //For the love of god, TODO: please find a more efficient way of collecting all floats from another script
            divisionTypeData.MovementSpeed += upgrade.MovementSpeed;
            divisionTypeData.MovementSpeedRunning += upgrade.MovementSpeedRunning;
            divisionTypeData.DistanceFromEnemiesToStartRunning += upgrade.DistanceFromEnemiesToStartRunning;
            divisionTypeData.MoveSpeedUpTime += upgrade.MoveSpeedUpTime;
            divisionTypeData.MoveSpeedUpFinishedTime += upgrade.MoveSpeedUpFinishedTime;
            divisionTypeData.MoveSpeedIncreasePercent += upgrade.MoveSpeedIncreasePercent;

            divisionTypeData.Damage += upgrade.Damage;
            divisionTypeData.HitChance = Mathf.Clamp(divisionTypeData.HitChance + upgrade.HitChance, 0, 1);
            divisionTypeData.Range += upgrade.Range;
            divisionTypeData.AutoAttackRange += upgrade.AutoAttackRange;
            divisionTypeData.Cooldown += upgrade.Cooldown;
            divisionTypeData.KnockbackDistance += upgrade.KnockbackDistance;
            divisionTypeData.KnockbackDuration += upgrade.KnockbackDuration;
            divisionTypeData.ChargeUpTime += upgrade.ChargeUpTime;

            divisionTypeData.MaxHealth += upgrade.MaxHealth;
            AtLeast(divisionTypeData.MaxHealth, 1);
            divisionTypeData.KnockbackTenacity = Mathf.Clamp(divisionTypeData.KnockbackTenacity + upgrade.KnockbackTenacity, 0, 1);
            divisionTypeData.BlockChanceMelee = Mathf.Clamp(divisionTypeData.BlockChanceMelee + upgrade.BlockChanceMelee, 0, 1);
            divisionTypeData.BlockChanceProjectile = Mathf.Clamp(divisionTypeData.BlockChanceProjectile + upgrade.BlockChanceProjectile, 0, 1);
            divisionTypeData.HitsPerBlock += upgrade.HitsPerBlock;

        }
        //Use when an upgraded value has to have a minimal value (i.e. can't be negative)
        private void AtLeast(int value, int targetedValue)
        {
            if (value <= targetedValue) value = targetedValue;
        }

        private void AtLeast(float value, float targetedValue)
        {
            if (value <= 1) value = targetedValue;
        }


    }
}