﻿using System.Collections.Generic;
using UnityEngine;

namespace GameStudio.HunterGatherer.Divisions
{
    /// <summary>ScriptableObject that holds color values for divisions and units<summary>
    [CreateAssetMenu(fileName = "DivisionColorPalette", menuName = "ScriptableObjects/DivisionColorPalette")]
    public class DivisionColorPalette : ScriptableObject
    {
        [SerializeField]
        private List<DivisionTextureColorPair> textureColorPairs = new List<DivisionTextureColorPair>();

        [SerializeField]
        private Color enemyFlag = Color.gray;

        [SerializeField]
        private Color friendlyUnit = new Color(0.8f, 0.8f, 0.8f);

        [SerializeField]
        private Color enemyUnit = new Color(0.2f, 0.2f, 0.2f);

        private int index;

        public Color EnemyFlag => enemyFlag;
        public Color FriendlyUnit => friendlyUnit;
        public Color EnemyUnit => enemyUnit;

        /// <summary>Return next color from friendly divisions list<summary>
        public Color GetFriendlyDivisionColor()
        {
            Color color = textureColorPairs[index].divisionColor;
            index++;
            if (index >= textureColorPairs.Count)
            {
                index = 0;
            }
            return color;
        }

        /// <summary>Return next texture from friendly divisions list<summary>
        public Texture GetFriendlyDivisionTexture()
        {
            Texture texture = textureColorPairs[index].divisionTexture;
            index++;
            if(index >= textureColorPairs.Count)
            {
                index = 0;
            }
            return texture;
        }

        /// <summary>Return next texture from friendly divisions list<summary>
        public Texture GetFriendlyDivisionTexture(Color color)
        {
            return textureColorPairs.Find(x => x.divisionColor == color)?.divisionTexture;
        }

        public Texture GetFriendlyFlagTexture(Color color, DivisionType divisionType) {
            if(divisionType == DivisionType.Archers) {
                return textureColorPairs.Find(x => x.divisionColor == color)?.archerFlagTexture;
            } else if(divisionType == DivisionType.Pikemen) {
                return textureColorPairs.Find(x => x.divisionColor == color)?.pikemanFlagTexture;
            } else if(divisionType == DivisionType.Swordsmen) {
                return textureColorPairs.Find(x => x.divisionColor == color)?.swordsmanFlagTexture;
            } else {
                return textureColorPairs.Find(x => x.divisionColor == color)?.heroFlagTexture;
            }
        }
    }
}