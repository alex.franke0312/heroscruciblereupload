﻿using GameStudio.HunterGatherer.GodFavor;
using GameStudio.HunterGatherer.GodFavor.UI;
using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.Networking.Events;
using GameStudio.HunterGatherer.Networking.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class SelectedGods : MonoBehaviour
{
    private static SelectedGods _instance;

    public static SelectedGods Instance { get { return _instance; } }

    [HideInInspector] public Dictionary<int, GodPowerManager.Gods> selectedGods;

    [SerializeField] private PlayerListHandler playerListHandler;

    [Header("Local Visuals")]
    public Sprite[] godIcons;
    public GameObject[] godIconObjects;

    [SerializeField]
    private GameObject godSelectBackground;
    [SerializeField]
    private GameObject godName;
    [SerializeField]
    private GameObject instruction;
    [SerializeField]
    private VideoClip[] instructionVideos;
    [SerializeField]
    private GameObject godDetails;

    private void Awake()
    {
        //NetworkingService.Instance.JoinedRoom += OnJoinedRoom;
        //NetworkingService.Instance.PlayerJoinedRoom += OnPlayerJoinedRoom;
        //NetworkingService.Instance.PlayerLeftRoom += OnPlayerLeftRoom;
        // NetworkingService.AddNetworkingEventListener(NetworkingEventType.GodSet, SetGod);

        if (_instance != null && _instance != this)
        {
            Destroy(_instance.gameObject);
        }

        _instance = this;

        DontDestroyOnLoad(gameObject);
        selectedGods = new Dictionary<int, GodPowerManager.Gods>();
    }

    private void OnJoinedRoom()
    {
        if (NetworkingService.IsConnected || NetworkingService.OfflineMode)
        {
            selectedGods = new Dictionary<int, GodPowerManager.Gods>();
            Instance.playerListHandler.Open();
            RaiseSetGod(GodFavorUI.CurrentGod);
        }
    }

    private void OnPlayerLeftRoom()
    {
        List<KeyValuePair<int, GodPowerManager.Gods>> leftPlayers = selectedGods.Where(x => NetworkingService.PlayerList.All(pl => pl.ActorNumber != x.Key)).ToList();
        foreach (KeyValuePair<int, GodPowerManager.Gods> leftPlayer in leftPlayers)
        {
            Debug.Log($"Player with id: {leftPlayer.Key} left the room.");
            selectedGods.Remove(leftPlayer.Key);
        }
    }

    private void OnPlayerJoinedRoom()
    {
        RaiseSetGod(GodFavorUI.CurrentGod);
    }

    private void OnDisable()
    {
        if (NetworkingService.IsConnected || NetworkingService.OfflineMode)
        {
            NetworkingService.RemoveNetworkingEventListener(NetworkingEventType.GodSet, SetGod);
        }
    }

    public void RaiseSetGod(GodPowerManager.Gods god)
    {
        GodSetInfo info = new GodSetInfo { actorNumber = NetworkingService.LocalPlayer.ActorNumber, GodType = god };
        NetworkingService.RaiseEvent(NetworkingEventType.GodSet, info, EventReceivers.Other);
        SetGod(info);
    }

    private void SetGod(object content)
    {
        if (!NetworkingService.IsConnected && !NetworkingService.OfflineMode || NetworkingService.OfflineMode)
        {
            return;
        }

        GodSetInfo info = (GodSetInfo)content;

        if (selectedGods.ContainsKey(info.actorNumber))
        {
            selectedGods[info.actorNumber] = info.GodType;
        }
        else
        {
            selectedGods.Add(info.actorNumber, info.GodType);
        }

        for (int i = 0; i < Instance.playerListHandler.players.Length; i++)
        {
            if (Instance.playerListHandler.players[i]?.ActorNumber == info.actorNumber)
            {
                AssignGodsToIcons(info, i);
            }
        }
    }

    private void AssignGodsToIcons(GodSetInfo info, int row)
    {
        if (info.actorNumber == NetworkingService.LocalPlayer.ActorNumber)
        {
            ChangeLocalSelectScreen(info);
        }

        Debug.Log($"{godIconObjects[row].name}");
        Instance.godIconObjects[row].SetActive(true);
        switch (info.GodType)
        {
            case GodPowerManager.Gods.Ares:
                Instance.godIconObjects[row].GetComponent<Image>().sprite = godIcons[0];
                break;
            case GodPowerManager.Gods.Athena:
                Instance.godIconObjects[row].GetComponent<Image>().sprite = godIcons[1];
                break;
            case GodPowerManager.Gods.Zeus:
                Instance.godIconObjects[row].GetComponent<Image>().sprite = godIcons[2];
                break;
        }
    }

    private void ChangeLocalSelectScreen(GodSetInfo info)
    {
        Color32 aresColor = new Color32(99, 13, 3, 75);
        Color32 athenaColor = new Color32(166, 153, 90, 75);
        Color32 zeusColor = new Color32(163, 201, 220, 75);

        string aresName = "Ares";
        string athenaName = "Athena";
        string zeusName = "Zeus";

        string aresDetails = "Ares provides his followers with a rage boost that greatly improves their hit chance.";
        string athenaDetails = "Athena provides her followers with a floating shield that blocks even the mighthiest projectiles.";
        string zeusDetails = "Zeus allows his followers to summon a lightning bolt with great destructive power.";

        switch (info.GodType)
        {
            case GodPowerManager.Gods.Ares:
                Instance.godSelectBackground.GetComponent<Image>().color = aresColor;
                Instance.godName.GetComponent<TextMeshProUGUI>().text = aresName;
                Instance.instruction.GetComponent<VideoPlayer>().clip = instructionVideos[0];
                Instance.godDetails.GetComponent<TextMeshProUGUI>().text = aresDetails;
                break;
            case GodPowerManager.Gods.Athena:
                Instance.godSelectBackground.GetComponent<Image>().color = athenaColor;
                Instance.godName.GetComponent<TextMeshProUGUI>().text = athenaName;
                Instance.instruction.GetComponent<VideoPlayer>().clip = instructionVideos[1];
                Instance.godDetails.GetComponent<TextMeshProUGUI>().text = athenaDetails;
                break;
            case GodPowerManager.Gods.Zeus:
                Instance.godSelectBackground.GetComponent<Image>().color = zeusColor;
                Instance.godName.GetComponent<TextMeshProUGUI>().text = zeusName;
                Instance.instruction.GetComponent<VideoPlayer>().clip = instructionVideos[2];
                Instance.godDetails.GetComponent<TextMeshProUGUI>().text = zeusDetails;
                break;
        }
    }
}
