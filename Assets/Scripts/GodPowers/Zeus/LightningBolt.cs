﻿using GameStudio.HunterGatherer.Networking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningBolt : GodPowers
{
    [SerializeField]
    public int damageAmount = 2;
    [SerializeField]
    private float lightningBoltDuration = 0.5f;
    public NetworkedMovingObject lightningBoltWarning;

    protected override void OnEnable()
    {
        base.OnEnable();
        StartCoroutine(Die(lightningBoltDuration));
    }
}
