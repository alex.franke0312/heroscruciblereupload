﻿using GameStudio.HunterGatherer.Divisions;
using GameStudio.HunterGatherer.Networking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameStudio.HunterGatherer.GodFavor
{
    public class GodShield : GodPowers
    {
        [SerializeField] private float godShieldDuration = 7.5f;

        private void OnEnable()
        {
            StartCoroutine(Die(godShieldDuration));
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.GetComponent<Projectile>())
            {
                Destroy(other.gameObject);
            }
        }
    }
}
