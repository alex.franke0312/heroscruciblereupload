﻿using GameStudio.HunterGatherer.Divisions;
using GameStudio.HunterGatherer.GodFavor.UI;
using GameStudio.HunterGatherer.Networking;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class InteractBehaviour : MonoBehaviour
{
    public UnityEvent OnInteract { get; } = new UnityEvent();

    protected GameObject item;
    protected NetworkedMovingObject networkMovingObject;

    protected virtual void Awake()
    {
        OnInteract.AddListener(Interact);
    }

    protected virtual void Interact()
    {
        // general interact stuff

        // play sound

        // animation
    }
}
