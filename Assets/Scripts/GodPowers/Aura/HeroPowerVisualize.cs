﻿using UnityEngine;
using GameStudio.HunterGatherer.GodFavor.UI;
using GameStudio.HunterGatherer.Networking;

public class HeroPowerVisualize : MonoBehaviour
{
    [SerializeField]
    private NetworkedMovingObject networkedMovingObject = null;
    public NetworkedMovingObject NetworkedMovingObject => networkedMovingObject;

    [SerializeField]
    private GameObject hero;

    [SerializeField]
    private GameObject auraInput;

    private GameObject aura;

    [SerializeField]
    private int powerCost = 20;

    [SerializeField]
    private float offset = 0.5f;

    private bool auraExists => aura != null;

    private Vector3 heroPosition;

    [SerializeField, Tooltip("Starting god power for a player")]
    private int startingGodPower = 20;

    void Start()
    {
        hero = this.gameObject;
        heroPosition = new Vector3(hero.transform.position.x, hero.transform.position.y + offset, hero.transform.position.z);
        GodFavorUI.Instance.OnGodFavorAmountChanged.AddListener(GodFavorChange);
        if (networkedMovingObject.IsMine)
        {
            GodFavorChange(startingGodPower);
        }
    }

    void Update()
    {
        //Update the aura position based on the hero
        if (networkedMovingObject != null && auraExists)
        {
            aura.transform.position = new Vector3(hero.transform.position.x, hero.transform.position.y + offset, hero.transform.position.z);
        }
    }

    //Return whether the GodPower is usable
    private bool CanUsePower()
    {
        return GodFavorUI.Instance.Amount >= powerCost;
    }

    private void GodFavorChange(float amount)
    {
        //Instantiate the aura when you can use your god power, destroy it when you cannot
        if (CanUsePower() && !auraExists && networkedMovingObject.IsMine)
        {
            aura = NetworkingService.Instance.Instantiate(auraInput.name, heroPosition, Quaternion.identity);
        }
        else if (!CanUsePower() && auraExists && networkedMovingObject.IsMine)
        {
            NetworkingService.Instance.Destroy(aura.GetComponent<NetworkedMovingObject>());
            aura = null;
        }
    }
}