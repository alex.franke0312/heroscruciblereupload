﻿using GameStudio.HunterGatherer.Networking;
using System.Collections;
using System.Collections.Generic;
using GameStudio.HunterGatherer.GodFavor;
using UnityEngine;

public class GodPowers : MonoBehaviour
{
    public NetworkedMovingObject networkedMovingObject;

    [Header("Screen shake")]
    [SerializeField]
    private bool applyScreenShake;
    [SerializeField]
    private float duration = 1f;
    [SerializeField]
    private float magnitude = 1f;


    protected virtual void Awake()
    {
        networkedMovingObject = GetComponent<NetworkedMovingObject>();
    }

    protected virtual void OnEnable()
    {
        if (applyScreenShake)
        {
            DoScreenShake();
        }
    }

    protected IEnumerator Die(float duration)
    {
        if (networkedMovingObject.IsMine)
        {
            yield return new WaitForSeconds(duration);
            NetworkingService.Instance.Destroy(networkedMovingObject);
            GodPowerManager.activeManager.IsPlacingGodPower = false;
        }
    }

    protected void DoScreenShake()
    {
        StartCoroutine(ScreenShake.Instance.Shake(duration, magnitude, new Vector2(transform.position.x, transform.position.z)));
    }
}
