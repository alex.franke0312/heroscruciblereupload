using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameStudio.HunterGatherer.GodFavor.UI
{
    public class GodFavorDot : MonoBehaviour
    {
        [SerializeField] GameObject visual;
        public GameObject Visual => visual;

        [SerializeField] bool status;
        public bool Status => status;

        public void SetStatus(bool status)
        {
            this.status = status;
            visual.SetActive(this.status);
        }
        public void ToggleStatus()
        {
            status = !status;
            visual.SetActive(status);
        }
    }
}