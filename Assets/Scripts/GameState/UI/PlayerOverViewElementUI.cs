﻿using GameStudio.HunterGatherer.Networking;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameStudio.HunterGatherer.GameState.UI
{
    /// <summary>Handles the UI for the player overview element</summary>
    public class PlayerOverViewElementUI : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField]
        private string offlineModeName = "You";

        [SerializeField]
        private Color backgroundWinColor = Color.white;

        [SerializeField]
        private Color backgroundPlayingColor = Color.white;

        [SerializeField]
        private Color backgroundLostColor = Color.white;

        [SerializeField]
        private Color txtWinColor = Color.black;

        [SerializeField]
        private Color txtPlayingColor = Color.black;

        [SerializeField]
        private Color txtLostColor = Color.black;

        [Header("References")]
        [SerializeField]
        private TextMeshProUGUI txtName = null;

        [SerializeField]
        private TextMeshProUGUI txtState = null;

        [SerializeField]
        private Image imgBackground = null;

        [SerializeField]
        private GameObject imgPlayer = null;

        /// <summary>Update the PlayerOverViewElement according to the playerproperties</summary>
        public void UpdateElement(KeyValuePair<Player, Dictionary<string, object>> playerProperties)
        {
            Player player = playerProperties.Key;
            PlayerState state = (PlayerState)playerProperties.Value[PlayerPropertyHandler.PlayerStateKey];

            txtName.SetText(player.NickName == string.Empty ? offlineModeName : player.NickName);
            txtState.SetText(state.ToString());

            UpdatePlayerIcon(player);
            UpdateBackgroundColor(state);
        }

        /// <summary>Updates the background color according to the PlayerState</summary>
        private void UpdateBackgroundColor(PlayerState state)
        {
            switch (state)
            {
                case PlayerState.Loading:
                    imgBackground.color = backgroundPlayingColor;
                    txtState.color = txtPlayingColor;
                    break;
                case PlayerState.Won:
                    imgBackground.color = backgroundWinColor;
                    txtState.color = txtWinColor;
                    break;
                case PlayerState.Draw:
                    imgBackground.color = backgroundWinColor;
                    txtState.color = txtWinColor;
                    break;
                case PlayerState.Playing:
                    imgBackground.color = backgroundPlayingColor;
                    txtState.color = txtPlayingColor;
                    break;
                case PlayerState.Lost:
                    imgBackground.color = backgroundLostColor;
                    txtState.color = txtLostColor;
                    break;
            }
        }

        /// <summary>Sets the player icon active if it's the current clients player</summary>
        private void UpdatePlayerIcon(Player player)
        {
            if (player.IsLocal)
            {
                imgPlayer.SetActive(true);
            }
            else
            {
                imgPlayer.SetActive(false);
            }
        }
    }
}
