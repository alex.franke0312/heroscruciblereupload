﻿using GameStudio.HunterGatherer.Networking;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameStudio.HunterGatherer.GameState.UI
{
    /// <summary>Handles the after game UI</summary>
    public class GameStateUI : MonoBehaviour
    {
        [Header("References")]
        [SerializeField]
        private GameObject txtWin = null;

        [SerializeField]
        private GameObject txtLoss = null;

        [SerializeField]
        private GameObject txtDraw = null;

        [SerializeField]
        private Button btnDisconnect = null;

        [SerializeField]
        private Button btnSpectate = null;

        [SerializeField]
        private TMP_Text txtSpectating = null;

        private void Start()
        {
            GameStateManager.OnGameStateChanged += UpdateUI;
            btnDisconnect.onClick.AddListener(Disconnect);
            btnSpectate.onClick.AddListener(Spectate);
            Hide();
        }

        private void OnDestroy()
        {
            GameStateManager.OnGameStateChanged -= UpdateUI;
        }

        /// <summary>Called when the player presses the spectate button in the end menu to start the spectating process</summary>
        private void Spectate()
        {
            //exit if offlinemode or soloplay
            if (NetworkingService.OfflineMode)
            {
                return;
            }

            Hide();
            txtSpectating.gameObject.SetActive(true);
            NetworkingPlayerManager.Instance.Spectate();
        }

        /// <summary>Disconnects the client from its current room</summary>
        private void Disconnect()
        {
            Hide();
            NetworkingService.Instance.Disconnect();
        }

        /// <summary>Checks if it should draw the WON, LOST or DRAW UI</summary>
        private void UpdateUI()
        {
            switch (GameStateManager.Instance.CurrentState)
            {
                case PlayerState.Won:
                    txtWin.SetActive(true);
                    btnSpectate.gameObject.SetActive(false);
                    Show();
                    break;

                case PlayerState.Lost:
                    txtLoss.SetActive(true);
                    Show();
                    break;

                case PlayerState.Draw:
                    txtDraw.SetActive(true);
                    Show();
                    break;

                case PlayerState.Playing:
                    break;
            }
        }

        /// <summary>Shows the after game UI</summary>
        private void Show()
        {
            gameObject.SetActive(true);
        }

        /// <summary>Hides the after game UI</summary>
        private void Hide()
        {
            txtWin.SetActive(false);
            txtLoss.SetActive(false);
            txtDraw.SetActive(false);
            gameObject.SetActive(false);
        }
    }
}