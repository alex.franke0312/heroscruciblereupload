﻿using GameStudio.HunterGatherer.Networking;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameStudio.HunterGatherer.GameState.UI
{
    /// <summary>Handles the UI for the player overview</summary>
    public class PlayerOverviewUI : MonoBehaviour
    {
        [Header("References")]
        [SerializeField]
        private TextMeshProUGUI txtLivingPlayerCount = null;

        [SerializeField]
        private Image imgIconBackground = null;

        [SerializeField]
        private GameObject playerElementPrefab = null;

        private List<PlayerOverViewElementUI> playerElements = new List<PlayerOverViewElementUI>();

        private void Start()
        {
            SetOverviewUI();
            PlayerOverView.OnShow += Show;
            PlayerOverView.OnHide += Hide;
            NetworkingService.Instance.PlayerPropertiesChanged += UpdateOverViewUI;
            Hide();
        }

        private void OnDestroy()
        {
            PlayerOverView.OnShow -= Show;
            PlayerOverView.OnHide -= Hide;
            if (NetworkingService.Instance)
                NetworkingService.Instance.PlayerPropertiesChanged -= UpdateOverViewUI;
            else
                Debug.LogWarning("NetworkingService Instance is null, can't access PlayerPropertiesChanged");
        }

        /// <summary>Shows the player overview UI</summary>
        private void Show()
        {
            gameObject.SetActive(true);
            imgIconBackground.enabled = false;
        }

        /// <summary>Hides the player overview UI</summary>
        private void Hide()
        {
            gameObject.SetActive(false);
            imgIconBackground.enabled = true;
        }

        /// <summary>Spawns elements for each player in the game</summary>
        private void SetOverviewUI()
        {
            foreach (var item in playerElements)
            {
                Destroy(item.gameObject);
            }
            playerElements.Clear();

            foreach (var playerProps in NetworkingService.PlayerProperties)
            {
                GameObject element = Instantiate(playerElementPrefab, transform);
                playerElements.Add(element.GetComponent<PlayerOverViewElementUI>());

                playerElements[playerElements.Count - 1].UpdateElement(playerProps);
            }
        }

        /// <summary>Updates the UI to show correct PlayerStates for all players</summary>
        private void UpdateOverViewUI(Player player)
        {
            if (playerElements.Count != NetworkingService.PlayerProperties.Count)
            {
                SetOverviewUI();
            }

            KeyValuePair<Player, Dictionary<string, object>>[] playerProperties = NetworkingService.PlayerProperties.OrderBy(p => (int)p.Value[PlayerPropertyHandler.PlayerStateKey]).ToArray();
            int playersAliveCount = 0;

            for (int i = 0; i < playerProperties.Length; i++)
            {
                playerElements[i].UpdateElement(playerProperties[i]);

                if ((PlayerState)playerProperties[i].Value[PlayerPropertyHandler.PlayerStateKey] == PlayerState.Playing)
                {
                    playersAliveCount++;
                }
            }

            txtLivingPlayerCount.SetText(playersAliveCount.ToString());
        }
    }
}