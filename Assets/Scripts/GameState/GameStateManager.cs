﻿using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.Networking.Events;
using GameStudio.HunterGatherer.SceneManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameStudio.HunterGatherer.GameState
{
    /// <summary>Handles the state of the player (e.g. if it is player, lost or won)</summary>
    public class GameStateManager : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField, Tooltip("The amount of miliseconds between players that die can be for it to be a draw")]
        private double drawMargin = 80;


        private bool soloPlay;

        public static GameStateManager Instance { get; private set; }

        public PlayerState CurrentState { get; private set; }

        public double DrawMargin { get { return drawMargin; } }

        public static event Action OnGameStateChanged;

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            if (NetworkingService.PlayerList.Length == 1)
            {
                soloPlay = true;
            }

            SceneManager.Instance.OnPlayersLoaded += PlayersLoaded;
            NetworkingPlayerManager.Instance.OnStartSpectating.AddListener(HandleSpectating);
            NetworkingService.Instance.PlayerLeftRoom += CheckWinState;
            NetworkingService.UpdatePlayerProperty(PlayerPropertyHandler.InGameSceneKey, true);
            NetworkingService.AddNetworkingEventListener(NetworkingEventType.PlayerWinGame, HandleWin);
        }

        private void OnDestroy()
        {
            if (SceneManager.Instance)
                SceneManager.Instance.OnPlayersLoaded -= PlayersLoaded;
            else
                Debug.LogWarning("SceneManager Instance is null, can't access OnPlayersLoaded");
            if (NetworkingPlayerManager.Instance)
                NetworkingPlayerManager.Instance.OnStartSpectating.AddListener(HandleSpectating);
            else
                Debug.LogWarning("NetworkingPlayerManager Instance is null, can't access OnStartSpectating");
            if (NetworkingPlayerManager.Instance)
                NetworkingService.Instance.PlayerLeftRoom -= CheckWinState;
            else
                Debug.LogWarning("NetworkingPlayerManager Instance is null, can't access PlayerLeftRoom");
            NetworkingService.RemoveNetworkingEventListener(NetworkingEventType.PlayerWinGame, HandleWin);
        }

        /// <summary>Sets the current state to PLAYING when all players are loaded</summary>
        private void PlayersLoaded()
        {
            CurrentState = PlayerState.Playing;
            NetworkingService.UpdatePlayerProperty(PlayerPropertyHandler.PlayerStateKey, (int)CurrentState);
        }

        /// <summary>Checks if someone is the last player alive</summary>
        private void CheckWinState()
        {
            if (NetworkingService.IsHost)
            {
                //get states of all players and filter them on players playing
                var playerStates = NetworkingService.GetPropertiesOfPlayersInRoom<int>(PlayerPropertyHandler.PlayerStateKey);
                var playersPlaying = playerStates.Where(state => state.Value == (int)PlayerState.Playing);

                if (playersPlaying.Count() == 1)
                {
                    //if only one player is playing, this player has won
                    int lastPlayingActorNr = playersPlaying.First().Key;
                    NetworkingService.RaiseEvent(NetworkingEventType.PlayerWinGame, null, new int[] { lastPlayingActorNr }, true);
                }
            }
        }

        /// <summary>Called when the player starts spectating to share this property with other players</summary>
        private void HandleSpectating()
        {
            if (CurrentState != PlayerState.Spectating)
            {
                CurrentState = PlayerState.Spectating;
                NetworkingService.UpdatePlayerProperty(PlayerPropertyHandler.PlayerStateKey, (int)CurrentState);
            }
        }

        /// <summary>Requests a the status (loss/draw/win) when all the clients units die</summary>
        public void RequestLoss()
        {
            if (soloPlay)
            {
                CurrentState = PlayerState.Lost;
                NetworkingService.UpdatePlayerProperty(PlayerPropertyHandler.PlayerStateKey, (int)CurrentState);
                OnGameStateChanged?.Invoke();
                return;
            }

            if (CurrentState == PlayerState.Playing)
            {
                NetworkingService.RaiseRequest(NetworkingRequestType.Lose, HandleLossRequest, -1);
            }
        }

        /// <summary>Handles this client winning</summary>
        private void HandleWin(object obj)
        {
            if (NetworkingService.IsConnected || NetworkingService.OfflineMode)
            {
                CurrentState = PlayerState.Won;
                NetworkingService.UpdatePlayerProperty(PlayerPropertyHandler.PlayerStateKey, (int)CurrentState);
                OnGameStateChanged?.Invoke();
            }
        }

        /// <summary>Handles the response from the loss request</summary>
        private void HandleLossRequest(object obj)
        {
            if (CurrentState != PlayerState.Playing || !NetworkingService.IsConnected) { return; }

            Dictionary<int, int> content = (Dictionary<int, int>)obj;

            foreach (var item in content)
            {
                if (item.Key == NetworkingService.LocalPlayer.ActorNumber)
                {
                    switch ((NetworkingLoseDesicionType)item.Value)
                    {
                        case NetworkingLoseDesicionType.Lost:
                            CurrentState = PlayerState.Lost;
                            break;

                        case NetworkingLoseDesicionType.Win:
                            CurrentState = PlayerState.Won;
                            break;

                        case NetworkingLoseDesicionType.Draw:
                            CurrentState = PlayerState.Draw;
                            break;
                    }

                    NetworkingService.UpdatePlayerProperty(PlayerPropertyHandler.PlayerStateKey, (int)CurrentState);
                    OnGameStateChanged?.Invoke();
                    break;
                }
            }
        }
    }
}