﻿using System;
using UnityEngine;

namespace GameStudio.HunterGatherer.GameState
{
    /// <summary>This class handles the PlayerOverviewUI showing and hiding</summary>
    public class PlayerOverView : MonoBehaviour
    {
        public static PlayerOverView Instance { get; private set; }

        public static event Action OnShow;

        public static event Action OnHide;

        private void Awake()
        {
            Instance = this;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                OnShow?.Invoke();
            }
            if (Input.GetKeyUp(KeyCode.Tab))
            {
                OnHide?.Invoke();
            }
        }
    }
}