﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace GameStudio.HunterGatherer.DayAndNightCycle
{
    ///<summary> This editor class is used to create an custom inspector for the evensystem of the daynightcycle</summary>
    [CustomEditor(typeof(DayNightCycle))]
    public class DayNightEditor : Editor
    {
        private SerializedProperty eventHourPairs;
        private SerializedProperty gameStartTime;
        private SerializedProperty durationOfHour;
        private ReorderableList list;
        private DayNightCycle dayNightScript;

        private void OnEnable()
        {
            dayNightScript = (DayNightCycle)target;
            eventHourPairs = serializedObject.FindProperty("eventHourPairs");
            gameStartTime = serializedObject.FindProperty("gameStartTime");
            durationOfHour = serializedObject.FindProperty("durationOfHour");

            list = new ReorderableList(serializedObject, eventHourPairs)
            {
                draggable = true,
                displayAdd = true,
                displayRemove = true,
                drawHeaderCallback = rect =>
                {
                    EditorGUI.LabelField(rect, "Events");
                },
                drawElementCallback = (rect, index, sel, act) =>
                {
                    var element = eventHourPairs.GetArrayElementAtIndex(index);

                    var unityEvent = element.FindPropertyRelative("onHourMatched");
                    var hour = element.FindPropertyRelative("hour");
                    var icon = element.FindPropertyRelative("icon");

                    EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), hour);

                    rect.y += EditorGUIUtility.singleLineHeight;

                    EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), icon);

                    rect.y += EditorGUIUtility.singleLineHeight;

                    EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width, EditorGUI.GetPropertyHeight(unityEvent)), unityEvent);
                },
                elementHeightCallback = index =>
                {
                    var element = eventHourPairs.GetArrayElementAtIndex(index);
                    var unityEvent = element.FindPropertyRelative("onHourMatched");

                    var height = EditorGUI.GetPropertyHeight(unityEvent) + EditorGUIUtility.singleLineHeight * 2;
                    return height;
                }
            };
        }

        public override void OnInspectorGUI()
        {
            DrawScriptField();
            DrawSettingsFields();
            DrawListFields();
        }

        ///<summary> This method will draw the field of where the script is going to be </summary>
        private void DrawScriptField()
        {
            // Disable editing
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.ObjectField("Script", MonoScript.FromMonoBehaviour(dayNightScript), typeof(DayNightCycle), false);
            EditorGUI.EndDisabledGroup();
            EditorGUILayout.Space();
        }

        ///<summary> This method will draw the fields of the variables of the DayNight script you can set in inspector</summary>
        private void DrawSettingsFields()
        {
            EditorGUILayout.PropertyField(gameStartTime);
            EditorGUILayout.PropertyField(durationOfHour);
            EditorGUILayout.Space();
            serializedObject.ApplyModifiedProperties();
        }

        ///<summary> This method will draw and update the reordable eventlist </summary>
        private void DrawListFields()
        {
            serializedObject.Update();
            list.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
        }
    }
}
