using System.Collections;
using System.Collections.Generic;
using GameStudio.HunterGatherer.Divisions;
using GameStudio.HunterGatherer.Divisions.Upgrades;
using UnityEngine;
using UnityEngine.UI;

namespace GameStudio.HunterGatherer.UI
{
    public class UpgradeButton : ActionButton
    {
        public Division Division { get; set; }
        public UpgradeBase upgrade { get; set; }

        UpgradeEventArgs args;

        public override void Show(Object obj)
        {
            Division = obj as Division;

            args = new UpgradeEventArgs(Division.NetworkedMovingObject, upgrade);

            imgIcon.sprite = upgrade.icon;

            gameObject.SetActive(true);
        }

        public override void PerformAction()
        {            
            EventManager.Instance.Invoke("OnUpgraded", args);
            onPerformAction?.Invoke();
        }
    }
}