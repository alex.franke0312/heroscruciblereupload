using System.Collections;
using System.Collections.Generic;
using GameStudio.HunterGatherer.Divisions;
using GameStudio.HunterGatherer.Divisions.Upgrades;
using GameStudio.HunterGatherer.Structures;
using UnityEngine;

namespace GameStudio.HunterGatherer.UI
{
    public class UpgradeCarousel : ButtonCarousel
    {
        [SerializeField] UpgradeBase[] upgrades;

        protected override void GeneratePool()
        {
            actionPool = new List<ActionButton>();
            for (int i = 0; i < upgrades.Length; i++)
            {
                try
                {
                    actionPool.Add(Instantiate(actionButton, transform).GetComponent<ActionButton>());
                }
                catch (MissingComponentException)
                {
                    Debug.LogError($"<b>[{gameObject.name}]</b> Your actionButton prefab doesn't contain the ActionButton component!");
                }
            }
        }

        public override void Show(Object sender, Vector3 position, List<Object> objects)
        {
            this.sender = sender;

            transform.position = position;

            if (sender.GetType().IsSubclassOf(typeof(ActionButton)))
            {
                (sender as ActionButton).onDeactivate += DeActivateAll;
            }

            for (int i = 0; i < actionPool.Count; i++)
            {
                if (i < upgrades.Length)
                {
                    (actionPool[i] as UpgradeButton).upgrade = upgrades[i];
                    actionPool[i].Show(objects[0]);
                    actionPool[i].onPerformAction += DeActivateAll;
                }
                else
                    actionPool[i].gameObject.SetActive(false);
            }

            SetCircle(upgrades.Length);

            gameObject.SetActive(true);
        }

        public override void DeActivateAll()
        {
            if (sender.GetType().IsSubclassOf(typeof(ActionButton)))
            {
                (sender as ActionButton).onDeactivate -= DeActivateAll;
            }
            base.DeActivateAll();
        }
    }
}