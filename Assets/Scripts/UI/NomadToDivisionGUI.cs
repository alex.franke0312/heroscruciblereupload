using System;
using UnityEngine;
using GameStudio.HunterGatherer.Divisions.UI;
using GameStudio.HunterGatherer.Divisions;

namespace GameStudio.HunterGatherer.UI
{
    class NomadToDivisionGUI : ShowGUIOnEvent
    {
        DivisionListEventArgs divisionArgs;

        [SerializeField] DivisionOverviewItem overviewItem;
        [SerializeField] string onSwordmenClicked, onArcherClicked, onPikemenClicked;

        SwapDivisionEventArgs upgradeEventArgs = new SwapDivisionEventArgs();

        protected override void Awake()
        {
            base.Awake();

            EventManager.Instance.AddListener(onSwordmenClicked, Upgrade);
            EventManager.Instance.AddListener(onArcherClicked, Upgrade);
            EventManager.Instance.AddListener(onPikemenClicked, Upgrade);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.Instance.RemoveListener(onSwordmenClicked, Upgrade);
            EventManager.Instance.RemoveListener(onArcherClicked, Upgrade);
            EventManager.Instance.RemoveListener(onPikemenClicked, Upgrade);
        }

        void Upgrade(EventArgs args)
        {
            upgradeEventArgs = args as SwapDivisionEventArgs;

            if (upgradeEventArgs.sender != gameObject.GetInstanceID())
                return;

            overviewItem.division.RaiseSetType(upgradeEventArgs.divisionType);
            transform.GetChild(0).gameObject.SetActive(false);
        }

        protected override void ShowGUI(EventArgs divisionArgs)
        {
            this.divisionArgs = divisionArgs as DivisionListEventArgs;

            if (!this.divisionArgs.division.Equals(overviewItem.division.NetworkedMovingObject))
                return;


            base.ShowGUI(divisionArgs);

            upgradeEventArgs.division = this.divisionArgs.division.GetComponent<Division>();
        }

        public void InvokeEvent(string eventName)
        {
            upgradeEventArgs.sender = gameObject.GetInstanceID();

            if (onSwordmenClicked == eventName)
            {
                upgradeEventArgs.divisionType = DivisionType.Swordsmen;
                EventManager.Instance.Invoke(onSwordmenClicked, upgradeEventArgs);
            }
            else if (onArcherClicked == eventName)
            {
                upgradeEventArgs.divisionType = DivisionType.Archers;
                EventManager.Instance.Invoke(onSwordmenClicked, upgradeEventArgs);

            }
            else if (onPikemenClicked == eventName)
            {
                upgradeEventArgs.divisionType = DivisionType.Pikemen;
                EventManager.Instance.Invoke(onSwordmenClicked, upgradeEventArgs);
            }
        }
    }


    class SwapDivisionEventArgs : EventArgs
    {
        public Division division;
        public DivisionType divisionType;
        public int sender;

        public SwapDivisionEventArgs()
        {
        }

        public SwapDivisionEventArgs(Division division, DivisionType divisionType, int sender)
        {
            this.division = division;
            this.divisionType = divisionType;
            this.sender = sender;
        }
    }
}