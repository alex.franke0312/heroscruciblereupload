﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameStudio.HunterGatherer.DayAndNightCycle;
using UnityEngine.UI;
using System;

namespace GameStudio.HunterGatherer.DayAndNightCycle
{
    /// <summary> Handles the clock mechanics like setting Day and Night times </summary>
    public class Clock : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField, Range(13, 24)]
        private float nightStartTime = 0;

        [SerializeField, Range(1, 12)]
        private float nightEndTime = 0;

        [Header("References")]
        [SerializeField]
        private Image nightDisc = null;

        [SerializeField]
        private Image dayDisc = null;

        [SerializeField]
        private Transform moon = null;

        [SerializeField]
        private Transform sun = null;

        [SerializeField]
        private Transform disc = null;

        [SerializeField]
        private Image dangerZone = null;

        [SerializeField]
        private Color safeColor;

        [SerializeField]
        private Color warningColor;

        [SerializeField]
        private Color dangerColor;

        [Header("Icon Settings")]
        [SerializeField]
        private Transform iconsParent = null;
        [SerializeField]
        private GameObject iconPrefab = null;
        [SerializeField]
        private float radius = 180;

        [SerializeField]
        private string onFloodStarted = "OnFloodStarted";

        private bool gameStartFlood = true;

        private void Awake()
        {
            PositionNightTime();
            AddIcons();
            RotateMoonAndSun();
        }

        private void Start()
        {
            EventManager.Instance.AddListener(onFloodStarted, OnFloodStarted);
            SetSafe();
        }

        private void OnValidate()
        {
            PositionNightTime();
            RotateMoonAndSun();
        }

        private void Update()
        {
            RotateClock();
        }

        /// <summary> Sets the position and size of night on the clock </summary>
        private void PositionNightTime()
        {
            float nightTimeImageRotation = nightStartTime / 24 * 360;
            nightDisc.transform.localRotation = Quaternion.Euler(0, 0, nightTimeImageRotation);
            dayDisc.transform.localRotation = Quaternion.Euler(0, 0, nightTimeImageRotation);
            nightDisc.fillAmount = (nightEndTime + 24 - nightStartTime) / 24;
        }

        ///<summary> Position the Moon and Sun on the clock based on the duration of the night </summary>
        private void RotateClock()
        {
            float clockRotation = DayNightCycle.Instance.CurrentTimeDayPercentage * 360;
            disc.transform.localRotation = Quaternion.Euler(0, 0, -clockRotation);
        }

        /// <summary>Add the icons of the events to the clock</summary>
        private void AddIcons()
        {
            Image image;
            GameObject iconObj;

            if (iconsParent)
                DestroyIcons();

            for (int i = 0; i < DayNightCycle.Instance.EventHourPairs.Count; i++)
            {
                if (DayNightCycle.Instance.EventHourPairs[i].icon)
                {
                    iconObj = Instantiate(iconPrefab, (iconsParent ? iconsParent : disc.transform));

                    image = iconObj.GetComponent<Image>();

                    if (!image)
                    {
                        image = iconObj.AddComponent<Image>();
                    }

                    image.sprite = DayNightCycle.Instance.EventHourPairs[i].icon;
                    iconObj.transform.localPosition = new Vector3(
                        (iconsParent ? iconsParent : disc.transform).localPosition.x + radius * Mathf.Cos((DayNightCycle.Instance.EventHourPairs[i].hour / 24f * 360 - 90) * Mathf.Deg2Rad),
                        (iconsParent ? iconsParent : disc.transform).localPosition.y + radius * Mathf.Sin((DayNightCycle.Instance.EventHourPairs[i].hour / 24f * 360 - 90) * Mathf.Deg2Rad),
                        0
                    );
                    iconObj.transform.rotation = Quaternion.Euler(0, 0, DayNightCycle.Instance.EventHourPairs[i].hour / 24f * 360);
                }
            }
        }

        /// <summary>Destroy Old Icons</summary>
        private void DestroyIcons()
        {
            for (int iIcon = iconsParent.childCount; iIcon-- > 0;)
            {
                if (Application.isPlaying)
                    Destroy(iconsParent.GetChild(iIcon).gameObject);
                else
                    DestroyImmediate(iconsParent.GetChild(iIcon).gameObject);
            }
        }

        /// <summary> Rotate the sun and moon with the night and day </summary>
        private void RotateMoonAndSun()
        {
            float angle = (nightDisc.fillAmount / 2) * 360f;
            moon.localRotation = Quaternion.Euler(0, 0, angle);
            sun.localRotation = Quaternion.Euler(0, 0, angle + 180);
        }

        /// <summary> Sets the danger zone to danger and adds a callback to set the dangerzone to safe when the flood has ended.  </summary>
        private void OnFloodStarted(EventArgs eventArgs)
        {
            //Don't do this for the first flood which is triggered on game start.
            if (!gameStartFlood)
            {
                StartFloodEventArgs args = (StartFloodEventArgs)eventArgs;
                args.FloodInfo.setOnComplete(SetSafe);
                SetDanger();
            }
            else
            {
                gameStartFlood = false;
            }
        }

        private void SetDanger()
        {
            if (dangerZone)
            {
                dangerZone.color = dangerColor;
            }
        }

        private void SetSafe()
        {
            dangerZone.color = safeColor;
        }

        public void SetWarning()
        {
            dangerZone.color = warningColor;
        }
    }
}

