﻿using UnityEngine;
using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.Utilities;
using System.Collections;
using GameStudio.HunterGatherer.GameState;

namespace GameStudio.HunterGatherer.DayAndNightCycle
{
    ///<summary> this class will activate or deactivate the base button</summary>
    public class BaseButtonActivator : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField]
        private float uIPosOffset = 5f;

        [Header("References")]
        [SerializeField]
        private GameObject btnBase = null;

        [SerializeField]
        private RectTransform canvasRect = null;

        private RectTransform btnBaseRect;
        private Transform hero;
        
        private void Awake()
        {
            btnBaseRect = (RectTransform)btnBase.transform;
            GameStateManager.OnGameStateChanged += GameStateManager_OnGameStateChanged;
        }

        private void OnDestroy() => GameStateManager.OnGameStateChanged -= GameStateManager_OnGameStateChanged;

        /// <summary> Event listener that disables btnBase when the game is over </summary>
        private void GameStateManager_OnGameStateChanged()
        {
            bool IsGameOver = (GameStateManager.Instance.CurrentState & 
                PlayerState.Lost | 
                PlayerState.Draw | 
                PlayerState.Won) != 0;

            if (IsGameOver)
            {
                this.enabled = false;
                btnBase.SetActive(false);
            }
        }

        ///<summary> Checks if the given hour is in the timeframe to activate the button</summary>
        public void SetActiveBaseButton(bool isActive)
        {
            if (enabled)
            {
                btnBase.SetActive(isActive);
                StartCoroutine(UpdateButtonPosition());
            }
        }

        ///<summary> Will align the button position to that of the hero </summary>
        private IEnumerator UpdateButtonPosition()
        {
            hero = NetworkingPlayerManager.Instance.HeroDivision.transform;

            while (btnBase.activeSelf)
            {
                CanvasHelper.AlignnUIInWorldSpace(btnBaseRect, canvasRect, hero.transform, uIPosOffset);

                yield return null;
            }
        }
    }
}
