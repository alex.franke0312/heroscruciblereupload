﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace GameStudio.HunterGatherer.DayAndNightCycle
{
    [Serializable]
    ///<summary> This class is used to make an object of an event and the time it will be invoked.</summary>
    public class EventHourPair
    {       
        [Tooltip("The event that will be fired on the given hour.")]
        public UnityEvent onHourMatched = new UnityEvent();
        
        [Tooltip("The icon which will be visible on the timer HUD")]
        public Sprite icon;

        [Range(0, 24), Tooltip("Represents the hours of the day.")]
        public int hour = 0;
    }
}