﻿using UnityEngine;

namespace GameStudio.HunterGatherer.DayAndNightCycle
{
    ///<summary> This class handles the day and night cycle by executing the preset of the lightingcondition</summary>
    [RequireComponent(typeof(Light))]
    public class LightingManager : MonoBehaviour
    {
        [Header("References")]
        private Light directionalLight = null;

        private void Start() => directionalLight = GetComponent<Light>();

        /// <summary> Increases the directional lighting intensity. </summary>
        public void TurnLightOn(float duration) => LeanTween.value(directionalLight.intensity, 1, duration).setOnUpdate(val => directionalLight.intensity = val);

        /// <summary> Decreases the directional lighting intensity. </summary>
        public void TurnLightOff(float duration) => LeanTween.value(directionalLight.intensity, 0.1f, duration).setOnUpdate(val => directionalLight.intensity = val);
    }
}
