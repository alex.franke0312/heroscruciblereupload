﻿using System.Collections.Generic;
using UnityEngine;
using GameStudio.HunterGatherer.CustomEvents;
using GameStudio.HunterGatherer.UI;
using UnityEngine.UI;

namespace GameStudio.HunterGatherer.DayAndNightCycle
{
    ///<summary> Handles the day and night cycle time and you can also set an event on a specific hour of the day</summary>
    public class DayNightCycle : MonoBehaviour
    {
        [Header("DayNightSettings")]
        [SerializeField]
        private static DayNightCycle instance;

        [SerializeField]
        private List<EventHourPair> eventHourPairs = null;

        [SerializeField, Range(0, 24)]
        private float gameStartTime = 0;

        [SerializeField, Tooltip("Duration of an hour in seconds"), Min(1f)]
        private float durationOfHour = 5f;

        private int currentHourOfDay;

        /// <summary> The amount of hours in a day 24. </summary>
        public const int DayDuration = 24;
        public static DayNightCycle Instance { get { return instance; } }

        /// <summary> Value indicating how late it is in game. (between 0 and 24 hours)</summary>
        public float CurrentTimeOfDay { get; private set; }
        /// <summary> Percentage of day. Is between 0 and 1. </summary>
        public float CurrentTimeDayPercentage => CurrentTimeOfDay / DayDuration;
        /// <summary> The amount of seconds each hour contains. </summary>
        public float DurationOfHour { get => durationOfHour; set => durationOfHour = value; }
        /// <summary> The hour you want to start the game in</summary>
        public float StartTime => gameStartTime;
        public List <EventHourPair> EventHourPairs => eventHourPairs;
        public UnityEventInt OnHourIncrement { get; } = new UnityEventInt();
        /// <summary> Indicates if it is daytime or not.</summary>
        public bool IsDay => CurrentHourOfDay < 20 && CurrentHourOfDay >= 4; 
        /// <summary> Value indicating wich hour it is in the game expressed as ints</summary>
        public int CurrentHourOfDay
        {
            get
            {
                return currentHourOfDay;
            }
            set
            {
                if (currentHourOfDay == value)
                {
                    return;
                }
                currentHourOfDay = value;
                OnHourIncrement.Invoke(value);
            }
        }

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            else
            {
                instance = this;
            }
            CurrentTimeOfDay = StartTime;
        }

        private void OnEnable()
        {
            OnHourIncrement.AddListener(CheckEventTime);
        }

        private void OnDisable()
        {
            OnHourIncrement.RemoveListener(CheckEventTime);
        }

        private void Update()
        {
            UpdateTime();
        }

        ///<summary> Updates the time each second and it will modulate it to a 24 gameHour cycle</summary>
        public void UpdateTime()
        {
            CurrentTimeOfDay += Time.deltaTime / DurationOfHour;
            CurrentTimeOfDay %= DayDuration;

            CurrentHourOfDay = Mathf.FloorToInt(CurrentTimeOfDay);
        }

        ///<summary> Checks which events in the list will be invoked on what time during the day</summary>
        public void CheckEventTime(int hour)
        {
            foreach (EventHourPair ehp in eventHourPairs)
            {
                if (ehp.hour == hour)
                {
                    ehp.onHourMatched.Invoke();
                }
            }
        }

        /// <summary> Finds the method in the eventHourPairs by methodname </summary>
        public List<int> FindEventByMethod(System.Action method)
        {
            List<int> appearances = new List<int>();
            foreach (var pair in EventHourPairs)
            {
                for (int i = 0; i < pair.onHourMatched.GetPersistentEventCount(); i++)
                {
                    if (pair.onHourMatched.GetPersistentMethodName(i) == nameof(method))
                    {
                        appearances.Add(pair.hour);
                    }
                }
            }
            return appearances;
        }

        /// <summary> Returns a list of all the eventhourpairs where the methodname matched the given parameter. </summary>
        public List<EventHourPair> FindEventHourPairByMethod(string methodName)
        {
            List<EventHourPair> appearances = new List<EventHourPair>();
            foreach (var pair in EventHourPairs)
            {
                for (int i = 0; i < pair.onHourMatched.GetPersistentEventCount(); i++)
                {
                    if (pair.onHourMatched.GetPersistentMethodName(i) == methodName)
                    {
                        appearances.Add(pair);
                        break;
                    }
                }
            }
            return appearances;
        }
    }
}
