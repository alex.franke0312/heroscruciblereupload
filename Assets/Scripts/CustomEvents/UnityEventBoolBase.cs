﻿using GameStudio.HunterGatherer.Structures;
using UnityEngine.Events;

namespace GameStudio.HunterGatherer.CustomEvents
{
    /// <summary>Custom UnityEvent that passes a bool and a base</summary>
    [System.Serializable]
    public class UnityEventBoolBase : UnityEvent<bool, Base> { }
}
