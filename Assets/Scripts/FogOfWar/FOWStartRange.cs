﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameStudio.HunterGatherer.DayAndNightCycle;

namespace GameStudio.HunterGatherer.FogOfWar
{
    /// <summary> Sets the range of the fog of war equal to a given value when the division is spawned </summary>
    public class FOWStartRange : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField]
        private float startRange = 10f;

        private DayNightCycle dayNightCycle = DayNightCycle.Instance;
        private int startNightHour;
        private int startDayHour;

        private void Awake()
        {
            if(DayNightCycle.Instance == null)
            {
                this.enabled = false;
                return;
            }

            // startNightHour = dayNightCycle.FindEventHourPairByMethod("TurnLightOff")[0].hour;
            // startDayHour = dayNightCycle.FindEventHourPairByMethod("TurnLightOn")[0].hour;
        }

        private void OnEnable()
        {
            if (dayNightCycle.CurrentHourOfDay >= startNightHour || dayNightCycle.CurrentHourOfDay <= startDayHour)
            {
                transform.localScale = new Vector3(startRange, transform.localScale.y, startRange);
            }        
        }
    }
}