﻿using System;
using System.Collections.Generic;
using GameStudio.HunterGatherer.CustomEvents;
using GameStudio.HunterGatherer.Divisions;
using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.Networking.Events;
using GameStudio.HunterGatherer.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace GameStudio.HunterGatherer.GodFavor.UI
{
    [System.Serializable]
    public class GodPowerEvent : UnityEvent<GodPowerManager.Gods> { }

    [System.Serializable]
    public class TooltipData
    {
        public string title;
        public string description;

        public TooltipData(string title, string description)
        {
            this.title = title;
            this.description = description;
        }
    }

    public class GodFavorUI : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField]
        private static GodPowerManager.Gods _currentGod = GodPowerManager.Gods.Ares;
        [SerializeField]
        private int startAmount = 0;
        [SerializeField, Range(1, 6)]
        private int maxAmount;
        [SerializeField, Tooltip("Randomize selected god at start of the game")]
        private bool randomizeGod = true;


        private float _amount;
        public float Amount
        {
            get { return _amount; }
            set
            {
                if (Math.Abs(_amount - value) < float.Epsilon)
                    return;

                _amount = value;
                OnGodFavorAmountChanged.Invoke(value);
            }
        }
        public UnityEventFloat OnGodFavorAmountChanged = new UnityEventFloat();

        [Header("References")]
        [SerializeField]
        //private TextMeshProUGUI txtGodFavorCounter = null;
        private GameObject godFavorDot = null;
        [SerializeField]
        private Transform godFavorLayout = null;
        List<GodFavorDot> godFavorDots = new List<GodFavorDot>();

        [SerializeField]
        private Image cooldownFiller;


        public GodPowerEvent OnGodPowerTrigger = new GodPowerEvent();
        public GodPowerEvent OnGodPowerFinished = new GodPowerEvent();

        public static GodPowerManager.Gods CurrentGod
        {
            get
            {
                return _currentGod;
            }
            set
            {
                //UpdateGodIcon(value);
                _currentGod = value;
            }
        }

        /// <summary>
        /// Sets god favor UI icon
        /// </summary>
        /// <param name="god"></param>
        private void UpdateGodIcon(GodPowerManager.Gods god)
        {
            switch (god)
            {
                case GodPowerManager.Gods.Zeus:
                    GodIcon.sprite = zeusIcon;
                    GodToolTip.Title = zeusTooltip.title;
                    GodToolTip.Description = zeusTooltip.description;
                    break;
                case GodPowerManager.Gods.Ares:
                    GodIcon.sprite = aresIcon;
                    GodToolTip.Title = aresTooltip.title;
                    GodToolTip.Description = aresTooltip.description;
                    break;
                case GodPowerManager.Gods.Athena:
                    GodIcon.sprite = athenaIcon;
                    GodToolTip.Title = athenaTooltip.title;
                    GodToolTip.Description = athenaTooltip.description;
                    break;
            }
        }

        public void SetGod(GodPowerManager.Gods god)
        {
            CurrentGod = god;
            UpdateGodIcon(god);
        }

        [Header("Icons")]
        [SerializeField]
        private Sprite zeusIcon;
        [SerializeField]
        private TooltipData zeusTooltip;
        [SerializeField]
        private Sprite aresIcon;
        [SerializeField]
        private TooltipData aresTooltip;
        [SerializeField]
        private Sprite athenaIcon;
        [SerializeField]
        private TooltipData athenaTooltip;
        [SerializeField]
        private Sprite nullIcon;
        [SerializeField]
        private TooltipData nullTooltip;


        private Image _godIcon;
        public Image GodIcon
        {
            get
            {
                if (_godIcon == null)
                    _godIcon = transform.GetChild(0).GetComponent<Image>();
                return _godIcon;
            }
        }


        private ShowTooltip _godTooltip;
        public ShowTooltip GodToolTip
        {
            get
            {
                if (_godTooltip == null)
                    _godTooltip = GetComponent<ShowTooltip>();
                return _godTooltip;
            }
        }

        public static GodFavorUI Instance { get; private set; }

        public GodFavorDot? GetLastEmptySlot()
        {
            for (int i = 0; i < godFavorDots.Count; i++)
            {
                if (!godFavorDots[i].Status)
                {
                    if (i == 0)
                        return godFavorDots[i];
                    else
                        return godFavorDots[i - 1];
                }
            }
            return godFavorDots[godFavorDots.Count - 1];
        }

        private void OnValidate()
        {

            startAmount = startAmount < 0 ? 0 : startAmount > maxAmount ? maxAmount : startAmount;

            if (godFavorDot == null || godFavorLayout == null)
                return;

            // Check for unused buttons
            for (int i = godFavorLayout.childCount; i-- > maxAmount;)
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.delayCall += () =>
                {
                    try
                    {
                        godFavorDots.Remove(godFavorDots[i]);
                        DestroyImmediate(godFavorLayout.GetChild(i).gameObject);
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        ResetDotsList();
                    }
                };
#endif
            }

            // Add new buttons if there aren't enough
            for (int i = godFavorLayout.childCount; i < maxAmount; i++)
            {
                godFavorDots.Add(Instantiate(godFavorDot, Vector3.zero, Quaternion.identity, godFavorLayout).GetComponent<GodFavorDot>());
            }

            // Set the status
            for (int i = maxAmount; i-- > 0;)
            {
                try
                {
                    godFavorDots[i].SetStatus(i < startAmount);
                }
                catch (MissingReferenceException)
                {
                    godFavorDots.RemoveAt(i);
                }
                catch (ArgumentOutOfRangeException)
                {
                    // Your list is fucked reset it
                    ResetDotsList();
                    break;
                }
            }
        }

        void ResetDotsList()
        {
            godFavorDots = new List<GodFavorDot>();
            for (int iDot = 0; iDot < maxAmount; iDot++)
            {
                godFavorDots.Add(godFavorLayout.GetChild(iDot).GetComponent<GodFavorDot>());
                godFavorDots[iDot].SetStatus(iDot < startAmount);
            }
        }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Debug.LogWarning("Singleton GodFavorUI has already been created, deleting object " + gameObject.name);
                Destroy(gameObject);
            }

            CurrentGod = GodPowerManager.Gods.Null;
            UpdateGodIcon(CurrentGod);

            ResetDotsList();
            OnGodFavorAmountChanged.AddListener(UpdateUI);
            Amount = startAmount;
        }

        public void RandomizeGod()
        {
            var values = Enum.GetValues(typeof(GodPowerManager.Gods));
            CurrentGod = (GodPowerManager.Gods)Random.Range(0, Enum.GetValues(typeof(GodPowerManager.Gods)).Length);
        }

        /// <summary>
        /// Set's the cooldown filler within the range of 0 to 1
        /// </summary>
        /// <param name="progress"></param>
        public void SetCooldownFiller(float progress)
        {
            cooldownFiller.fillAmount = progress;
        }

        private void OnEnable()
        {
            if (NetworkingService.IsConnected || NetworkingService.OfflineMode)
            {
                NetworkingService.AddNetworkingEventListener(NetworkingEventType.GodFavorResourceAdded, RecieveGodFavorFromPickup);
            }
        }

        private void OnDisable()
        {
            if (NetworkingService.IsConnected || NetworkingService.OfflineMode)
            {
                NetworkingService.RemoveNetworkingEventListener(NetworkingEventType.GodFavorResourceAdded, RecieveGodFavorFromPickup);
            }
        }

        /// <summary>Add given amount of manpower</summary>
        public void AddGodFavor(float amount)
        {
            Amount += amount;
            Amount = Amount < 0 ? 0 : Amount > maxAmount ? maxAmount : Amount;
        }

        /// <summary>Remove given amount of manpower</summary>
        public void RemoveGodFavor(float amount)
        {
            Amount -= amount;
            Amount = Amount < 0 ? 0 : Amount > maxAmount ? maxAmount : Amount;
        }

        /// <summary></summary>
        private void UpdateUI(float _)
        {
            for (int iDot = 0; iDot < godFavorDots.Count; iDot++)
            {
                godFavorDots[iDot].SetStatus(iDot < Amount);
            }
        }

        /// <summary>This will check if you killed a hero or another unit and grants you the proper amount manpower as a reward for it</summary>
        private void RecieveGodFavorFromPickup(object content)
        {
            // Guard clause to exit if not connected and not in offline mode
            if (!NetworkingService.IsConnected && !NetworkingService.OfflineMode)
            {
                return;
            }

            GodFavorResourceAddedInfo info = (GodFavorResourceAddedInfo)content;
            GameObject collectorObject = NetworkingResources.Find(info.CollectorViewID);

            // Guard clause to exit if attackerObject is null
            if (collectorObject == null)
            {
                return;
            }

            Unit collector = collectorObject.GetComponent<Unit>();

            // Guard clause to exit if attackerObject is not mine
            if (!collector.NetworkedMovingObject.IsMine)
            {
                return;
            }

            // TODO: complete networking support implementation and test
            int ownerActorNr = collector.NetworkedMovingObject.OwnerActorNr;
            if (ownerActorNr == NetworkingPlayerManager.Instance.HeroDivision.OwnerActorNr)
            {
                AddGodFavor(info.Amount);
            }
        }


        private void Update()
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Y))
                AddGodFavor(5f);

            if (Input.GetKeyDown(KeyCode.N))
                RemoveGodFavor(5f);
#endif

            if (Input.GetKeyDown(KeyCode.Tab) && !GodPowerManager.activeManager.IsShowingPowerRange)
            {

                switch (CurrentGod)
                {
                    case GodPowerManager.Gods.Ares:
                        CurrentGod = GodPowerManager.Gods.Ares;
                        break;
                    case GodPowerManager.Gods.Athena:
                        CurrentGod = GodPowerManager.Gods.Athena;
                        break;
                    case GodPowerManager.Gods.Zeus:
                        CurrentGod = GodPowerManager.Gods.Zeus;
                        break;
                    case GodPowerManager.Gods.Null:
                        CurrentGod = GodPowerManager.Gods.Null;
                        break;
                }
            }
        }

        public void TriggerGodPower()
        {
            GodPowerManager.activeManager?.GodPower(CurrentGod);
        }
    }
}
