using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameStudio.HunterGatherer.Networking;
using UnityEngine;
using FMODUnity;
using GameStudio.HunterGatherer.Utilities;
using GameStudio.HunterGatherer.UI;
using GameStudio.HunterGatherer.Divisions;

namespace GameStudio.HunterGatherer.Structures
{
    public class UpgradeStructureInteract : StructureInteract
    {
        [Header("Upgrade Structure specific")]
        [SerializeField]
        private string setUI = "OnUpgradeRange";

        private int localUses = 0;
        [SerializeField] private int maxLocalUses = 2;
        protected List<GameObject> previousColliders = new List<GameObject>();
        [SerializeField]
        private GameObject validUpgradeIcon;
        [SerializeField]
        private GameObject invalidUpgradeIcon;

        [Header("Audio Settings")]
        [SerializeField]
        protected StudioParameterTrigger fmodTrigger;

        private void OnEnable()
        {
            EventManager.Instance.AddListener("OnUpgraded", CheckUse);
        }

        private void OnDisable()
        {
            EventManager.Instance.RemoveListener("OnUpgraded", CheckUse);
            UpgradeUIManager.Instance.DeActivateAll(this);
        }

        protected override void OnTriggerEnter(Collider other)
        {
            NetworkedMovingObject collider = other.GetComponent<NetworkedMovingObject>();
            collider ??= other.GetComponentInParent<NetworkedMovingObject>();

            if ((!collider || !collider.IsMine || previousColliders.Contains(collider.gameObject)))
                return;

            if (IsValidInteraction(other))
            {
                //If you're eligible to upgrade, run the following code
                if (CheckUpgradeValidity())
                {
                    previousColliders.Add(collider.gameObject);

                    UpgradeUIManager.Instance.Show(
                        this,
                        transform.position + new Vector3(
                            transform.up.x * transform.localScale.x,
                            transform.up.y * transform.localScale.y,
                            transform.up.z * transform.localScale.z),
                        previousColliders.Select(x => x.GetComponent<Division>()).ToObjectList()
                    );

                    this.previousCollider = collider.gameObject;
                }
            }
        }

        protected override void OnTriggerExit(Collider other)
        {
            NetworkedMovingObject collider = other.GetComponent<NetworkedMovingObject>();
            collider ??= other.GetComponentInParent<NetworkedMovingObject>();

            if (!collider || !collider.IsMine)
                return;
            if (localUses < maxLocalUses && IsValidInteraction(other) && this.previousColliders.Contains(collider.gameObject))
            {
                this.previousCollider = null;
                this.previousColliders.Remove(collider.gameObject);

                UpgradeUIManager.Instance.Show(
                    this,
                    transform.position + new Vector3(
                        transform.up.x * transform.localScale.x * 5,
                        transform.up.y * transform.localScale.y * 5,
                        transform.up.z * transform.localScale.z * 5),
                    previousColliders.Select(x => x.GetComponent<Division>()).ToObjectList()
                );
            }
        }

        private void CheckUse(System.EventArgs args)
        {
            if (previousColliders.Count > 0 &&
                previousColliders.Contains(((UpgradeEventArgs)args).NetworkedMovingObject.gameObject))
            {
                Use();
            }
        }

        public override void Use(object obj = null)
        {
            if (obj == null)
            {
                localUses++;
            }

            base.Use(obj);

            if (fmodTrigger != null)
                fmodTrigger.TriggerParameters();
        }

        private bool CheckUpgradeValidity()
        {
            if (localUses >= maxLocalUses)
            {
                return false;
            }

            return true;
        }
    }
}