using GameStudio.HunterGatherer.Networking;
using UnityEngine;
using UnityEngine.Events;
using GameStudio.HunterGatherer.Divisions;
using Photon.Pun;
using GameStudio.HunterGatherer.Divisions.Upgrades;
using System.Text;

namespace GameStudio.HunterGatherer.Structures
{
    /// <summary> Check for possible interactions and if possible fire an event </summary>
    public class StructureInteract : MonoBehaviour
    {
        [Header("Max Uses")]
        [SerializeField] protected bool _hasMaxUses = true;
        [SerializeField, Range(1, 10)] protected int _maxUses = 1;
        protected int _uses = 0;
        public string onMaxUsesReached = "onMaxUsesReached";

        [Header("Interaction Checks")]
        [SerializeField] bool _checkHero = true;
        [SerializeField] bool _checkUnit = false;
        [SerializeField] bool _checkDivision = true;

        [Header("Events")]
        public string onStructureEnter = "onStructureEnter";
        public string onStructureExit = "onStructureExit";

        [Header("Fog of War")]
        [SerializeField] bool _visibleInFogOfWar = true;
        [SerializeField] GameObject structureModel;

        protected GameObject previousCollider = null;

        PhotonView myPhotonView;

        void Awake()
        {
            onStructureEnter += gameObject.GetInstanceID();
            onStructureExit += gameObject.GetInstanceID();
            onMaxUsesReached += gameObject.GetInstanceID();

            EventManager.Instance.AddEvent(onStructureEnter, true);
            EventManager.Instance.AddEvent(onStructureExit, true);
            EventManager.Instance.AddEvent(onMaxUsesReached);

            myPhotonView = GetComponent<PhotonView>();

            FogOfWarCheck();
        }

        private void OnEnable()
        {
            NetworkingService.AddNetworkingEventListener(Networking.Events.NetworkingEventType.UseStructure, Use);
        }

        public void OnDisable()
        {
            NetworkingService.RemoveNetworkingEventListener(Networking.Events.NetworkingEventType.UseStructure, Use);
        }

        //Mostly for developer purposes; Structure testing becomes a lot harder if you don't see them
        private void FogOfWarCheck()
        {
            if (_visibleInFogOfWar) structureModel.SetActive(true);
        }

        protected bool IsValidInteraction(Collider collider)
        {
            bool valid = false;

            valid = !_checkHero && !_checkUnit && !_checkDivision;
            valid = _checkHero ? collider.GetComponent<Division>() && collider.CompareTag("Division") && collider.GetComponent<Division>().SelectableObject.ObjectName == "Hero" || valid : valid;
            valid = _checkUnit ? collider.GetComponentInParent<Unit>() && collider.CompareTag("Unit") || valid : valid;
            valid = _checkDivision ? collider.GetComponent<Division>() && collider.CompareTag("Division") && collider.GetComponent<Division>().SelectableObject.ObjectName != "Hero" || valid : valid;

            return valid;
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
            if (_hasMaxUses && _uses >= _maxUses)
            {
                EventManager.Instance.Invoke(onMaxUsesReached);
                return;
            }

            NetworkedMovingObject collider = other.GetComponent<NetworkedMovingObject>();
            collider ??= other.GetComponentInParent<NetworkedMovingObject>();

            if ((!collider || !collider.IsMine))
                return;

            if (IsValidInteraction(other) && !this.previousCollider)
            {
                EventManager.Instance.Invoke(onStructureEnter, new NetworkMovingObjectArgs(collider));
                this.previousCollider = collider.gameObject;
            }
        }

        protected virtual void OnTriggerExit(Collider other)
        {
            NetworkedMovingObject collider = other.GetComponent<NetworkedMovingObject>();
            collider ??= other.GetComponentInParent<NetworkedMovingObject>();

            if (!collider || !collider.IsMine)
                return;

            if (IsValidInteraction(other) && this.previousCollider && this.previousCollider == collider.gameObject)
            {
                EventManager.Instance.Invoke(onStructureExit, new NetworkMovingObjectArgs(collider));
                this.previousCollider = null;
            }
        }

        public virtual void Use(object obj = null)
        {
            int id = System.Convert.ToInt32(obj);

            if (obj != null && id == myPhotonView.ViewID)
            {
                _uses++;
                CheckMaxUses();
            }
            else if (obj == null)
            {
                if (!NetworkingService.IsHost)
                    NetworkingService.RaiseEvent(Networking.Events.NetworkingEventType.UseStructure,
                                             myPhotonView.ViewID,
                                             Networking.Events.EventReceivers.Host);
                else
                {
                    _uses++;
                    CheckMaxUses();
                }
            }
        }

        public void CheckMaxUses()
        {
            if (_hasMaxUses && _uses >= _maxUses)
            {
                EventManager.Instance.Invoke(onMaxUsesReached);
                Destory();
            }
        }

        public void Destory()
        {
            if (NetworkingService.IsHost)
            {
                NetworkingService.Instance.Destroy(GetComponent<NetworkedObject>());
            }
        }
    }
}