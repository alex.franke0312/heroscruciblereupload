﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameStudio.HunterGatherer.DayAndNightCycle;
using GameStudio.HunterGatherer.Selection;
using GameStudio.HunterGatherer.BluePrints;

namespace GameStudio.HunterGatherer.Structures
{
    /// <summary> Handles the cancellation of the local highlighted base </summary>
    public class CancelLocalBluePrintPlacement : MonoBehaviour
    {
        private int endOfNight;
        private BluePrintPlacer bluePrintPlacer;
        private EventHourPair cancelBluePrintPlacement = new EventHourPair();
        private GameObject baseButton;
        private List<EventHourPair> eventHourPairs = new List<EventHourPair>();

        private void Awake()
        {
            baseButton = GameObject.Find("BaseButtonUI");
            if (baseButton) // will cause issues in Arena otherwise
            {
                bluePrintPlacer = baseButton.GetComponent<BluePrintPlacer>();
                eventHourPairs = DayNightCycle.Instance.FindEventHourPairByMethod("SetActiveBaseButton");
                //endOfNight = eventHourPairs[0].hour;
            }
        }

        private void OnEnable()
        {        
            AddCancellationEvent();
        }

        private void OnDisable()
        {
            cancelBluePrintPlacement.onHourMatched.RemoveListener(DestroyBlueBrint);
        }

        private void Update()
        {
            CancelPlacement();
        }

        /// <summary> Cancels the blueprint placement if the blueprint hasn't been placed yet when the base button disappears </summary>
        private void AddCancellationEvent()
        {
            cancelBluePrintPlacement.hour = endOfNight;
            cancelBluePrintPlacement.onHourMatched.AddListener(DestroyBlueBrint);
            if (bluePrintPlacer) // will cause issues in Arena otherwise
            {
                DayNightCycle.Instance.EventHourPairs.Add(cancelBluePrintPlacement);
            }
        }

        /// <summary> Cancel the blue print placement with right mouse click</summary>
        private void CancelPlacement()
        {
            if (Input.GetMouseButtonDown(1))
            {
                if(DayNightCycle.Instance.CurrentHourOfDay != endOfNight)
                {
                    baseButton.GetComponent<BaseButtonActivator>().SetActiveBaseButton(true);
                }

                SelectionManager.Instance.State = SelectionState.SelectAndInteract;
                DestroyBlueBrint();
            }
        }

        /// <summary> Destroy method that gets added as listener when the base button should deactivate </summary>
        private void DestroyBlueBrint()
        {
            SelectionManager.Instance.State = SelectionState.SelectAndInteract;
            if (bluePrintPlacer) // will cause issues in Arena otherwise
            {
                bluePrintPlacer.StopAllCoroutines();
            }
            Destroy(gameObject);
        }
    }
}
