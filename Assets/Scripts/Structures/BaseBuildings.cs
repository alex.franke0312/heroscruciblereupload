﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.UI;
using GameStudio.HunterGatherer.Selection;
using UnityEngine.UI;

namespace GameStudio.HunterGatherer.Structures
{
    /// <summary> Script used by a building in the base</summary>
    public abstract class BaseBuildings : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField]
        protected float offSet = 0;

        [SerializeField]
        protected float baseRange = 0;

        protected SelectableObject selectableObject;
        protected IBuildingUI UIInstance;

        public SelectableObject SelectableObject { get { return selectableObject; } }

        protected virtual void Start()
        {
            selectableObject = GetComponent<SelectableObject>();         
            selectableObject.OnSelect.AddListener(ActivateUtility);
            selectableObject.OnDeselect.AddListener(DeactivateUtility);     
        }

        protected void Update()
        {
            if (selectableObject.IsSelected)
            {
                UIInstance.PositionBuildingUtility(transform);
            }
        }

        /// <summary> Activate the utility specific for the building</summary>
        protected virtual void ActivateUtility()
        {
            if (GetComponent<NetworkedMovingObject>().IsMine)
            {
                UIInstance.ActivateBuildingUtility(true);
            }
            
        }

        /// <summary> Deactivate the utility specific for this building</summary>
        protected virtual void DeactivateUtility()
        {
            UIInstance.ActivateBuildingUtility(false);
        }
    }
}
