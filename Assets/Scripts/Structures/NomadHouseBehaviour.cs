using GameStudio.HunterGatherer;
using GameStudio.HunterGatherer.Divisions;
using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.Structures;
using GameStudio.HunterGatherer.UI;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NomadHouseBehaviour : StructureInteract
{
    //private static int id = 0;
    [SerializeField]
    private int nomadAmountInDivision = 8;

    [SerializeField]
    bool randomCombatUnit = false;

    string onSpawnDivision;

    void OnEnable()
    {
        GetComponent<Collider>().enabled = true;
        EventManager.Instance.AddListener(onStructureEnter, SpawnNomadDivision);
    }

    private void OnDisable()
    {
        EventManager.Instance.RemoveListener(onStructureEnter, SpawnNomadDivision);
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if (previousCollider != null)
            return;

        NetworkedMovingObject collider = other.GetComponent<NetworkedMovingObject>();
        collider ??= other.GetComponentInParent<NetworkedMovingObject>();

        if (!collider)
            return;

        //if player has max amount of divisions, dont give them nomads
        if ((NetworkingPlayerManager.Instance.PlayerDivisions.Count >= NetworkingPlayerManager.Instance.MaxDivisions))
        {
            return;
        }

        if (!collider.IsMine)
            return;

        if (IsValidInteraction(other) && !this.previousCollider)
        {
            previousCollider = other.gameObject;
            collider.enabled = false;
            EventManager.Instance.Invoke(onStructureEnter);
            this.previousCollider = collider.gameObject;
            Use();
        }

        previousCollider = null;
    }

    public override void Use(object obj = null)
    {
        base.Use(obj);
        gameObject.SetActive(false);
    }

    public void SpawnNomadDivision()
    {
        Division newDivision;

        if (randomCombatUnit)
        {
            int randomDivisionTypeMax = UnityEngine.Random.Range(0, (Enum.GetValues(typeof(DivisionType)).Length - 2));
            newDivision = NetworkingPlayerManager.Instance.SpawnDivision(transform.position, Quaternion.identity, (DivisionType)Enum.GetValues(typeof(DivisionType)).GetValue(randomDivisionTypeMax));
        }
        else
        {
            newDivision = NetworkingPlayerManager.Instance.SpawnDivision(transform.position, Quaternion.identity, DivisionType.Nomad);
        }

        if (!newDivision) return;
        for (int i = 0; i < nomadAmountInDivision - 1; i++)
        {
            newDivision.SpawnUnit();
        }

        UpgradeUIManager.Instance.Show(
            newDivision,
            transform.position + new Vector3(
                transform.up.x * transform.localScale.x,
                transform.up.y * transform.localScale.y,
                transform.up.z * transform.localScale.z),
            new List<UnityEngine.Object> { newDivision }
        );

        gameObject.SetActive(false);
    }
}
