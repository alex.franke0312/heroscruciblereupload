﻿using GameStudio.HunterGatherer.Divisions;
using GameStudio.HunterGatherer.Networking;
using System.Collections;
using UnityEngine;
using GameStudio.HunterGatherer.UI;
using UnityEngine.UI;
using GameStudio.HunterGatherer.DayAndNightCycle;
using UnityEngine.Events;
using System.Linq;
using System.Collections.Generic;

namespace GameStudio.HunterGatherer.Structures
{
    /// <summary> Script on the blueprint of the base handling canceling and final base creation </summary>
    public class BaseBluePrint : MonoBehaviour
    {
        [Header("References")]
        [SerializeField]
        private GameObject finishedBase = null;

        [Header("Settings")]
        [SerializeField]
        private float maxHeroDistance = 10f;

        [SerializeField]
        private float progressBarFillAmount = 0.01f;

        [SerializeField]
        private GameObject sliderPrefab = null;

        private Canvas canvas;
        private bool baseIsLoading;
        private Division heroDivision;
        private NetworkedMovingObject networkedMovingObject;
        private IBaseUI UIInstance;
        private GameObject baseButton;
        private GameObject progressBar;
        private List<EventHourPair> eventHourPairs = new List<EventHourPair>();

        private bool HasHeroArrived => Vector3.Distance(heroDivision.transform.position, transform.position) < maxHeroDistance;

        private void Awake()
        {
            networkedMovingObject = GetComponent<NetworkedMovingObject>();
            heroDivision = NetworkingPlayerManager.Instance.HeroDivision.GetComponent<Division>();
            canvas = FindObjectOfType<Canvas>();       
            baseButton = GameObject.Find("BaseButtonUI");
        }

        private void OnEnable()
        {
            this.enabled = networkedMovingObject.IsMine;

            if (this.enabled)
            {
                heroDivision.OnChangedGoal.AddListener(CancelBluePrintPlacementIfPlaced);
                CreateDestroyEvent();
            }
        }

        private void OnDisable()
        {
            if (networkedMovingObject.IsMine)
            {
                heroDivision.OnChangedGoal.RemoveListener(CancelBluePrintPlacementIfPlaced);
                eventHourPairs[0].onHourMatched.RemoveListener(DestroyBluePrintAtTime);
            }
        }

        /// <summary> Start checking the distance between the blueprint and the hero </summary>
        public void StartCheckingHeroDistance()
        {
            StartCoroutine(CheckHeroDistance());
        }

        /// <summary> Create an event to destroy the blueprint at a given time </summary>
        private void CreateDestroyEvent()
        {
            eventHourPairs = DayNightCycle.Instance.FindEventHourPairByMethod("SetActiveBaseButton");
            EventHourPair destroyBluePrint = new EventHourPair();
            eventHourPairs[0].onHourMatched.AddListener(DestroyBluePrintAtTime);
            DayNightCycle.Instance.EventHourPairs.Add(destroyBluePrint);
        }

        /// <summary> Destroy the blueprint when the game time equals a given time </summary>
        private void DestroyBluePrintAtTime()
        {
            if (progressBar != null)
            {
                Destroy(progressBar.gameObject);
            }
               
            StopCoroutine(CheckHeroDistance());
            heroDivision.MoveOrder(heroDivision.transform.position, heroDivision.MoveTarget.Direction);
            baseButton.GetComponent<BaseButtonActivator>().SetActiveBaseButton(false);
            NetworkingService.Instance.Destroy(networkedMovingObject);
        }

        /// <summary> Check the distance between the hero and the base. If the hero is close enough, start spawning the final base</summary>
        private IEnumerator CheckHeroDistance()
        {       
            while (!HasHeroArrived)
            {
                yield return null;
            }
            
            StartCoroutine(LoadBase());
        }

        /// <summary> Cancels the placement of the blueprint </summary>
        private void CancelBluePrintPlacementIfPlaced(DivisionGoal goal)
        {
            if (goal != DivisionGoal.PlaceBase)
            {      
                baseButton.GetComponent<BaseButtonActivator>().SetActiveBaseButton(true);

                if(progressBar != null)
                {
                    Destroy(progressBar.gameObject);
                }
                StopCoroutine(CheckHeroDistance());
                NetworkingService.Instance.Destroy(networkedMovingObject);                          
            }
        }

        /// <summary> Load in the final version of the base seen in multiplayer </summary>
        private IEnumerator LoadBase()
        {
            progressBar = Instantiate(sliderPrefab, canvas.transform);
            UIInstance = new BaseBluePrintUI(progressBar.GetComponent<Slider>(), canvas);
            UIInstance.ActivateProgressBar(true);
            baseIsLoading = true;

            while (baseIsLoading)
            {
                if (DayNightCycle.Instance.IsDay)
                {
                    yield break;
                }

                UIInstance.PositionProgressBar(transform);
                UIInstance.ProgressBar.value += progressBarFillAmount;

                if (UIInstance.ProgressBar.value == UIInstance.ProgressBar.maxValue)
                {
                    UIInstance.Destroy();
                    UIInstance = null;

                    NetworkingService.Instance.Instantiate(finishedBase.name, transform.position,
                                         Quaternion.identity).GetComponent<NetworkedMovingObject>();

                    NetworkingService.Instance.Destroy(networkedMovingObject);
                    baseIsLoading = false;
                    StopCoroutine(LoadBase());
                    yield break;
                }
                yield return null;
            }
        }
    }
}
