﻿using UnityEngine;
using UnityEngine.UI;

namespace GameStudio.HunterGatherer.UI
{
    /// <summary> Interface for methods used for UI on the base</summary>
    public interface IBaseUI
    {
        Slider ProgressBar { get; }
        Canvas Canvas { get; }

        /// <summary> Activate the progressbar </summary>
        void ActivateProgressBar(bool isActivated);

        /// <summary> Position the progressbar on a given transform</summary>
        void PositionProgressBar(Transform transform);

        /// <summary> Destroy function called to destroy UI elements </summary>
        void Destroy();
    }
}
