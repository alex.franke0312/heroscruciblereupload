﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameStudio.HunterGatherer.UI
{
    /// <summary> Interface for activating UI elements within the base </summary>
    public interface IBuildingUI
    {
        Button Button { get; }
        Slider ProgressBar { get; }
        Canvas Canvas { get; }
        RectTransform UtilityUI { get; }

        /// <summary> Activate the button for building a building </summary>
        void ActivateBuildButton(bool isActivated, Button button);

        /// <summary> Activate the progressbar for loading a building </summary>
        void ActivateProgressBar(bool isActivated, Slider progressBar);

        /// <summary> Activate the UI for the specific building utility </summary>
        void ActivateBuildingUtility(bool isActivated);

        /// <summary> Position the button for building a building on said building </summary>
        void PositionBuildButton(Transform transform, Button button);

        /// <summary> Position the progressbar for loading a building on said building </summary>
        void PositionProgressBar(Transform transform, Slider progressBar);

        /// <summary> Position the UI for the specific building utility on said building </summary>
        void PositionBuildingUtility(Transform transform);
    }
}

