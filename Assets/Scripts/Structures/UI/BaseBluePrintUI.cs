﻿using GameStudio.HunterGatherer.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace GameStudio.HunterGatherer.UI
{
    /// <summary> Class handeling the positioning of UI on the blueprint </summary>
    public class BaseBluePrintUI : IBaseUI
    {
        private Slider progressBar;
        private Canvas canvas;

        public Slider ProgressBar => progressBar;
        public Canvas Canvas => canvas;

        public BaseBluePrintUI(Slider progressBar, Canvas canvas)
        {
            this.progressBar = progressBar;
            this.canvas = canvas;
        }

        /// <summary> Activate the progressbar </summary>
        public void ActivateProgressBar(bool isActivated)
        {
            progressBar.gameObject.SetActive(isActivated);
        }

        /// <summary> Position the progressbar on the blueprint </summary>
        public void PositionProgressBar(Transform transform)
        {
            CanvasHelper.AlignnUIInWorldSpace((RectTransform)progressBar.transform, Canvas.GetComponent<RectTransform>(), transform, 0f);
        }

        /// <summary> Destroy the progressbar </summary>
        public void Destroy()
        {
            Object.Destroy(progressBar.gameObject);
        }
    }
}
