﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameStudio.HunterGatherer.DayAndNightCycle;
using GameStudio.HunterGatherer.Networking;

namespace GameStudio.HunterGatherer.Structures
{
    /// <summary> Script placed on the multitplayer finished base handling disassemblemechanic</summary>
    public class Base : MonoBehaviour
    {
        private NetworkedMovingObject networkMovingObject;
        private Bounds bounds;
        private List<EventHourPair> eventHourPairs;

        private void Awake()
        {
            networkMovingObject = GetComponent<NetworkedMovingObject>();
        }

        private void OnEnable()
        {
            if (!networkMovingObject.IsMine)
            {
                return;
            }

            CalculateBaseSize();
            CreateDisassembleEvent();
        }

        /// <summary> Calculates the accurate size of the base </summary>
        private void CalculateBaseSize()
        {
            bounds = new Bounds(transform.position, Vector3.zero);

            foreach (Renderer r in GetComponentsInChildren<Renderer>())
            {
                bounds.Encapsulate(r.bounds);
            }
        }

        /// <summary> Create a new event in the morning for disassembling the base </summary>
        private void CreateDisassembleEvent()
        {
            eventHourPairs = DayNightCycle.Instance.FindEventHourPairByMethod("SetActiveBaseButton");
            eventHourPairs[0].onHourMatched.AddListener(DisassembleBase);
        }

        /// <summary> Disassemble the base </summary>
        private void DisassembleBase()
        {
            if (!networkMovingObject.IsMine)
            {
                return;
            }
            NetworkingService.Instance.Destroy(networkMovingObject);
            eventHourPairs?[0].onHourMatched.RemoveListener(DisassembleBase);
        }
    }
}
