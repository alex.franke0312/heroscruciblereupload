﻿using GameStudio.HunterGatherer.Divisions;
using GameStudio.HunterGatherer.Divisions.UI;
using GameStudio.HunterGatherer.Networking;
using System.Collections.Generic;
using UnityEngine;

namespace GameStudio.HunterGatherer.Structures
{
    /// <summary>Checks if a division is in range, then triggers the OnEnterExitBase event</summary>
    [RequireComponent(typeof(Collider))]
    public class BaseDivisionEditRange : MonoBehaviour
    {
        [Header("References")]
        [SerializeField]
        private NetworkedMovingObject baseNetworkedMovingObject = null;

        [SerializeField]
        private Base baseStructure = null;

        private List<Division> divisionsInRange = new List<Division>();
        private List<Division> enemyDivisionsInBase = new List<Division>();
        private static DivisionOverview divisionOverview;

        private void OnEnable()
        {
            if(divisionOverview == null)
            {
               divisionOverview = FindObjectOfType<DivisionOverview>();
            }
        }

        private void OnDisable()
        {
            divisionsInRange.ForEach(x => x.OnEnterExitBase.Invoke(false, baseStructure));

            for(int i = enemyDivisionsInBase.Count; i--> 0;)
            {
                OnEnemyDivisionExit(enemyDivisionsInBase[i]);
            }
        }

        private void Update()
        {
            if (baseNetworkedMovingObject.IsMine)
            {
                if (enemyDivisionsInBase.Count > 0)
                {
                    divisionOverview.EnemyIsInBase = true;
                }
                else
                {
                    divisionOverview.EnemyIsInBase = false;         
                }
            }
        }

        private void OnDestroy()
        {
            divisionOverview = null;
        }

        /// <summary> Add an enemy division to the EnemyDivisionsInBase List </summary>
        private void OnEnemyDivisionEnter(Division division)
        {
            division.OnDisableDivision.AddListener(OnEnemyDivisionExit);
            if (!enemyDivisionsInBase.Contains(division))
            {
                enemyDivisionsInBase.Add(division);
            }
        }

        /// <summary>Remove an enemy division from the EnemyDivisionsInBase List when it exits the base</summary>
        private void OnEnemyDivisionExit(Division division)
        {
            division.OnDisableDivision.RemoveListener(OnEnemyDivisionExit);
            if (enemyDivisionsInBase.Contains(division))
            {
                enemyDivisionsInBase.Remove(division);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!baseNetworkedMovingObject.IsMine)
            {
                return;
            }

            Division division = other.GetComponent<Division>();

            if (division == null)
            {
                return;
            }

            if (!division.IsMine)
            {
                OnEnemyDivisionEnter(division);
            }
            else
            {
                divisionsInRange.Add(division);
                division.OnEnterExitBase.Invoke(true, baseStructure);
            }           
         
        }

        private void OnTriggerExit(Collider other)
        {
            if (!baseNetworkedMovingObject.IsMine)
            {
                return;
            }

            Division division = other.GetComponent<Division>();

            if (division == null)
            {
                return;
            }

            if (!division.IsMine)
            {
                OnEnemyDivisionExit(division);
            }
            else
            {
                divisionsInRange.Remove(division);
                division.OnEnterExitBase.Invoke(false, baseStructure);
            }                   
        }
    }
}