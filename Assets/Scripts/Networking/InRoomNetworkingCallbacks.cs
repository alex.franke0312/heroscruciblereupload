﻿namespace GameStudio.HunterGatherer.Networking
{
    /// <summary>Interface to implement when a class wants callbacks on important events in room </summary>
    public interface IInRoomNetworkingCallbacks
    {
        /// <summary>Called when a player has entered to room</summary>
        void OnPlayerEnteredRoom(Player player);

        /// <summary>Called when a player has left to room</summary>
        void OnPlayerLeftRoom(Player player);

        /// <summary>Called when a new host has been assigned</summary>
        void OnHostMigrated(Player player);
    }
}