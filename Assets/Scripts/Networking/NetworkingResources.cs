﻿using GameStudio.HunterGatherer.Networking.Events;
using GameStudio.HunterGatherer.SceneManagement;
using Photon.Pun;
using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;

namespace GameStudio.HunterGatherer.Networking
{
    using Photon.Realtime;
    using UnityScene = UnityEngine.SceneManagement.Scene;
    using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;

    /// <summary>Custom used solution for pooling of networked gameobjects</summary>
    public class NetworkingResources : MonoBehaviour, IPunPrefabPool
    {
        [SerializeField, Tooltip("Prefabs of moving objects that need to be synchronized")]
        private GameObject[] movingObjects = null;

        [SerializeField, Tooltip("Prefabs of static objects that do not need to be synchronized")]
        private GameObject[] staticObjects = null;

        private Dictionary<string, ConcurrentQueue<GameObject>> resourcePoolDict =
            new Dictionary<string, ConcurrentQueue<GameObject>>();

        private static Dictionary<int, GameObject> staticObjectDict = new Dictionary<int, GameObject>();

        private static Transform PoolParent;
        private static Transform movingObjectParent;
        private static Transform staticObjectParent;

        private void Awake()
        {
            VerifyMovingObjects();
            VerifyStaticObjects();
            CreatePoolParents();

            UnitySceneManager.sceneUnloaded += OnSceneUnloaded;
            PhotonNetwork.PrefabPool = this;
        }

        private void Start()
        {
            NetworkingService.AddNetworkingEventListener(NetworkingEventType.SceneObjectSpawn, OnSceneObjectSpawn);
            NetworkingService.AddNetworkingEventListener(NetworkingEventType.StaticObjectSpawn, OnStaticObjectSpawn);
            NetworkingService.AddNetworkingEventListener(NetworkingEventType.StaticObjectDestroy,
                OnStaticObjectDestroy);
        }

        private void OnDestroy()
        {
            NetworkingService.RemoveNetworkingEventListener(NetworkingEventType.SceneObjectSpawn, OnSceneObjectSpawn);
            NetworkingService.RemoveNetworkingEventListener(NetworkingEventType.StaticObjectSpawn, OnStaticObjectSpawn);
            NetworkingService.RemoveNetworkingEventListener(NetworkingEventType.StaticObjectDestroy,
                OnStaticObjectDestroy);
        }

        /// <summary>creates pool parent in scene and make sure it doesn't get destroyed when loading new scenes </summary>
        private void CreatePoolParents()
        {
            if (PoolParent != null)
            {
                GameObject.Destroy(PoolParent.gameObject);
            }

            PoolParent = new GameObject("PoolParent").transform;

            movingObjectParent = new GameObject("MovingObjects").transform;
            movingObjectParent.parent = PoolParent;

            staticObjectParent = new GameObject("StaticObjects").transform;
            staticObjectParent.parent = PoolParent;

            DontDestroyOnLoad(PoolParent.gameObject);
        }

        /// <summary>Returns a transform based on given movingObject flag</summary>
        public static Transform GetPoolParent(bool movingObject)
        {
            return movingObject ? movingObjectParent : staticObjectParent;
        }

        /// <summary>Returns a gameobject that can be either be a moving object or static object, based on given viewid</summary>
        public static GameObject Find(int viewId)
        {
            if (viewId > NetworkedStaticObject.BaseIdNumber)
            {
                if (staticObjectDict.ContainsKey(viewId))
                {
                    return staticObjectDict[viewId];
                }

                return null;
            }

            return PhotonView.Find(viewId)?.gameObject;
        }

        /// <summary>Returns units owned by player with given actor number</summary>
        public static List<GameObject> GetUnitsOfPlayer(int playerActorNr)
        {
            PhotonView[] views = PhotonNetwork.PhotonViews;
            List<GameObject> units = new List<GameObject>();
            for (int i = 0; i < views.Length; i++)
            {
                if (views[i].OwnerActorNr == playerActorNr)
                {
                    units.Add(views[i].gameObject);
                }
            }

            return units;
        }

        /// <summary>Verifies moving objects to be pooled in the moving objects array</summary>
        private void VerifyMovingObjects()
        {
            foreach (GameObject prefab in movingObjects)
            {
                if (prefab.activeSelf)
                {
                    //to comply with photon standards the resource needs to be inactive
                    Debug.LogWarning("prefab with name " + prefab.name +
                                     "was active which is not complient with photon instantiation rules");
                    prefab.SetActive(false);
                }

                PhotonView photonView = prefab.GetComponent<PhotonView>();
                if (photonView == null || photonView.OwnershipTransfer != OwnershipOption.Takeover)
                {
                    Debug.LogError("prefab with photonview " + photonView +
                                   " is either null or doesn't have the takeover ownership option enabled");
                    continue;
                }

                resourcePoolDict.Add(prefab.name, new ConcurrentQueue<GameObject>());
            }
        }

        /// <summary>Verifies static objects to be pooled in the static objects array</summary>
        private void VerifyStaticObjects()
        {
            foreach (GameObject prefab in staticObjects)
            {
                if (prefab.GetComponent<NetworkedStaticObject>() == null)
                {
                    Debug.LogError("prefab with name " + prefab.name +
                                   " doesn't have a NetworkedStaticObject component. This is necessary");
                    continue;
                }

                NetworkedMovingObject movable = prefab.GetComponent<NetworkedMovingObject>();
                if (movable != null)
                {
                    Destroy(movable);
                    Debug.LogError("static object  " + prefab.name +
                                   " can't have a network moving object component. Remove this");
                    continue;
                }

                resourcePoolDict.Add(prefab.name, new ConcurrentQueue<GameObject>());
            }
        }

        /// <summary>Called when a client wants the host to spawn a scene object, it spawns a sceneobject based on given information</summary>
        private void OnSceneObjectSpawn(object content)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                SceneObjectSpawnInfo info = (SceneObjectSpawnInfo) content;
                PhotonNetwork.InstantiateSceneObject(info.PrefabId, info.Position, info.Rotation);
            }
        }

        /// <summary>Called when an event has been raised to spawn a static object, it will create one using the given information</summary>
        private void OnStaticObjectSpawn(object content)
        {
            SceneObjectSpawnInfo info = (SceneObjectSpawnInfo) content;

            //try getting a new resource from the poolad game objects stored in the queue
            GameObject instance = null;
            ConcurrentQueue<GameObject> pooledGameObjects = resourcePoolDict[info.PrefabId];
            if (pooledGameObjects.TryDequeue(out instance))
            {
                instance.transform.position = info.Position;
                instance.transform.rotation = info.Rotation;
            }
            else
            {
                //if there are no avaialbe pooled game objects, the used resource for instantiation will be the original prefab
                GameObject res = null;
                foreach (GameObject prefab in staticObjects)
                {
                    if (prefab.name == info.PrefabId)
                    {
                        res = prefab;
                        break;
                    }
                }

                instance = Instantiate(res, info.Position, info.Rotation);
                instance.name = res.name;

                //set static object view id and add it to stored object dictionary searches
                NetworkedStaticObject obj = instance.GetComponent<NetworkedStaticObject>();
                obj.SetViewId(staticObjectDict.Count);
                staticObjectDict.Add(obj.ViewId, instance);
            }

            instance.SetActive(true);
        }

        /// <summary>Called when an event has been raised to destroy a static object, it will reset the gameobject and return it to the pool</summary>
        private void OnStaticObjectDestroy(object content)
        {
            GameObject go = staticObjectDict[(int) content];

            //destroy gameobject normally and exit if player destroys object when not in a room
            if (PhotonNetwork.NetworkClientState != ClientState.Joined)
            {
                GameObject.Destroy(go);
                return;
            }

            if (resourcePoolDict.ContainsKey(go.name))
            {
                go.transform.position = Vector3.zero;
                go.transform.rotation = Quaternion.identity;
                go.SetActive(false);
                resourcePoolDict[go.name].Enqueue(go);
            }
            else
            {
                Debug.LogWarning("Failed Destroying static networked gameobject with name " + go.name +
                                 " because it wasn't in the resource dictionary");
            }
        }

        /// <summary>Returns whether given prefabId corresponds to a static object</summary>
        public bool IsStaticObject(string prefabId)
        {
            for (int i = 0; i < staticObjects.Length; i++)
            {
                if (staticObjects[i].name == prefabId)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>Should be called before a game scene is unloaded to destroy all pooled objects related to it</summary>
        private void OnSceneUnloaded(UnityScene scene)
        {
            if (!SceneManager.Instance.IsGameScene(scene))
            {
                return;
            }

            //empty queue for every prefab
            GameObject empty = null;
            foreach (var resource in resourcePoolDict)
            {
                while (resource.Value.TryDequeue(out empty))
                {
                }
            }

            staticObjectDict.Clear();
        }

        /// <summary>Will either destroy or pool the given object depending on state of player in the room</summary>
        public void Destroy(GameObject go)
        {
            //destroy gameobject normally and exit if player destroys object when not in a room
            if (PhotonNetwork.NetworkClientState != ClientState.Joined)
            {
                GameObject.Destroy(go);
                return;
            }

            if (resourcePoolDict.ContainsKey(go.name))
            {
                //if the gameobject to be destroyed is in the resource dictionary it can be reset and enqueued for reusing
                go.transform.position = Vector3.zero;
                go.transform.rotation = Quaternion.identity;
                go.SetActive(false);
                //transfer ownership to scene while this gameobject is inactive
                go.GetComponent<PhotonView>().TransferOwnership(0);
                resourcePoolDict[go.name].Enqueue(go);
            }
            else
            {
                Debug.LogWarning("Failed Destroying gameobject with name " + go.name +
                                 " because it wasn't in the resource dictionary");
            }
        }

        /// <summary>Will either return an instantiated or pooled object</summary>
        public GameObject Instantiate(string prefabId, Vector3 position, Quaternion rotation)
        {
            //exit with error if the prefabId is a key in the resourcePool
            if (!resourcePoolDict.ContainsKey(prefabId))
            {
                Debug.LogError("Failed instantiating object with name " + prefabId +
                               ". Make sure it matches the original prefab name");
                return null;
            }

            //try getting a new resource from the poolad game objects stored in the queue
            GameObject instance = null;
            ConcurrentQueue<GameObject> pooledGameObjects = resourcePoolDict[prefabId];
            if (pooledGameObjects.TryDequeue(out instance))
            {
                instance.transform.position = position;
                instance.transform.rotation = rotation;
            }
            else
            {
                //if there are no avaialbe pooled game objects, the used resource for instantiation will be the original prefab
                GameObject res = null;
                foreach (GameObject prefab in movingObjects)
                {
                    if (prefab.name == prefabId)
                    {
                        res = prefab;
                        break;
                    }
                }

                instance = Instantiate(res, position, rotation);
                instance.name = res.name;
            }

            return instance;
        }
    }
}