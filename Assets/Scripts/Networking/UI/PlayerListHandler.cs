﻿using TMPro;
using UnityEngine;
using GameStudio.HunterGatherer.SceneManagement;
using UnityEngine.UI;
using GameStudio.HunterGatherer.Networking.Events;

namespace GameStudio.HunterGatherer.Networking.UI
{
    /// <summary>Handles the players just joining the room by providing functionality for players joining, leaving and starting the game</summary>
    public class PlayerListHandler : MonoBehaviour, IInRoomNetworkingCallbacks
    {
        [System.Serializable]
        private struct PlayerListItem
        {
//#pragma warning disable 0649
            public GameObject Root;
            public TMP_Text TxtItemText;
            public GameObject IconPerson;
            public GameObject GodIcon;
//#pragma warning restore 0649
        }

        [SerializeField]
        private PlayerListItem[] items = null;

        [SerializeField]
        private GameObject gameTitle = null;

        [SerializeField]
        private Button btnPlay = null;

        [SerializeField]
        private Button btnLeave = null;

        [SerializeField]
        private Image imgPlayFilled = null;

        [SerializeField]
        private GameObject multiplayerWindow = null;

        [SerializeField]
        private LoginFormHandler loginFormHandler = null;

        [SerializeField]
        private GameObject menuButtons = null;

        [SerializeField]
        private TMP_Text txtRoom = null;

        [SerializeField]
        private GameObject selectionScreen = null;

        [SerializeField]
        private GameObject godSelect = null;

        [SerializeField]
        private GameObject tutorialScreen = null;

        [HideInInspector]
        public Player[] players;

        public string SceneToLoad { get; set; } = "Game";

        private void Awake()
        {
            btnPlay.onClick.AddListener(OnPlayButtonClick);
            btnLeave.onClick.AddListener(OnLeaveButtonClick);

            NetworkingService.Instance.Disconnected += OnDisconnected;
            NetworkingService.Instance.JoinedRoom += Open;
            NetworkingService.AddNetworkingEventListener(NetworkingEventType.LobbyEnd, OnLobbyEnd);

            gameObject.SetActive(false);
            //godSelect.SetActive(false); // Legacy code. God Selection is a removed feature
            tutorialScreen.SetActive(false);
        }

        private void OnEnable()
        {
            NetworkingService.AddInRoomCallbacksTarget(this);
        }

        private void OnDisable()
        {
            NetworkingService.RemoveInRoomCallbacksTarget(this);
        }

        private void OnDestroy()
        {
            NetworkingService.Instance.Disconnected -= OnDisconnected;
            NetworkingService.Instance.JoinedRoom -= Open;
            NetworkingService.RemoveNetworkingEventListener(NetworkingEventType.LobbyEnd, OnLobbyEnd);
        }

        /// <summary>Shows gameobject with player items related to current players in room if not offline and in a room</summary>
        public void Open()
        {
            //dont open if the room we joined was an offline room or not in a room at all
            if (NetworkingService.OfflineMode || !NetworkingService.InRoom)
            {
                return;
            }

            //show gameobject
            selectionScreen.SetActive(true);
            gameObject.SetActive(true);
            //godSelect.SetActive(true); // Legacy code. God Selection is a removed feature
            multiplayerWindow.SetActive(false);
            menuButtons.SetActive(false);
            gameTitle.SetActive(false);
            tutorialScreen.SetActive(true);

            txtRoom.text = NetworkingService.CurrentRoom.Name;

            //only the master client can use the play button
            btnPlay.gameObject.SetActive(NetworkingService.IsHost);

            //get max player count and player list from photon
            int maxPlayers = NetworkingService.CurrentRoom.MaxPlayers;
            Player[] playerList = NetworkingService.PlayerList;

            //fill players array with players listed
            players = new Player[maxPlayers];
            for (int i = 0; i < playerList.Length; i++)
            {
                players[i] = playerList[i];
            }

            //update player items based on players in players array
            for (int i = 0; i < maxPlayers; i++)
            {
                items[i].Root.SetActive(true);

                if (players[i] != null)
                {
                    items[i].TxtItemText.text = players[i].NickName;
                    items[i].IconPerson.SetActive(true);
                }
                else
                {
                    items[i].TxtItemText.text = "";
                    items[i].IconPerson.SetActive(false);
                    // items[i].GodIcon.SetActive(false); // Legacy code. God Selection is a removed feature
                }
            }
        }

        /// <summary>Should be called when a player enters the room to update the player list with new player</summary>
        public void OnPlayerEnteredRoom(Player player)
        {
            if (players == null)
            {
                return;
            }

            for (int i = 0; i < players.Length; i++)
            {
                if (players[i] == null)
                {
                    players[i] = player;
                    items[i].TxtItemText.text = player.NickName;
                    items[i].IconPerson.SetActive(true);
                    break;
                }
            }
        }

        /// <summary>Should be called when a new master client has been assigned to update the visiablity of the play button</summary>>
        public void OnHostMigrated(Player newHost)
        {
            //set button visibility based on if we are the new host
            btnPlay.gameObject.SetActive(newHost.IsLocal);
        }

        /// <summary>Should be called when a player leaves the room to remove leaving player from list</summary>
        public void OnPlayerLeftRoom(Player player)
        {
            if (players == null)
            {
                return;
            }

            for (int i = 0; i < players.Length; i++)
            {
                if (players[i] == player)
                {
                    players[i] = null;
                    items[i].TxtItemText.text = "";
                    items[i].IconPerson.SetActive(false);
                    items[i].GodIcon.SetActive(false);
                    break;
                }
            }
        }

        /// <summary>Should be called when the lobby state ends to start loading the specified game scene. Falls back to "Game" when left empty.</summary>
        private void OnLobbyEnd(object content)
        {
            string scene = content as string;
            string sceneToLoad = string.IsNullOrEmpty(scene) ? "Game" : scene;
            if (NetworkingService.IsConnected)
            {
                btnPlay.interactable = false;
                SceneManager.Instance.LoadGameScene(sceneToLoad);
            }
        }

        /// <summary>Should be called when the player presses the leave button to leave and disconnect and hide the gameobject</summary>
        public void OnLeaveButtonClick()
        {
            btnLeave.interactable = false;
            NetworkingService.Instance.Disconnect();
        }

        /// <summary>Should be called when the play button has been clicked by the master client so send the endlobby message </summary>
        private void OnPlayButtonClick()
        {
            btnPlay.interactable = false;
            NetworkingService.SendEndLobby(SceneToLoad);
        }

        /// <summary>Should be called when the player disconnects to hide this gameobject if it is active</summary>
        private void OnDisconnected(string cause)
        {
            players = null;
            btnLeave.interactable = true;
            btnPlay.interactable = true;
            imgPlayFilled.fillAmount = 0f;

            gameTitle.SetActive(true);
            selectionScreen.SetActive(false);
            gameObject.SetActive(false);
            //godSelect.SetActive(false); // Legacy code. God Selection is a removed feature
            tutorialScreen.SetActive(false);

            multiplayerWindow.SetActive(true);
            menuButtons.SetActive(true);
            loginFormHandler.Open();
        }
    }
}