﻿using System;
using TMPro;
using UnityEngine;
using GameStudio.HunterGatherer.SceneManagement;
using UnityEngine.UI;

namespace GameStudio.HunterGatherer.Networking.UI
{
    /// <summary>Handles the logging in of the user by providing options for username and roomname to connect and join a room with</summary>
    public class LoginFormHandler : MonoBehaviour
    {
        [Header("References")]
        [SerializeField]
        private InputField inputFieldUsername = null;

        [SerializeField]
        private InputField inputFieldRoomname = null;

        [SerializeField]
        private TMP_Text txtLogin = null;

        [SerializeField]
        private Button btnLogin = null;

        private Action closed;

        private string username;
        private string roomname;

        private void Awake()
        {
            inputFieldUsername.onEndEdit.AddListener(OnUsernameEditEnd);
            inputFieldRoomname.onEndEdit.AddListener(OnRoomnameEditEnd);

            NetworkingService.Instance.Connected += OnConnected;
            NetworkingService.Instance.Disconnected += OnDisconnected;
            NetworkingService.Instance.JoinedRoom += OnJoinedRoom;
            NetworkingService.Instance.JoinRoomFailed += OnJoinRoomFailed;
            NetworkingService.Instance.LeftRoom += OnLeftRoom;
        }

        private void OnDestroy()
        {
            NetworkingService.Instance.Connected -= OnConnected;
            NetworkingService.Instance.Disconnected -= OnDisconnected;
            NetworkingService.Instance.JoinedRoom -= OnJoinedRoom;
            NetworkingService.Instance.JoinRoomFailed -= OnJoinRoomFailed;
            NetworkingService.Instance.LeftRoom -= OnLeftRoom;
        }

        private void OnEnable()
        {
            btnLogin.enabled = true;
        }

        /// <summary>Use to show the form so the user can connect with a username to a room</summary>
        public void Open()
        {
            gameObject.SetActive(true);
            txtLogin.text = "Login";
            OnUsernameEditEnd(inputFieldUsername.text);
            OnRoomnameEditEnd(inputFieldRoomname.text);
        }

        /// <summary>Subscribes callback so that it Will be called when the form has been closed</summary>
        public void WaitForClose(Action callback)
        {
            closed += callback;
        }

        /// <summary>Should be called after the close animation has finished to reset this object</summary>
        public void OnClosed()
        {
            inputFieldUsername.text = "";
            inputFieldRoomname.text = "";

            closed?.Invoke();
            closed = null;

            transform.localScale = Vector3.one;

            gameObject.SetActive(false);
        }

        /// <summary>Should be called when the username inputfield has been edited</summary>
        public void OnUsernameEditEnd(string edit)
        {
            if (!edit.Contains(" "))
            {
                username = edit;
            }
        }

        /// <summary>Should be called when the roomname inputfield has been edited</summary>
        public void OnRoomnameEditEnd(string edit)
        {
            roomname = edit;
        }

        /// <summary>Should be called when a player chooses to play in offline mode by clicken the play offline text</summary>
        public void OnPlayOfflineClick()
        {
            NetworkingService.SetOffline();
        }

        /// <summary>Should be called when the login button has been clicked to connect user inputted username and roomname</summary>
        public void OnLoginButtonClick()
        {
            bool hasUsername = !string.IsNullOrEmpty(username);
            bool hasRoomname = !string.IsNullOrEmpty(roomname);
            if (hasUsername && hasRoomname)
            {
                txtLogin.text = "Connecting";
                btnLogin.enabled = false;
                NetworkingService.Instance.ConnectUserToRoomInDefaultLobby(username, roomname);
            }
            else
            {
                if (!hasUsername)
                {
                    inputFieldUsername.text = "";
                }

                if (!hasRoomname)
                {
                    inputFieldRoomname.text = "";
                }
            }
        }

        /// <summary>Use to let the form know whether the connection went succesfully</summary>
        private void OnConnected(bool online)
        {
            txtLogin.text = online ? "Joining Room" : "Joining Offline Room";
            roomname = null;
            username = null;
        }

        /// <summary>Use to let the form know </summary>
        private void OnDisconnected(string cause)
        {
            if (gameObject.activeInHierarchy)
            {
                txtLogin.text = "Login";
                btnLogin.enabled = true;
            }
        }

        /// <summary>Use to let the form know whether the joining/creating went succesfully </summary>
        private void OnJoinedRoom()
        {
            if (NetworkingService.OfflineMode)
            {
                txtLogin.text = "Loading Game";
                SceneManager.Instance.LoadGameScene("Game");
            }
            else
            {
                txtLogin.text = "Joined";
            }
        }

        /// <summary>Called when joining a room has failed to reset ui elements</summary>
        private void OnJoinRoomFailed(string message)
        {
            Debug.LogWarning("Joining Room failed :: " + message);
            txtLogin.text = "Login";
            btnLogin.enabled = true;
        }

        /// <summary>Should be called when the player leaves a room</summary>
        private void OnLeftRoom()
        {
            if (!gameObject.activeInHierarchy)
            {
                Open();
            }
        }
    }
}