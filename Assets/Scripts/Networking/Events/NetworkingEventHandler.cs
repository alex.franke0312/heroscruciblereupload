﻿using ExitGames.Client.Photon;
using GameStudio.HunterGatherer.Environment;
using GameStudio.HunterGatherer.GameState;
using GameStudio.HunterGatherer.Selection;
using GameStudio.HunterGatherer.Structures;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameStudio.HunterGatherer.Networking.Events
{
    using PhotonPlayer = Photon.Realtime.Player;

    /// <summary>The Networking event hanlder manages the raising of events and event callbacks of the nerworking service</summary>
    public class NetworkingEventHandler
    {
        private Dictionary<NetworkingEventType, Action<object>> events = new Dictionary<NetworkingEventType, Action<object>>();

        private Dictionary<int, Action<object>> requests = new Dictionary<int, Action<object>>();

        private List<NetworkingRequest> requestsInProgress = new List<NetworkingRequest>();

        /// <summary>Returns whether a code equates to a NetworkingRequestType</summary>
        private bool IsNetworkingRequestCode(byte code) => Enum.IsDefined(typeof(NetworkingRequestType), code);

        /// <summary>Returns whether a code equates to a NetworkingRequestDecision</summary>
        private bool IsNetworkingRequestDecision(byte code) => Enum.IsDefined(typeof(NetworkingRequestDecision), code);

        /// <summary>Returns whether the given code equates to a PunEvent</summary>
        private bool IsPunEvent(byte code) => code >= 200;

        private event Action<NetworkingRequestType, int, Action<NetworkingRequestType, int>, NetworkingRequestDelay> OnHandleRequest;

        private SerializationService serializationService = new SerializationService();

        public NetworkingEventHandler(Action<NetworkingRequestType, int, Action<NetworkingRequestType, int>, NetworkingRequestDelay> requestHandler)
        {
            OnHandleRequest += requestHandler;
        }

        /// <summary>Clears cached data from the event handler</summary>
        public void Clear()
        {
            requests.Clear();
            requestsInProgress.Clear();
        }

        /// <summary>Raises event of given type, with given content, send at given receivergroup and with given reliability</summary>
        public void RaiseEvent(NetworkingEventType type, object content, ReceiverGroup receivers, bool sendReliable)
        {
            RaiseEventOptions eventOptions = new RaiseEventOptions { Receivers = receivers };
            SendOptions sendOptions = sendReliable ? SendOptions.SendReliable : SendOptions.SendUnreliable;
            PhotonNetwork.RaiseEvent((byte)type, content, eventOptions, sendOptions);
        }

        /// <summary>Raises event of given type, with given content, send at given receivergroup and with given reliability</summary>
        public void RaiseEvent(NetworkingEventType type, object content, int[] targetActorNumbers, bool sendReliable)
        {
            RaiseEventOptions eventOptions = new RaiseEventOptions { TargetActors = targetActorNumbers };
            SendOptions sendOptions = sendReliable ? SendOptions.SendReliable : SendOptions.SendUnreliable;
            PhotonNetwork.RaiseEvent((byte)type, content, eventOptions, sendOptions);
        }

        /// <summary>Raises request based on given type and first index of viewIds array. Stores callback for when request has been handled by host</summary>
        public void RaiseRequest(NetworkingRequestType type, int targetViewId, Action<object> callback)
        {
            object[] requestContent = null;
            //try handling request, output requestcontent on succes and exit if it failed
            switch (type)
            {
                case NetworkingRequestType.Lose:
                    requestContent = new object[] { targetViewId, DateTime.UtcNow.TimeOfDay.ToString() };
                    break;
            }

            if (requests.ContainsKey(targetViewId))
            {
                //if a request with the same target viewID is already in waiting, we exit;
                return;
            }

            //add request item with targets viewID as key and callback as value to invoke callback when a decision has been made by the host
            requests.Add(targetViewId, callback);
            //send event to host only
            RaiseEventOptions eventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.MasterClient };
            PhotonNetwork.RaiseEvent((byte)type, requestContent, eventOptions, SendOptions.SendReliable);
        }

        /// <summary>Adds listener to given networking event type</summary>
        public void AddListener(NetworkingEventType type, Action<object> callback)
        {
            if (!events.ContainsKey(type))
            {
                events.Add(type, null);
            }

            events[type] += callback;
        }

        /// <summary>Removes listener from given networking event type</summary>
        public void RemoveListener(NetworkingEventType type, Action<object> callbackToRemove)
        {
            if (events.ContainsKey(type) && events[type] != null)
            {
                events[type] -= callbackToRemove;
            }
        }

        /// <summary>Handles photon event coming in</summary>
        public void HandleEvent(EventData eventData)
        {
            byte code = eventData.Code;
            if (IsNetworkingRequestCode(code))
            {
                if (!PhotonNetwork.IsMasterClient)
                {
                    Debug.LogWarning("Did request to a client other than host :: This is not intended behaviour!");
                    return;
                }

                HandleRequest(eventData);
            }
            else if (!IsPunEvent(code))
            {
                if (IsNetworkingRequestDecision(code))
                {
                    //if this event was a request dicision by the host we check if we have stored a request related to it
                    object[] content = (object[])eventData.CustomData;
                    int targetViewId = (int)content[1];
                    if (requests.ContainsKey(targetViewId))
                    {
                        HandleRequestDecision((NetworkingRequestDecision)code, targetViewId, content[0]);
                    }
                }
                else
                {
                    NetworkingEventType type = (NetworkingEventType)code;
                    //if the event is not a networking request, a pun event or request answer, the networking event can be handled
                    EventCallback(type)?.Invoke(eventData.CustomData);
                }
            }
        }

        /// <summary>Handles given request decision based on type</summary>
        private void HandleRequestDecision(NetworkingRequestDecision type, int targetViewID, object decisionContent)
        {
            if (type != NetworkingRequestDecision.DecisionFailed)
            {
                if (type != NetworkingRequestDecision.LoseDecision)
                {
                    PhotonPlayer p = p = (PhotonPlayer)decisionContent;
                    Player playerDecidedOn = new Player(p.ActorNumber, p.NickName, p.IsLocal, p.IsMasterClient);
                    requests[targetViewID].Invoke(playerDecidedOn);
                }
                else
                {
                    requests[targetViewID].Invoke(decisionContent);
                }
            }
            else
            {
                Debug.LogWarning((string)decisionContent + " content will be null");
                requests[targetViewID].Invoke(null);
            }
            requests.Remove(targetViewID);
        }

        /// <summary>Starts Handling of request raised by client based on request event data</summary>
        private void HandleRequest(EventData requestEventData)
        {
            NetworkingRequestType type = (NetworkingRequestType)requestEventData.Code;
            object[] content = (object[])requestEventData.CustomData;
            NetworkingRequest request;
            string timeStamp = (string)content[1];

            int targetViewId = (int)content[0];

            if (targetViewId != -1 && !NetworkingResources.Find(targetViewId).activeInHierarchy)
            {
                //make sure when request arives, the object the request is about is an active item in the scene
                string errorMessage = "Failed handling request as host for " + type + " request :: gameobject was inactive.";
                object[] errorContent = new object[] { errorMessage, targetViewId };

                Debug.LogWarning(errorMessage);
                RaiseEventOptions eventOptions = new RaiseEventOptions { TargetActors = new int[] { requestEventData.Sender } };
                PhotonNetwork.RaiseEvent((byte)NetworkingRequestDecision.DecisionFailed, errorMessage, eventOptions, SendOptions.SendReliable);
                return;
            }

            //berrypick and itempickup have only one viewID as content and that is of the gameobject the request is about
            request = requestsInProgress.Where(r => r.type == type && r.targetViewId == targetViewId).FirstOrDefault();
            if (request == null)
            {
                //if no request of this type and target is already in progress, add a new one to the list and start handling it
                requestsInProgress.Add(new NetworkingRequest(type, targetViewId, requestEventData.Sender, timeStamp));
                OnHandleRequest(type, targetViewId, OnRequestFinished, GetRequestDelay(type));
            }
            else
            {
                //if a request with the same target and type is already in progress we just add this actors number and timestamp to the request's dictionary
                request.timeStamps.Add(requestEventData.Sender, timeStamp);
            }
        }

        /// <summary>Should be called when a request has been finished. Decides on what decision to make based on type and targetViewID</summary>
        private void OnRequestFinished(NetworkingRequestType type, int targetViewId)
        {
            NetworkingRequest finishedRequest = requestsInProgress.Where(r => r.type == type && r.targetViewId == targetViewId).First();

            switch (type)
            {
                case NetworkingRequestType.Lose:
                    DecideOnEndGameState(finishedRequest, type, targetViewId);
                    break;
            }

            requestsInProgress.Remove(finishedRequest);
        }

        /// <summary>Decides on what decision to make based on the time difference from the requests</summary>
        private void DecideOnEndGameState(NetworkingRequest finishedRequest, NetworkingRequestType type, int targetViewId)
        {
            //Create array of requests ordered by their timestamp
            KeyValuePair<int, string>[] requests = finishedRequest.timeStamps.OrderBy(timeStamp => DateTime.Parse(timeStamp.Value)).ToArray();
            Dictionary<int, int> content = new Dictionary<int, int>();
            //Get the amount of players still alive
            IEnumerable<PhotonPlayer> players = PhotonNetwork.PlayerList.Where(player => (int)player.CustomProperties[PlayerPropertyHandler.PlayerStateKey] == (int)PlayerState.Playing);
            int amountOfPlayersAlive = players.Count();

            if (requests.Length < amountOfPlayersAlive)
            {
                //If the amount of request is smaller than the amount of players alive all players requesting will lose
                for (int i = 0; i < requests.Length; i++)
                {
                    content.Add(requests[i].Key, (int)NetworkingLoseDesicionType.Lost);
                }

                if (requests.Length == amountOfPlayersAlive - 1)
                {
                    //If there is only one player left that didn't make a loss request that player will win
                    Dictionary<int, string> dict = requests.ToDictionary(x => x.Key, x => x.Value);

                    foreach (var player in players)
                    {
                        if (!dict.ContainsKey(player.ActorNumber))
                        {
                            //Raise event to notify the winner cliet it won
                            RaiseEvent(NetworkingEventType.PlayerWinGame, null, new int[] { player.ActorNumber }, true);
                            break;
                        }
                    }
                }
            }
            else { content = GetCloseEndGameStateResults(requests); }

            PhotonNetwork.RaiseEvent((byte)NetworkingRequestDecision.LoseDecision, new object[] { content, targetViewId }, GetEventOptions(finishedRequest), SendOptions.SendReliable);
        }

        /// <summary>Gets target Actors from timeStamp keys and add as target actors in event options</summary>
        private RaiseEventOptions GetEventOptions(NetworkingRequest finishedRequest)
        {
            int[] targetActors = new int[finishedRequest.timeStamps.Count];
            int targetActorCount = 0;
            foreach (var timestamp in finishedRequest.timeStamps) { targetActors[targetActorCount++] = timestamp.Key; }
            return new RaiseEventOptions { TargetActors = targetActors };
        }

        /// <summary>Gets the EndGame state result when it needs to decide if between winning, losing or a draw</summary>
        private Dictionary<int, int> GetCloseEndGameStateResults(KeyValuePair<int, string>[] requests)
        {
            Dictionary<int, int> content = new Dictionary<int, int>();

            NetworkingLoseDesicionType endState = NetworkingLoseDesicionType.Lost;

            for (int i = 0; i < requests.Length; i++)
            {
                //Once it is draw all other request will be draw so no logic needs to be applied
                if (endState != NetworkingLoseDesicionType.Draw)
                {
                    if (i == requests.Length - 1)
                    {
                        //Win if request is the latest and no other requests are draw
                        endState = NetworkingLoseDesicionType.Win;
                    }
                    else if ((DateTime.Parse(requests[i].Value) - DateTime.Parse(requests[requests.Length - 1].Value)).TotalMilliseconds < GameStateManager.Instance.DrawMargin)
                    {
                        //Draw if the time between the the last request and this request is inside of the drawMargin
                        endState = NetworkingLoseDesicionType.Draw;
                    }
                    else { endState = NetworkingLoseDesicionType.Lost; }
                }
                //Add the state with player to the content dictionary
                content.Add(requests[i].Key, (int)endState);
            }

            return content;
        }

        /// <summary>Returns delegate of given networking event type</summary>
        private Action<object> EventCallback(NetworkingEventType type)
        {
            return events.ContainsKey(type) ? events[type] : null;
        }

        private NetworkingRequestDelay GetRequestDelay(NetworkingRequestType requestType)
        {
            switch (requestType)
            {
                case NetworkingRequestType.Lose:
                    return NetworkingRequestDelay.Slow;
            }

            return NetworkingRequestDelay.Fast;
        }

        /// <summary>Nested class for storing request in progress information</summary>
        private class NetworkingRequest
        {
            public readonly NetworkingRequestType type;
            public readonly int targetViewId;
            public readonly Dictionary<int, string> timeStamps;

            public NetworkingRequest(NetworkingRequestType type, int targetViewId, int actorNumber, string timeStamp)
            {
                this.type = type;
                this.targetViewId = targetViewId;
                this.timeStamps = new Dictionary<int, string> { { actorNumber, timeStamp } };
            }
        }
    }
}