﻿using GameStudio.HunterGatherer.GameState;
using Photon.Pun;

namespace GameStudio.HunterGatherer.Networking
{
    /// <summary>Extending networking service it provides additional methods for handling connection outside and inside a room</summary>
    public static class NetworkingServiceExtensions
    {
        /// <summary>Tries connecting the user to the master service and then to a room with given roomname in the default lobby with given username</summary>
        public static void ConnectUserToRoomInDefaultLobby(this NetworkingService service, string username, string roomname)
        {
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(roomname))
            {
                if (PhotonNetwork.IsConnectedAndReady)
                {
                    //if the player is already connected and ready to join a room, join a room with given name
                    service.JoinRoomInDefaultLobbyWithName(roomname);
                }
                else
                {
                    //if the player is not connected yet, connect to master with given username and cache the roomname for joining  a room later
                    service.SetRoomnameToConnectWith(roomname);

                    PhotonNetwork.NickName = username;
                    PhotonNetwork.ConnectUsingSettings();
                }
            }
        }

        /// <summary>Makes the user leave the current room he is in</summary>
        public static void LeaveCurrentRoom(this NetworkingService service)
        {
            if (PhotonNetwork.InRoom && PhotonNetwork.IsConnectedAndReady)
            {
                PhotonNetwork.LeaveRoom();
            }
        }

        /// <summary>Disconnect the user from the master server</summary>
        public static void Disconnect(this NetworkingService service)
        {
            if (PhotonNetwork.IsConnected)
            {
                NetworkingService.UpdatePlayerProperty(PlayerPropertyHandler.PlayerStateKey, (int)PlayerState.Lost);
                PhotonNetwork.Disconnect();
            }
        }
    }
}