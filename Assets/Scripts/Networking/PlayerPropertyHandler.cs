﻿using ExitGames.Client.Photon;
using GameStudio.HunterGatherer.GameState;
using Photon.Pun;
using Photon.Realtime;
using System.Collections.Generic;

namespace GameStudio.HunterGatherer.Networking
{
    /// <summary>Class used for managing networked player properties</summary>
    public class PlayerPropertyHandler
    {
        public Dictionary<Player, Dictionary<string, object>> PlayerProperties { get; } = new Dictionary<Player, Dictionary<string, object>>();

        public const string InGameSceneKey = "InGameScene";
        public const string PlayerStateKey = "PlayerState";

        /// <summary>Clears the player properties dictionary and adds default values for each property</summary>
        public void SetDefaultValues()
        {
            PlayerProperties.Clear();
            UpdateProperty(InGameSceneKey, false);
            UpdateProperty(PlayerStateKey, (int)PlayerState.Loading);
        }

        /// <summary>removes given player's properties from player properties dictionary</summary>
        public void RemovePlayerProperties(Player player)
        {
            if (PlayerProperties.ContainsKey(player))
            {
                PlayerProperties.Remove(player);
            }
        }

        /// <summary>Updates PlayerProperties with given player's new properties</summary>
        public void UpdateCachedProperties(Player player, Hashtable newProps)
        {
            if (PlayerProperties.ContainsKey(player))
            {
                PlayerProperties[player].Merge(newProps);
            }
            else
            {
                Dictionary<string, object> properties = new Dictionary<string, object>();
                foreach (var item in newProps) { properties.Add((string)item.Key, item.Value); }
                PlayerProperties.Add(player, properties);
            }
        }

        /// <summary>Updates property with given key with given value</summary>
        public void UpdateProperty<T>(string key, T value)
        {
            Hashtable table = PhotonNetwork.LocalPlayer.CustomProperties;
            if (table.ContainsKey(key))
            {
                table[key] = value;
            }
            else
            {
                table.Add(key, value);
            }
            PhotonNetwork.SetPlayerCustomProperties(table);
        }

        /// <summary>Returns property of given given based on key</summary>
        public T GetProperty<T>(string key)
        {
            Hashtable table = PhotonNetwork.LocalPlayer.CustomProperties;
            return table.ContainsKey(key) ? (T)table[key] : default;
        }

        /// <summary>Returns a dictionary containing for each actor key, the property value</summary>
        public Dictionary<int, T> GetPropertiesOfPlayersInRoom<T>(string key)
        {
            Dictionary<int, T> properties = new Dictionary<int, T>();
            Hashtable table;
            foreach (var player in PhotonNetwork.CurrentRoom.Players)
            {
                table = player.Value.CustomProperties;
                properties.Add(player.Key, table.ContainsKey(key) ? (T)table[key] : default);
            }
            return properties;
        }
    }
}