﻿using ExitGames.Client.Photon;
using GameStudio.HunterGatherer.GameState;
using GameStudio.HunterGatherer.Networking.Events;
using GameStudio.HunterGatherer.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace GameStudio.HunterGatherer.Networking
{
    using PhotonPlayer = Photon.Realtime.Player;
    using PhotonRoom = Photon.Realtime.Room;
    using IEnumerator = System.Collections.IEnumerator;

    /// <summary>Serving as the main class for networking, it facilitates all network related functionalities, wrapping the photon library</summary>
    public class NetworkingService : MonoBehaviour, IConnectionCallbacks, IMatchmakingCallbacks, IInRoomCallbacks, ILobbyCallbacks, IOnEventCallback
    {
        public static NetworkingService Instance { get; private set; }

        [Header("Settings")]
        [SerializeField]
        private int sendRate = 40;

        [SerializeField]
        private int serializationRate = 20;

        [SerializeField]
        private byte maxPlayersInRoom = 10;

        /// <summary>Returns whether the client is in an offline mode or not</summary>
        public static bool OfflineMode
        {
            get { return PhotonNetwork.OfflineMode; }
        }

        /// <summary>Returns the whether the user is inside a room</summary>
        public static bool InRoom
        {
            get { return PhotonNetwork.InRoom; }
        }

        /// <summary>Returns if this client is the host</summary>
        public static bool IsHost
        {
            get { return PhotonNetwork.IsMasterClient; }
        }

        public static bool IsConnected
        {
            get { return PhotonNetwork.IsConnected; }
        }

        /// <summary>Returns information on the current room the user is in. Will return null if we are not in a room</summary>
        public static Room CurrentRoom
        {
            get
            {
                PhotonRoom room = PhotonNetwork.CurrentRoom;
                return room == null ? null : new Room(room.Name, room.MaxPlayers, room.PlayerCount);
            }
        }

        /// <summary>Your local player's information</summary>
        public static Player LocalPlayer
        {
            get
            {
                PhotonPlayer p = PhotonNetwork.LocalPlayer;
                return new Player(p.ActorNumber, p.NickName, p.IsLocal, p.IsMasterClient);
            }
        }

        /// <summary>Returns the cached player properties of all players in the room (also the ones that have left)</summary>
        public static Dictionary<Player, Dictionary<string, object>> PlayerProperties
        {
            get { return propertyHandler.PlayerProperties; }
        }

        /// <summary>Returns a list of ordered players inside the current room.</summary>
        public static Player[] PlayerList
        {
            get
            {
                PhotonPlayer[] players = PhotonNetwork.PlayerList;
                if (players == null) { return null; }

                Player[] playerList = new Player[players.Length];
                for (int i = 0; i < playerList.Length; i++)
                {
                    playerList[i] = new Player
                    (
                        players[i].ActorNumber,
                        players[i].NickName,
                        players[i].IsLocal,
                        players[i].IsMasterClient
                    );
                }
                return playerList;
            }
        }

        public byte MaxPlayersInRoom
        {
            get { return maxPlayersInRoom; }
        }

        public event Action<bool> Connected;

        public event Action<string> Disconnected;

        public event Action<string> JoinRoomFailed;

        public event Action JoinedRoom;

        public event Action LeftRoom;

        public event Action PlayerJoinedRoom;

        public event Action PlayerLeftRoom;

        public event Action<Player> PlayerPropertiesChanged;

        private static NetworkingEventHandler eventHandler;
        private static PlayerPropertyHandler propertyHandler;
        private static List<IInRoomNetworkingCallbacks> inRoomCallbackTargets = new List<IInRoomNetworkingCallbacks>();

        private NetworkingResources resources;

        private string roomnameConnectingWith;
        private const string offlineRoomName = "OfflineRoom";

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else if (Instance != this)
            {
                GameObject.DestroyImmediate(gameObject);
                return;
            }

            resources = GetComponent<NetworkingResources>();
            eventHandler = new NetworkingEventHandler(HandleRequest);
            propertyHandler = new PlayerPropertyHandler();

            PhotonNetwork.SendRate = this.sendRate;
            PhotonNetwork.SerializationRate = this.serializationRate;
            PhotonNetwork.AddCallbackTarget(this);

            DontDestroyOnLoad(this.gameObject);
        }

        private void OnDestroy()
        {
            if (PhotonNetwork.OfflineMode)
            {
                //handle disconnect in offline mode
                PhotonNetwork.Disconnect();
                return;
            }

#if UNITY_EDITOR
            if (!EditorApplication.isPlayingOrWillChangePlaymode && EditorApplication.isPlaying)
            {
                //make sure we are disconnected when being destroyed
                if (PhotonNetwork.IsConnected)
                {
                    PhotonNetwork.Disconnect();
                }
            }
#endif
        }

        /// <summary>Function to start RequestHandleEnumerator with given type, viewID and callback</summary>
        private void HandleRequest(NetworkingRequestType type, int viewId, Action<NetworkingRequestType, int> onComplete, NetworkingRequestDelay ticks)
        {
            StartCoroutine(RequestHandleEnumerator(type, viewId, onComplete, ticks));
        }

        /// <summary>Enumerator used to wait for a request to be handled, calling onComplete callback when it has finished</summary>
        private IEnumerator RequestHandleEnumerator(NetworkingRequestType type, int viewId, Action<NetworkingRequestType, int> onComplete, NetworkingRequestDelay ticks)
        {
            float seconds = (1.0f / serializationRate) * (int)ticks;
            yield return new WaitForSeconds(seconds);
            onComplete(type, viewId);
        }

        /// <summary>Makes user join or create a room with cached roomname</summary>
        private void JoinRoomInDefaultLobby()
        {
            if (PhotonNetwork.IsConnectedAndReady)
            {
                RoomOptions options = new RoomOptions { MaxPlayers = maxPlayersInRoom };
                PhotonNetwork.JoinOrCreateRoom(roomnameConnectingWith, options, TypedLobby.Default);

                roomnameConnectingWith = null;
            }
        }

        /// <summary>Makes user join or create a room using given roomname</summary>
        public void JoinRoomInDefaultLobbyWithName(string roomname)
        {
            if (PhotonNetwork.IsConnectedAndReady && !string.IsNullOrEmpty(roomname))
            {
                RoomOptions options = new RoomOptions { MaxPlayers = maxPlayersInRoom };
                PhotonNetwork.JoinOrCreateRoom(roomname, options, TypedLobby.Default);
            }
        }

        /// <summary>Makes the user create a room using the offline room name (works only in offline mode)</summary>
        private void JoinOfflineRoom()
        {
            if (PhotonNetwork.OfflineMode)
            {
                PhotonNetwork.CreateRoom(offlineRoomName);
            }
        }

        /// <summary>Sets cached roomname to given roomname</summary>
        public void SetRoomnameToConnectWith(string roomname)
        {
            if (!string.IsNullOrEmpty(roomname))
            {
                this.roomnameConnectingWith = roomname;
            }
        }

        /// <summary>Instantiates and returns a game object with given prefab name at given position with given rotation on all clients</summary>
        public GameObject Instantiate(string prefabName, Vector3 position, Quaternion rotation, bool isSceneObject = false)
        {
            if (isSceneObject)
            {
                if (resources.IsStaticObject(prefabName))
                {
                    //static objects are spawned using the static objects spawn event type.
                    SceneObjectSpawnInfo info = new SceneObjectSpawnInfo(prefabName, position, rotation);
                    eventHandler.RaiseEvent(NetworkingEventType.StaticObjectSpawn, info, ReceiverGroup.All, true);
                    return null;
                }
                else if (!PhotonNetwork.IsMasterClient)
                {
                    //objects spawned as scene object by a non host client will be converted into an event send to the host to spawn it instead
                    SceneObjectSpawnInfo info = new SceneObjectSpawnInfo(prefabName, position, rotation);
                    int[] targets = new int[] { PhotonNetwork.CurrentRoom.MasterClientId };
                    eventHandler.RaiseEvent(NetworkingEventType.SceneObjectSpawn, info, targets, true);
                    return null;
                }

                return PhotonNetwork.InstantiateSceneObject(prefabName, position, rotation);
            }
            else
            {
                if (!resources.IsStaticObject(prefabName))
                {
                    return PhotonNetwork.Instantiate(prefabName, position, rotation);
                }
                else
                {
                    Debug.LogError("Static objects are part of the scene and not owned :: use sceneObject = true instead");
                    return null;
                }
            }
        }

        /// <summary>Destroys given game object on all clients</summary>
        public void Destroy(NetworkedObject obj)
        {
            if (resources.IsStaticObject(obj.name))
            {
                eventHandler.RaiseEvent(NetworkingEventType.StaticObjectDestroy, obj.ViewId, ReceiverGroup.All, true);
            }
            else
            {
                PhotonNetwork.Destroy(obj.gameObject);
            }
        }

        /// <summary>Raises event of given type, with given content, send at given eventreceivers.</summary>
        public static void RaiseEvent(NetworkingEventType type, object content, EventReceivers receivers, bool sendReliable = true)
        {
            eventHandler.RaiseEvent(type, content, (ReceiverGroup)receivers, sendReliable);
        }

        /// <summary>Raises event of given type, with given content, send with given eventOptions.</summary>
        public static void RaiseEvent(NetworkingEventType type, object content, int[] targetActorNumbers, bool sendReliable = true)
        {
            eventHandler.RaiseEvent(type, content, targetActorNumbers, sendReliable);
        }

        /// <summary>
        /// Raises request to be send to the host for object with given targetViewId
        /// </summary>
        /// <param name="type">Type of networking request you are sending</param>
        /// <param name="callback">Callback for when the request has finished. !important: can be null if it failed</param>
        public static void RaiseRequest(NetworkingRequestType type, Action<object> callback, int targetViewId)
        {
            eventHandler.RaiseRequest(type, targetViewId, callback);
        }

        /// <summary>Add given target to in room callbacks list </summary>
        public static void AddInRoomCallbacksTarget(IInRoomNetworkingCallbacks target)
        {
            inRoomCallbackTargets.Add(target);
        }

        /// <summary>Removes given target from in room callbacks list </summary>
        public static void RemoveInRoomCallbacksTarget(IInRoomNetworkingCallbacks target)
        {
            inRoomCallbackTargets.Remove(target);
        }

        /// <summary>Adds a listener to be called when the given type of networking event has been fired</summary>
        public static void AddNetworkingEventListener(NetworkingEventType type, Action<object> callback)
        {
            if (callback != null)
            {
                eventHandler.AddListener(type, callback);
            }
        }

        /// <summary>Removes given listener of given networking event type</summary>
        public static void RemoveNetworkingEventListener(NetworkingEventType type, Action<object> callback)
        {
            if (callback != null)
            {
                if (eventHandler != null)
                    eventHandler.RemoveListener(type, callback);
                else
                    Debug.LogWarning("Cannot remove listener because eventhandler is null");
            }
        }

        /// <summary>
        /// Updates your player property and synchronizes it with other players if you are in a room
        /// </summary>
        /// <typeparam name="T">Type of value (this needs to be a serializable type)</typeparam>
        /// <param name="key">property key</param>
        /// <param name="value">new value you want your property to have</param>
        public static void UpdatePlayerProperty<T>(string key, T value)
        {
            if (!string.IsNullOrEmpty(key) && PhotonNetwork.InRoom)
            {
                propertyHandler.UpdateProperty(key, value);
            }
        }

        /// <summary>
        /// Returns your player property based on given key. Make sure the type matches the key
        /// </summary>
        /// <typeparam name="T">Type of value the property has</typeparam>
        /// <param name="key">Key to search for</param>
        /// <returns></returns>
        public static T GetPlayerProperty<T>(string key)
        {
            return !string.IsNullOrEmpty(key) ? propertyHandler.GetProperty<T>(key) : default;
        }

        /// <summary>
        /// Returns a dictionary containing for each player their properties value based on given key.
        /// </summary>
        /// <typeparam name="T">Type of value the property has</typeparam>
        /// <param name="key">Key to search for</param>
        /// <returns></returns>
        public static Dictionary<int, T> GetPropertiesOfPlayersInRoom<T>(string key)
        {
            return !string.IsNullOrEmpty(key) ? propertyHandler.GetPropertiesOfPlayersInRoom<T>(key) : default;
        }

        /// <summary>Sets offline mode to true (works only if the player is not connected yet)</summary>
        public static void SetOffline()
        {
            if (!PhotonNetwork.IsConnected && !PhotonNetwork.OfflineMode)
            {
                PhotonNetwork.OfflineMode = true;
                propertyHandler.SetDefaultValues();
            }
        }

        /// <summary>Returns a player currently in this room based on actor number</summary>
        public static Player GetPlayerInRoom(int actorNr)
        {
            var players = PhotonNetwork.CurrentRoom.Players;
            if (players.ContainsKey(actorNr))
            {
                PhotonPlayer p = players[actorNr];
                return new Player(p.ActorNumber, p.NickName, p.IsLocal, p.IsMasterClient);
            }
            else
            {
                return null;
            }
        }

        public void OnConnected()
        {
        }

        /// <summary>Decides whether to join an offline room or to join an online room in a default lobby</summary>
        public void OnConnectedToMaster()
        {
            if (PhotonNetwork.OfflineMode)
            {
                /*
                 OnConnectedToMaster will be called when offline mode is set to true so the user can connect to an offline room
                 this means that if we got connected to master in offline mode we join the offline room and give a callback with online
                 set to false
                 */
                Connected?.Invoke(false);
                JoinOfflineRoom();
            }
            else
            {
                //if we got connected to master in online mode we connect to the default lobby if the cached roomname is not null or empty
                if (!string.IsNullOrEmpty(roomnameConnectingWith))
                {
                    Connected?.Invoke(true);
                    JoinRoomInDefaultLobby();
                }
            }
        }

        public void OnCreatedRoom()
        {
        }

        public void OnCreateRoomFailed(short returnCode, string message)
        {
        }

        public void OnCustomAuthenticationFailed(string debugMessage)
        {
        }

        public void OnCustomAuthenticationResponse(Dictionary<string, object> data)
        {
        }

        /// <summary>Fires the disconnected event to let the client know it got disconnected from the server</summary>
        public void OnDisconnected(DisconnectCause cause)
        {
#if UNITY_EDITOR
            //exit if we get the callback when exiting playmode
            if (!EditorApplication.isPlayingOrWillChangePlaymode && EditorApplication.isPlaying)
            {
                return;
            }
            else if (!EditorApplication.isPlaying)
            {
                return;
            }
#endif

            propertyHandler.PlayerProperties.Clear();
            eventHandler.Clear();
            roomnameConnectingWith = null;

            if (!SceneManager.Instance.InMainMenu)
            {
                SceneManager.Instance.LoadMainMenuScene();
            }

            Disconnected?.Invoke(cause.ToString());
        }

        public void OnFriendListUpdate(List<FriendInfo> friendList)
        {
        }

        /// <summary>Fires the joined event to let the client know it joined the room</summary>
        public void OnJoinedRoom()
        {
            propertyHandler.SetDefaultValues();

            foreach (var player in PhotonNetwork.CurrentRoom.Players)
            {
                PhotonPlayer photonPlayer = player.Value;
                Player p = new Player(photonPlayer.ActorNumber, photonPlayer.NickName, photonPlayer.IsLocal, photonPlayer.IsMasterClient);
                propertyHandler.UpdateCachedProperties(p, photonPlayer.CustomProperties);
            }

            JoinedRoom?.Invoke();
        }

        public void OnJoinRandomFailed(short returnCode, string message)
        {
        }

        public void OnJoinRoomFailed(short returnCode, string message)
        {
            JoinRoomFailed?.Invoke(message);
        }

        /// <summary>Fires the leftroom event to let the client know it left the room</summary>
        public void OnLeftRoom()
        {
#if UNITY_EDITOR
            //exit if we get the callback when exiting playmode
            if (!EditorApplication.isPlayingOrWillChangePlaymode && EditorApplication.isPlaying)
            {
                return;
            }
            else if (!EditorApplication.isPlaying)
            {
                return;
            }
#endif
            propertyHandler.PlayerProperties.Clear();
            eventHandler.Clear();

            LeftRoom?.Invoke();
        }

        /// <summary>Updates callback IInRoomCallbackTargets with player that is the new host</summary>
        public void OnMasterClientSwitched(PhotonPlayer newMasterClient)
        {
            foreach (IInRoomNetworkingCallbacks target in inRoomCallbackTargets)
            {
                target.OnHostMigrated(new Player
                (
                    newMasterClient.ActorNumber,
                    newMasterClient.NickName,
                    newMasterClient.IsLocal,
                    newMasterClient.IsMasterClient
                ));
            }
        }

        /// <summary>Updates callback IInRoomCallbackTargets with player that entered the room</summary>
        public void OnPlayerEnteredRoom(PhotonPlayer newPlayer)
        {
            //update cached properties with new players properties
            Player p = new Player(newPlayer.ActorNumber, newPlayer.NickName, newPlayer.IsLocal, newPlayer.IsMasterClient);
            propertyHandler.UpdateCachedProperties(p, newPlayer.CustomProperties);

            foreach (IInRoomNetworkingCallbacks target in inRoomCallbackTargets)
            {
                target.OnPlayerEnteredRoom(new Player
                (
                    newPlayer.ActorNumber,
                    newPlayer.NickName,
                    newPlayer.IsLocal,
                    newPlayer.IsMasterClient
                ));
            }

            PlayerJoinedRoom?.Invoke();
        }

        /// <summary>Updates callback IInRoomCallbackTargets with player that left</summary>
        public void OnPlayerLeftRoom(PhotonPlayer otherPlayer)
        {
            Player player = new Player(otherPlayer.ActorNumber, otherPlayer.NickName, otherPlayer.IsLocal, otherPlayer.IsMasterClient);

            foreach (IInRoomNetworkingCallbacks target in inRoomCallbackTargets)
            {
                target.OnPlayerLeftRoom(player);
            }

            if (SceneManager.Instance.InMainMenu)
            {
                propertyHandler.RemovePlayerProperties(player);
            }
            else if (SceneManager.Instance.InGameScene)
            {
                PlayerState leavingPlayerState = (PlayerState)otherPlayer.CustomProperties[PlayerPropertyHandler.PlayerStateKey];

                if (leavingPlayerState == PlayerState.Playing || leavingPlayerState == PlayerState.Loading)
                {
                    leavingPlayerState = PlayerState.Lost;
                }

                propertyHandler.UpdateCachedProperties(player, new Hashtable() { { PlayerPropertyHandler.PlayerStateKey, (int)leavingPlayerState } });
                PlayerPropertiesChanged?.Invoke(player);
            }

            PlayerLeftRoom?.Invoke();
        }

        /// <summary>Updates properties stored by the property handler</summary>
        public void OnPlayerPropertiesUpdate(PhotonPlayer targetPlayer, Hashtable changedProps)
        {
            Player p = new Player(targetPlayer.ActorNumber, targetPlayer.NickName, targetPlayer.IsLocal, targetPlayer.IsMasterClient);
            propertyHandler.UpdateCachedProperties(p, changedProps);

            PlayerPropertiesChanged?.Invoke(p);
        }

        public void OnRegionListReceived(RegionHandler regionHandler)
        {
        }

        public void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
        {
        }

        /// <summary>Sends the end lobby rpc if the user is the masterclient and in a room</summary>
        public static void SendEndLobby(string scene)
        {
            if (PhotonNetwork.InRoom && PhotonNetwork.IsMasterClient)
            {
                eventHandler.RaiseEvent(NetworkingEventType.LobbyEnd, scene, ReceiverGroup.All, true);
                PhotonNetwork.CurrentRoom.IsOpen = false;
            }
        }

        public void OnJoinedLobby()
        {
        }

        public void OnLeftLobby()
        {
        }

        public void OnRoomListUpdate(List<RoomInfo> roomList)
        {
        }

        public void OnLobbyStatisticsUpdate(List<TypedLobbyInfo> lobbyStatistics)
        {
        }

        /// <summary>Handles event callbacks by firing an event based on the type of photon event that was fired</summary>
        public void OnEvent(EventData photonEvent)
        {
            eventHandler.HandleEvent(photonEvent);
        }
    }
}