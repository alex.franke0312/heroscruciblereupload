using GameStudio.HunterGatherer.CustomEvents;
using GameStudio.HunterGatherer.Divisions;
using GameStudio.HunterGatherer.GameState;
using GameStudio.HunterGatherer.SceneManagement;
using GameStudio.HunterGatherer.UI;
using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameStudio.HunterGatherer.Networking
{
    using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;

    /// <summary>The NetworkingPlayerManager manages player units and player properties among other things</summary>
    public class NetworkingPlayerManager : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField]
        private GameObject unitSpawnPoints;

        [SerializeField]
        private string nameOfDivisionPrefab = "Division";

        [SerializeField]
        private uint startingDivisionCount = 2;

        [SerializeField]
        private uint startingUnitCounterPerDivision = 12;

        [SerializeField]
        private int maxDivisions = 7;

        [SerializeField]
        private float spawnDistance = 5f;

        [SerializeField, Tooltip("Set to true if you want all players to spawn at the same spawnpoint")]
        private bool collectiveSpawn = false;

        [SerializeField, Tooltip("Textures usable for identification, should be length of max players in room")]
        private Texture[] unitTexturePalette = null;

        [SerializeField, Tooltip("Hero Flag textures usable for identification, should be length of max players in room")]
        private Texture[] heroFlagTexturePalette = null;
        [SerializeField, Tooltip("Archers Flag textures usable for identification, should be length of max players in room")]
        private Texture[] archersFlagTexturePalette = null;
        [SerializeField, Tooltip("Pikemen Hero Flag textures usable for identification, should be length of max players in room")]
        private Texture[] pikemenFlagTexturePalette = null;
        [SerializeField, Tooltip("Swordsmen Hero Flag textures usable for identification, should be length of max players in room")]
        private Texture[] swordsmenFlagTexturePalette = null;

        [SerializeField]
        private Texture defaultUnitTexture = null;

        [SerializeField]
        private Texture defaultHeroFlagTexture = null;
        [SerializeField]
        private Texture defaultArchersFlagTexture = null;
        [SerializeField]
        private Texture defaultPikemenFlagTexture = null;
        [SerializeField]
        private Texture defaultSwordsmenFlagTexture = null;

        public static NetworkingPlayerManager Instance { get; private set; }

        public Texture DefaultUnitTexture { get { return defaultUnitTexture; } }
        public Texture DefaultHeroFlagTexture { get { return defaultHeroFlagTexture; } }
        public Texture DefaultArchersFlagTexture { get { return defaultArchersFlagTexture; } }
        public Texture DefaultPikemenFlagTexture { get { return defaultPikemenFlagTexture; } }
        public Texture DefaultSwordsmenFlagTexture { get { return defaultSwordsmenFlagTexture; } }

        public bool IsSpectating { get; private set; }
        public Dictionary<int, Texture> PlayerTextures { get; } = new Dictionary<int, Texture>();
        public Dictionary<int, Texture> HeroFlagTextures { get; } = new Dictionary<int, Texture>();
        public Dictionary<int, Texture> ArchersFlagTextures { get; } = new Dictionary<int, Texture>();
        public Dictionary<int, Texture> PikemenFlagTextures { get; } = new Dictionary<int, Texture>();
        public Dictionary<int, Texture> SwordsmenFlagTextures { get; } = new Dictionary<int, Texture>();
        public List<NetworkedMovingObject> PlayerDivisions { get; } = new List<NetworkedMovingObject>(); //TODO: decide if it's necessary to have this list. Anwser: Yes :)
        public NetworkedMovingObject HeroDivision { get; private set; }
        public UnityEventDivision OnDivisionCreated { get; } = new UnityEventDivision();
        public UnityEvent OnStartSpectating { get; } = new UnityEvent();
        public int MaxDivisions => maxDivisions;
        public uint StartingDivisionCount { get { return startingDivisionCount; } }

        private PhotonView photonView;
        private int playerNumber;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }

            photonView = this.GetComponent<PhotonView>();
            SetPlayerTextures();
            SetPlayerFlagTextures();
        }

        private void Start()
        {
            UnitySceneManager.sceneUnloaded += OnSceneUnloaded;
            SceneManager.Instance.OnPlayersLoaded += AllPlayersLoaded;
        }

        private void OnDestroy()
        {
            UnitySceneManager.sceneUnloaded -= OnSceneUnloaded;
            SceneManager.Instance.OnPlayersLoaded -= AllPlayersLoaded;
        }

        /// <summary>Returns texture based on player number</summary>
        public Texture GetPlayerTexture(int playerNumber)
        {
            return playerNumber != -1 ? PlayerTextures[playerNumber + 1] : defaultUnitTexture;
        }

        public Texture GetPlayerFlagTexture(int actorNumber, DivisionType divisionType)
        {
            if (divisionType == DivisionType.Archers)
            {
                return actorNumber != -1 ? ArchersFlagTextures[actorNumber] : defaultArchersFlagTexture;
            }
            else if (divisionType == DivisionType.Pikemen)
            {
                return actorNumber != -1 ? PikemenFlagTextures[actorNumber] : defaultPikemenFlagTexture;
            }
            else if (divisionType == DivisionType.Swordsmen)
            {
                return actorNumber != -1 ? SwordsmenFlagTextures[actorNumber] : defaultSwordsmenFlagTexture;
            }
            else
            {
                return actorNumber != -1 ? HeroFlagTextures[actorNumber] : defaultHeroFlagTexture;
            }
        }

        /// <summary>Creates a new division at given position with given rotation</summary>
        public Division SpawnDivision(Vector3 position, Quaternion rotation, DivisionType type = DivisionType.Swordsmen)
        {
            if (PlayerDivisions.Count >= maxDivisions)
            {
                Debug.LogWarning($"Max divisions reached ({maxDivisions})");
                return null;
            }
            Division division = NetworkingService.Instance.Instantiate(nameOfDivisionPrefab, position, rotation).GetComponent<Division>();
            division.RaiseSetType(type);
            division.SpawnUnit();
            division.OnDisableDivision.AddListener(RemoveDivision);
            PlayerDivisions.Add(division.GetComponent<NetworkedMovingObject>());
            OnDivisionCreated.Invoke(division);
            return division;
        }

        /// <summary>Creates a new hero division at given position with given rotation</summary>
        public void SpawnHero(Vector3 position, Quaternion rotation)
        {
            Division division = NetworkingService.Instance.Instantiate(nameOfDivisionPrefab, position, rotation).GetComponent<Division>();
            division.RaiseSetType(DivisionType.Hero);
            division.SpawnUnit();
            division.OnDisableDivision.AddListener((_) => RequestLoss());
            division.OnDisableDivision.AddListener(RemoveDivision);
            division.OnDisableDivision.AddListener((_) => DestroyAllUnits());
            NetworkedMovingObject divisionNetworkedObject = division.GetComponent<NetworkedMovingObject>();
            PlayerDivisions.Add(divisionNetworkedObject);
            HeroDivision = divisionNetworkedObject;
            OnDivisionCreated.Invoke(division);
        }

        /// <summary>Handle what needs to happen when the given division is disabled</summary>
        private void RemoveDivision(Division division)
        {
            division.OnDisableDivision.RemoveListener(RemoveDivision);
            PlayerDivisions.Remove(division.GetComponent<NetworkedMovingObject>());
        }

        /// <summary>Raises a request loss event</summary>
        private void RequestLoss()
        {
            GameStateManager.Instance.RequestLoss();
        }

        /// <summary> Destroys all units from the player </summary>
        private void DestroyAllUnits()
        {
            for (int i = PlayerDivisions.Count - 1; i >= 0; i--)
            {
                Division division = PlayerDivisions[i].GetComponent<Division>();

                for (int j = division.UnitCount - 1; j >= 0; j--)
                {
                    NetworkingService.Instance.Destroy(division.Units[j].NetworkedMovingObject);
                }
                NetworkingService.Instance.Destroy(PlayerDivisions[i]);
            }
        }

        /// <summary>Sets up spectator mode by finding our which player attack our units last, if no interaction with other players was done a random player is choosen to spectate</summary>
        public void Spectate()
        {
            IsSpectating = true;
            OnStartSpectating.Invoke();
        }

        /// <summary>Sets Texture for each player based on palette</summary>
        private void SetPlayerTextures()
        {
            if (unitTexturePalette.Length < NetworkingService.Instance.MaxPlayersInRoom)
            {
                Debug.LogWarning("unit texture palette is too small to assign textures for maximum amount of players :: increase it");
            }

            for (int i = 0; i < unitTexturePalette.Length; i++)
            {
                PlayerTextures.Add(i + 1, unitTexturePalette[i]);
            }
        }

        private void SetPlayerFlagTextures()
        {
            if (heroFlagTexturePalette.Length < NetworkingService.Instance.MaxPlayersInRoom ||
                archersFlagTexturePalette.Length < NetworkingService.Instance.MaxPlayersInRoom ||
                pikemenFlagTexturePalette.Length < NetworkingService.Instance.MaxPlayersInRoom ||
                swordsmenFlagTexturePalette.Length < NetworkingService.Instance.MaxPlayersInRoom)
            {
                Debug.LogWarning("unit flag texture palette is too small to assign textures for maximum amount of players :: increase it");
            }

            for (int i = 0; i < heroFlagTexturePalette.Length; i++)
            {
                HeroFlagTextures.Add(i, heroFlagTexturePalette[i]);
                ArchersFlagTextures.Add(i, archersFlagTexturePalette[i]);
                PikemenFlagTextures.Add(i, pikemenFlagTexturePalette[i]);
                SwordsmenFlagTextures.Add(i, swordsmenFlagTexturePalette[i]);
            }
        }

        /// <summary>Called when all players have loaded the game scene, spawns units and resources</summary>
        private void AllPlayersLoaded()
        {
            unitSpawnPoints = GameObject.Find("UnitSpawnPoints");

            //get number in room
            int numInRoom = 0;
            foreach (Player p in NetworkingService.PlayerList)
            {
                numInRoom++;
                if (p.IsLocal) { break; }
            }

            playerNumber = numInRoom;

            //Only the host should randomize locations..
            if (PhotonNetwork.IsMasterClient)
            {
                ShuffleChildren shuffler = unitSpawnPoints.GetComponent<ShuffleChildren>();

                shuffler.ShuffleTransform();

                //And then send those positions to the other players.
                photonView.RPC("SendLocations", RpcTarget.All, shuffler.children as Vector3[]);
            }
        }

        /// <summary>
        ///  Method that sends the host's randomized spawn locations to all locations,
        ///  sets them in the right location,
        ///  and then calls SetupInitialUnitSpawn with the new values.
        /// </summary>
        /// <param name="array"></param>
        [PunRPC]
        void SendLocations(Vector3[] array)
        {
            Vector3[] snapshot = new Vector3[array.Length];
            //Fill array with snapshots of positions which don't update when swapping spawnpoints
            for (int i = 0; i < array.Length; i++)
            {
                snapshot[i] = array[i];
            }

            //Swap positions of spawnpoints with the new positions
            for (int i = 0; i < array.Length; i++)
            {
                unitSpawnPoints.transform.GetChild(i).position = snapshot[i];
            }

            SetupInitialUnitSpawn(GetUnitSpawnPoint(playerNumber));
        }
        /// <summary>Returns spawnpoint for this player based on given number in room and collective spawn flag</summary>
        private Vector3 GetUnitSpawnPoint(int numInRoom)
        {
            GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("UnitSpawnPoint");
            string name;
            int num;
            if (!collectiveSpawn)
            {
                //search for spawnpoint for this player if collective spawn is not enabled
                foreach (GameObject go in spawnPoints)
                {
                    name = go.name;
                    num = int.Parse(name.Substring(name.IndexOf(' ') + 1));
                    if (num == numInRoom)
                    {
                        return go.transform.position;
                    }
                }
            }
            else
            {
                //spawn every player on unit spawn point 1
                for (int i = 0; i < spawnPoints.Length; i++)
                {
                    name = spawnPoints[i].name;
                    num = int.Parse(name.Substring(name.IndexOf(' ') + 1));
                    if (num == 1)
                    {
                        return spawnPoints[i].transform.position;
                    }
                }
            }
            return Vector3.zero;
        }

        /// <summary>Should be called when a game scene has been unloaded to reset any values related to in game scene state</summary>
        private void OnSceneUnloaded(UnityEngine.SceneManagement.Scene scene)
        {
            if (SceneManager.Instance.IsGameScene(scene))
            {
                NetworkingService.UpdatePlayerProperty(PlayerPropertyHandler.InGameSceneKey, false);
                IsSpectating = false;
            }
        }

        /// <summary>Spawns units for this client at one of the unit spawn points on the map</summary>
        private void SetupInitialUnitSpawn(Vector3 atPosition)
        {
            SetCameraPos(atPosition);

            SpawnHero(GetRandomPositionAroundPoint(atPosition), Quaternion.identity);

            for (int i = 0; i < startingDivisionCount; i++)
            {
                Division division = SpawnDivision(GetRandomPositionAroundPoint(atPosition), Quaternion.identity);

                for (int j = 0; j < startingUnitCounterPerDivision - 1; j++)
                {
                    division.SpawnUnit();
                }
            }

            LoadingScreen.Instance.Stop(true);
        }

        /// <summary> Set camera Pos to player spawn point </summary>
        private void SetCameraPos(Vector3 pos)
        {
            Camera.main.transform.parent.transform.position = pos;
        }

        /// <summary>Return random Vector3 in a circle around the given point</summary>
        private Vector3 GetRandomPositionAroundPoint(Vector3 point)
        {
            Vector2 randomOffset = UnityEngine.Random.insideUnitCircle * spawnDistance;
            return point + new Vector3(randomOffset.x, 0, randomOffset.y);
        }
    }
}