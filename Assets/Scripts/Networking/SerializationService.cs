﻿using ExitGames.Client.Photon;
using GameStudio.HunterGatherer.Networking.Events;
using UnityEngine;

namespace GameStudio.HunterGatherer.Networking
{
    /// <summary>Used by the networking event handler it handles serialization of custom types</summary>
    public class SerializationService
    {
        private const char CodeSceneObjectInfo = 'O';
        private const char CodeDivisionSetTypeInfo = 'D';
        private const char CodeUnitSetDivisionInfo = 's';
        private const char CodeUnitHitInfo = 'h';
        private const char CodeUnitFireProjectileInfo = 'p';
        private const char CodeUnitMoveTarget = 'm';
        private const char CodeManPowerResourceAddedInfo = 'l';
        private const char CodeUnitSetMovementSpeed = 'M';
        private const char CodeGodSetInfo = 'g';
        private const char CodeSyncDivisionData = 'A';
        private const char CodeSyncHealDataInfo = 'H';

        /// <summary>Creates instance of Serialization Service registering custom types</summary>
        public SerializationService()
        {
            RegisterSceneObjectSpawnInfo();
            RegisterDivisionSetTypeInfo();
            RegisterUnitSetDivisionInfo();
            RegisterUnitHitInfo();
            RegisterUnitFireProjectileInfo();
            RegisterDivisionMoveTargetInfo();
            RegisterManPowerResourceAddedInfo();
            RegisterUnitSetMovementSpeedInfo();
            RegisterGodSetInfo();
            RegisterSyncDivisionData();
            RegisterHealDataInfo();
        }

        /// <summary>Registers the scene object spawn info struct to photon</summary>
        private void RegisterSceneObjectSpawnInfo()
        {
            bool registered = PhotonPeer.RegisterType(
                typeof(SceneObjectSpawnInfo),
                (byte)CodeSceneObjectInfo,
                SceneObjectSpawnInfo.Serialize,
                SceneObjectSpawnInfo.Deserialize
                );

            if (!registered)
            {
                Debug.LogWarning("Failed registering scene object spawn  Info");
            }
        }

        /// <summary>Registers the divisionSetTypeInfo struct to photon</summary>
        private void RegisterDivisionSetTypeInfo()
        {
            bool registered = PhotonPeer.RegisterType(
                typeof(DivisionSetTypeInfo),
                (byte)CodeDivisionSetTypeInfo,
                DivisionSetTypeInfo.Serialize,
                DivisionSetTypeInfo.Deserialize);

            if (!registered)
            {
                Debug.LogWarning("failed registering divisionSetType info");
            }
        }

        /// <summary>Registers the unitSetDivisionInfo struct to photon</summary>
        private void RegisterUnitSetDivisionInfo()
        {
            bool registered = PhotonPeer.RegisterType(
                typeof(UnitSetDivisionInfo),
                (byte)CodeUnitSetDivisionInfo,
                UnitSetDivisionInfo.Serialize,
                UnitSetDivisionInfo.Deserialize);

            if (!registered)
            {
                Debug.LogWarning("failed registering unitSetDivision info");
            }
        }

        /// <summary>Registers the unitHitInfo struct to photon</summary>
        private void RegisterUnitHitInfo()
        {
            bool registered = PhotonPeer.RegisterType(
                typeof(UnitHitInfo),
                (byte)CodeUnitHitInfo,
                UnitHitInfo.Serialize,
                UnitHitInfo.Deserialize);

            if (!registered)
            {
                Debug.LogWarning("failed registering unitHit info");
            }
        }

        /// <summary>Registers the unitFireProjectileInfo struct to photon</summary>
        private void RegisterUnitFireProjectileInfo()
        {
            bool registered = PhotonPeer.RegisterType(
                typeof(UnitFireProjectileInfo),
                (byte)CodeUnitFireProjectileInfo,
                UnitFireProjectileInfo.Serialize,
                UnitFireProjectileInfo.Deserialize);

            if (!registered)
            {
                Debug.LogWarning("failed registering unitFireProjectile info");
            }
        }

        /// <summary>Registers the MoveTargetInfo struct to photon</summary>
        private void RegisterDivisionMoveTargetInfo()
        {
            bool registered = PhotonPeer.RegisterType(
                typeof(DivisionSetMoveTargetInfo),
                (byte)CodeUnitMoveTarget,
                DivisionSetMoveTargetInfo.Serialize,
                DivisionSetMoveTargetInfo.Deserialize);

            if (!registered)
            {
                Debug.LogWarning("failed registering MoveTarget Info");
            }
        }

        /// <summary>Registers the ManPowerResourceAddedInfo struct to photon</summary>
        private void RegisterManPowerResourceAddedInfo()
        {
            bool registered = PhotonPeer.RegisterType(
                typeof(ManPowerResourceAddedInfo),
                (byte)CodeManPowerResourceAddedInfo,
                ManPowerResourceAddedInfo.Serialize,
                ManPowerResourceAddedInfo.Deserialize);

            if (!registered)
            {
                Debug.LogWarning("failed registering ManPowerResourceAdded Info");
            }
        }

        /// <summary>Registers the MoveTargetInfo struct to photon</summary>
        private void RegisterUnitSetMovementSpeedInfo()
        {
            bool registered = PhotonPeer.RegisterType(
                typeof(UnitSetMovementSpeedInfo),
                (byte)CodeUnitSetMovementSpeed,
                UnitSetMovementSpeedInfo.Serialize,
                UnitSetMovementSpeedInfo.Deserialize);

            if (!registered)
            {
                Debug.LogWarning("failed registering MoveTarget Info");
            }
        }

        /// <summary>Registers the GodSetInfo struct to photon</summary>
        private void RegisterGodSetInfo()
        {
            bool registered = PhotonPeer.RegisterType(
                typeof(GodSetInfo),
                (byte)CodeGodSetInfo,
                GodSetInfo.Serialize,
                GodSetInfo.Deserialize);

            if (!registered)
            {
                Debug.LogWarning("failed registering GodSet Info");
            }
        }

        private void RegisterSyncDivisionData()
        {
            bool registered = PhotonPeer.RegisterType(
                typeof(SyncDivisionDataInfo),
                (byte)CodeSyncDivisionData,
                SyncDivisionDataInfo.Serialize,
                SyncDivisionDataInfo.Deserialize
            );

            if (!registered)
            {
                Debug.LogWarning("failed registering SyncDivisionData Info");
            }
        }
        private void RegisterHealDataInfo()
        {
            bool registered = PhotonPeer.RegisterType(
                typeof(SyncUnitHealthInfo),
                (byte)CodeSyncHealDataInfo,
                SyncUnitHealthInfo.Serialize,
                SyncUnitHealthInfo.Deserialize
            );

            if (!registered)
            {
                Debug.LogWarning("failed registering SyncUnitHeal Info");
            }
        }
    }
}