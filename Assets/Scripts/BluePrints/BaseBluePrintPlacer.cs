﻿using GameStudio.HunterGatherer.BluePrints;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameStudio.HunterGatherer.Structures;
using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.Selection;

namespace GameStudio.HunterGatherer.BluePrints
{
    public class BaseBluePrintPlacer : BluePrintPlacer
    {
        protected override void ConfirmBluePrintPosition()
        {
            base.ConfirmBluePrintPosition();

            bluePrint = NetworkingService.Instance.Instantiate(nameOfHighLightedPrefab, hit.point
                                                  , Quaternion.identity).GetComponent<NetworkedMovingObject>();

            bluePrint.GetComponent<BaseBluePrint>().StartCheckingHeroDistance();
            heroDivision.PlaceBaseOrder(bluePrint.transform.position);
        }
    }
}
