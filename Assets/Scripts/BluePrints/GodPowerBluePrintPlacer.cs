﻿using System.Collections;
using GameStudio.HunterGatherer.GodFavor;
using UnityEngine;
using GameStudio.HunterGatherer.Networking;
using GameStudio.HunterGatherer.Selection;
using GameStudio.HunterGatherer.GodFavor.UI;
namespace GameStudio.HunterGatherer.BluePrints
{
    public class GodPowerBluePrintPlacer : BluePrintPlacer
    {
        [Header("General values")]
        private GameObject godPower;
        private Vector3 visualiserPosition;
        private GodPowerManager.Gods god;
        private LayerMask layerMaskGodShield;

        private void Start()
        {
            layerMaskGodShield = LayerMask.GetMask("GodShield");
        }

        public override void ActivateBluePrintPositioning()
        {
            if (!GodPowerManager.activeManager.IsPlacingGodPower)
            {
                god = GodFavorUI.CurrentGod;
                switch (god)
                {
                    case GodPowerManager.Gods.Zeus:
                        highlightPrefab = GodPowerManager.activeManager.GodPowerZeusHighLight;
                        break;
                    case GodPowerManager.Gods.Ares:
                        break;
                    case GodPowerManager.Gods.Athena:
                        highlightPrefab = GodPowerManager.activeManager.GodPowerAthenaHighlight;
                        break;
                }

                base.ActivateBluePrintPositioning();
                GodPowerManager.activeManager.IsPlacingGodPower = true;
            }
        }

        private void Update()
        {
            CancelPlacement();

            if (god == GodPowerManager.Gods.Athena && godPower != null)
            {
                // This can be coded in a cleaner way (observer patern?)
                if(!godPower.activeSelf)
                {
                    godPower = null;
                    return;
                }

                godPower.transform.position = transform.position + Vector3.up * GodPowerManager.activeManager.ShieldDistance;
            }
        }

        /// <summary>Determine the position of the blueprint locally</summary>
        public override void HandleBluePrintPosition()
        {
            switch (god)
            {
                case GodPowerManager.Gods.Zeus:
                    base.HandleBluePrintPosition();
                    break;
                case GodPowerManager.Gods.Ares:
                    base.HandleBluePrintPosition();
                    break;
                case GodPowerManager.Gods.Athena:
                    highlight.transform.position = transform.position + Vector3.up * GodPowerManager.activeManager.ShieldDistance;
                    if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.G) && GodPowerManager.activeManager.IsShowingPowerRange)
                    {
                        ConfirmBluePrintPosition();
                        GodPowerManager.activeManager.IsShowingPowerRange = false;
                        GodPowerManager.activeManager.IsPlacingGodPower = true;
                    }
                    break;
            }
        }

        /// <summary> Cancel the blue print placement with right mouse click</summary>
        private void CancelPlacement()
        {
            if (Input.GetMouseButtonDown(1))
            {
                SelectionManager.Instance.State = SelectionState.SelectAndInteract;
                DestroyBlueBrint();
                GodPowerManager.activeManager.IsShowingPowerRange = false;
            }
        }

        /// <summary> Destroy method that gets added as listener when the base button should deactivate </summary>
        private void DestroyBlueBrint()
        {
            Destroy(highlight);
            GodPowerManager.activeManager.IsPlacingGodPower = false;
        }

        /// <summary>Place and instantiate the blueprint in multiplayer</summary>
        protected override void ConfirmBluePrintPosition()
        {
            visualiserPosition = hit.point;
            base.ConfirmBluePrintPosition();

            GameObject godPowerVisualiser;

            GodPowerManager.activeManager.IsPlacingGodPower = false;
            GodFavorUI.Instance.RemoveGodFavor(GodPowerManager.activeManager.GetGodFavorCost(god));
            
            // Trigger god favor event
            GodFavorUI.Instance.OnGodPowerTrigger?.Invoke(god);

            switch (god)
            {
                case GodPowerManager.Gods.Zeus:
                    godPowerVisualiser = NetworkingService.Instance.Instantiate(GodPowerManager.activeManager.LightningWarning.name, visualiserPosition + Vector3.up * 0.5f, GodPowerManager.activeManager.LightningWarning.transform.rotation);
                    godPowerVisualiser.GetComponent<GodPowerVisualiser>().scaleTime = GodPowerManager.activeManager.LightningBoltDelayTime + 0.1f;
                    visualiserPosition = godPowerVisualiser.transform.position;
                    StartCoroutine(Delay(GodPowerManager.activeManager.LightningBoltDelayTime, GodPowerManager.activeManager.GodPowerZeus.name, GodPowerManager.activeManager.GodPowerZeus, godPowerVisualiser));
                    break;
                case GodPowerManager.Gods.Ares:
                    break;
                case GodPowerManager.Gods.Athena:
                    StartCoroutine(Delay(GodPowerManager.activeManager.GodShieldDelayTime, GodPowerManager.activeManager.GodPowerAthena.name, GodPowerManager.activeManager.GodPowerAthena, null));
                    break;
            }
        }

        // A short delay before the godpowers get placed
        IEnumerator Delay(float delayTime, string godPowerName, GameObject godPowerPrefab, GameObject godPowerVisualiser)
        {
            yield return new WaitForSeconds(delayTime);

            Vector3 position = Vector3.zero;
            if (godPowerVisualiser)
            {
                position = godPowerVisualiser.transform.position + Vector3.up * godPowerPrefab.transform.localScale.y;
                position = AdjustPosition(position, godPowerVisualiser);
            }
            else
            {
                position = transform.position + Vector3.up * GodPowerManager.activeManager.ShieldDistance;
            }

            godPower = NetworkingService.Instance.Instantiate(godPowerName, position, Quaternion.identity);
            godPower.SetActive(true);
        }

        private Vector3 AdjustPosition(Vector3 position, GameObject godPowerVisualiser)
        {
            // Adjust the y position of the lightning bolt if there is a godshield
            if (Physics.Raycast(godPowerVisualiser.transform.position, godPowerVisualiser.transform.TransformDirection(Vector3.up), out _, Mathf.Infinity, layerMaskGodShield))
            {
                return transform.position + Vector3.up * GodPowerManager.activeManager.ShieldDistance;
            }
            return position;
        }
    }
}