﻿using System;
using System.Collections;
using System.Collections.Generic;

using GameStudio.HunterGatherer.DayAndNightCycle;
using GameStudio.HunterGatherer.Networking;

using UnityEngine;

namespace GameStudio.HunterGatherer.Utilities
{
    public class Cheats : MonoBehaviour
    {
        DayNightCycle DayNightCycle { get; set; }

        private void Awake()
        {
            if (!NetworkingService.OfflineMode)
            {
                enabled = false;
                return;
            }

            DayNightCycle = GetComponent<DayNightCycle>();
        }

        void FixedUpdate()
        {
            DayNightCycleSpeed();
        }

        /// <summary>
        /// Increase and decrease duration of an hour
        /// </summary>
        private void DayNightCycleSpeed()
        {
            // Speed time up
            if (Input.GetKey(KeyCode.KeypadPlus))
            {
                if (DayNightCycle.DurationOfHour > 1f)
                {
                    DayNightCycle.DurationOfHour -= Time.fixedDeltaTime * 5;
                    Debug.Log($"Sped up hour duration to: {DayNightCycle.DurationOfHour}");
                }
            }
            // Speed time down
            else if (Input.GetKey(KeyCode.KeypadMinus))
            {
                if (DayNightCycle.DurationOfHour < 30f)
                {
                    DayNightCycle.DurationOfHour += Time.fixedDeltaTime * 5;
                    Debug.Log($"Slowed down hour duration to: {DayNightCycle.DurationOfHour}");
                }
            }
        }
    }
}