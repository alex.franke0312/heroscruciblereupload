﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameStudio.HunterGatherer.Utilities
{
    /// <summary> Attach this script to an object to make it look at the camera </summary>
    public class LookAtCamera : MonoBehaviour
    {
        private Camera cam;

        private void Start()
        {
            cam = Camera.main;
        }

        private void Update()
        {
            LookAtCameraBillBoard();
        }

        /// <summary> Look at the camera billboard </summary>
        private void LookAtCameraBillBoard()
        {
            transform.LookAt(transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.up);
        }
    }
}
